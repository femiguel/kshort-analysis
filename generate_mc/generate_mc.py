import os
import subprocess

#########################################################
####################### LOGGER ##########################
#########################################################

import logging
__logging_format__ = '%(asctime)s - %(levelname)s: %(message)s'
def configure_logger():
    '''
    Configure the logger instances.
    '''
    f = logging.Formatter(__logging_format__)
    h = logging.StreamHandler()
    h.setFormatter(f)
    h.setLevel(logging.INFO)
    logging.getLogger().addHandler(h)
    logging.getLogger().setLevel(logging.INFO)

configure_logger()

logger = logging.getLogger(__name__)


#########################################################
#########################################################


config = {"year"       : 2016,
          "mag"        : "Down",
          #"event_type" : 34114102,
          "event_type" : 34112100,
          "nevts"      : 500,
          "first_event": 1,
          "run_number" : 1082,
          "dddbtag"    : "dddb-20170721-3",
          "conddbtag"  : "sim-20170721-2-vc-md100",
          "l0tck"      : "0x160F",
          "hlt1tck"    : "0x5138160F",
          "hlt2tck"    : "0x6138160F"}

def make_directory(name, subdir):
    cwd  = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(cwd, name, subdir)

    if not os.path.exists(path):
        os.makedirs(path)

    return path

def setup_tags(path, dddbtag, conddbtag):
    outfname = os.path.join(path, "tags.py")
    code     = ["from Configurables import LHCbApp",
                "LHCbApp().DDDBtag   = '{}'".format(dddbtag),
                "LHCbApp().CondDBtag = '{}'".format(conddbtag),]
    
    return write_code(outfname, code)

def write_code(ofilename, code):
    with open(ofilename, "w") as outf:
        for line in code:
            outf.write(line + "\n")

    return ofilename

def setup_input_data(datafile, path):
    outfname = os.path.join(path, "data.py")
    code      = ["from GaudiConf import IOExtension",
                 "IOExtension().inputFiles(['{}'], clear=True)".format(datafile)]

    return write_code(ofilename = outfname,
                      code      = code)

def gauss(path, config):
    year, mag, event_type, first_event, run_number, nevts = config["year"], config["mag"], config["event_type"], config["first_event"], config["run_number"], config["nevts"]
    output_data = os.path.join(path, "gauss.sim")
    gauss_extraopts = ["$APPCONFIGOPTS/Gauss/Beam6500GeV-m{polarity}100-{year}-nu1.6.py".format(polarity = mag[0].lower(), year = year),
                       "$APPCONFIGOPTS/Gauss/DataType-2016.py",
                       "$DECFILESROOT/options/{}.py".format(event_type),
                       "$LBPYTHIA8ROOT/options/Pythia8.py",
                       "$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py",
                       "$APPCONFIGOPTS/Gauss/RICHRandomHits.py",
                       "$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py",
                       "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]

    code = ["from Gauss.Configuration import GenInit",
            "GaussGen                  = GenInit('GaussGen')",
            "GaussGen.FirstEventNumber = {}".format(first_event),
            "GaussGen.RunNumber        = {}".format(run_number),
            "from Configurables import LHCbApp",
            "LHCbApp().EvtMax          = {}".format(nevts),
            "from Gauss.Configuration import OutputStream",
            "OutputStream('GaussTape').Output = \"DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='RECREATE'\"".format(output_data)
        ]

    config_file = write_code(ofilename = os.path.join(path, "Gauss-Job.py"),
                             code      = code)

    opts  = " ".join(["'{}'".format(o) for o in gauss_extraopts])
    opts += " " + config_file
    
    return output_data, opts

def boole(path, config):
    year = 2015 if int(config["year"]) < 2018 else config["year"]

    output_data = os.path.join(path, "boole.digi")
    boole_extraopts = ["$APPCONFIGOPTS/Boole/Default.py",
                       "$APPCONFIGOPTS/Boole/EnableSpillover.py",
                       "$APPCONFIGOPTS/Boole/DataType-{}.py".format(year),
                       "$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py",
                       "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]

    code = ["from Configurables import Boole",
            "Boole().DatasetName = '{}'".format(output_data.replace(".digi", ""))]

    config_file = write_code(ofilename = os.path.join(path, "Boole-Job.py"),
                             code      = code)

    opts  = " ".join(["'{}'".format(o) for o in boole_extraopts])
    opts += " " + config_file

    return output_data, opts

def moore(path, config):
    step = config["step"]
    output_data = os.path.join(path, step+".digi")
    
    if step == "l0":
        moore_extraopts = ["$APPCONFIGOPTS/L0App/L0AppSimProduction.py",
                           "$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py",
                           "$APPCONFIGOPTS/L0App/L0AppTCK-{}.py".format(config["l0tck"]),
                           "$APPCONFIGOPTS/L0App/DataType-{}.py".format(config["year"]),
                           "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]
        code = ["from Configurables import L0App",
                "L0App().outputFile = '{}'".format(output_data)]

    elif step == "hlt1":
        moore_extraopts = ["$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py",
                           "$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py",
                           "$APPCONFIGOPTS/Conditions/TCK-{}.py".format(config["hlt1tck"]),
                           "$APPCONFIGOPTS/Moore/DataType-{}.py".format(config["year"]),
                           "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]
        code = ["from Configurables import Moore",
                "Moore().outputFile = '{}'".format(output_data)]
    
    elif step == "hlt2":
        moore_extraopts = ["$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py",
                           "$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py",
                           "$APPCONFIGOPTS/Conditions/TCK-{}.py".format(config["hlt2tck"]),
                           "$APPCONFIGOPTS/Moore/DataType-{}.py".format(config["year"]),
                           "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]

        code = ["from Configurables import Moore",
                "Moore().outputFile = '{}'".format(output_data)]
    else:
        raise NotImplementedError("Trigger step {} unknown".format(step))

    

    config_file = write_code(ofilename = os.path.join(path, step+"-Job.py"),
                             code      = code)

    opts  = " ".join(["'{}'".format(o) for o in moore_extraopts])
    opts += " " + config_file

    return output_data, opts

def brunel(path, config):
    output_data = os.path.join(path, "brunel.ldst")

    brunel_extraopts = ["$APPCONFIGOPTS/Brunel/MC-WithTruth.py",
                        "$APPCONFIGOPTS/Brunel/DataType-{}.py".format(config["year"]),
                        "$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py",
                        "$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py"]

    code = ["from Configurables import Brunel",
            "Brunel().DatasetName = '{}'".format(output_data.replace(".ldst", "")),
            "Brunel().OutputType = 'LDST'"]

    config_file = write_code(ofilename = os.path.join(path, "Brunel-Job.py"),
                             code      = code)
    
    opts  = " ".join(["'{}'".format(o) for o in brunel_extraopts])
    opts += " " + config_file

    return output_data, opts

functions = {"gauss" : gauss,
             "boole" : boole,
             "moore" : moore,
             "brunel": brunel}

def runner(application, version, path, config, input_data = None):
    main_dir = make_directory(application, config["step"] if "step" in config else "")

    tagsfile  = setup_tags(path      = main_dir,
                           dddbtag   = config["dddbtag"],
                           conddbtag = config["conddbtag"])

    extra_opts = [tagsfile]
    if input_data is not None:
        extra_opts += [setup_input_data(datafile = input_data,
                                        path     = main_dir)]

    extra_opts = " ".join(extra_opts)

    try:
        func = functions[application.lower()]
    except KeyError:
        raise NotImplementedError("Application {} not implemented".format(application))

    output_data, opts = func(path        = main_dir,
                             config      = config)

    cmd  = "cd {main_dir};"
    cmd += "lb-dev {application}/{version};"
    cmd += "cd {application}Dev_{version};"
    cmd += "make;"
    cmd += "./run gaudirun.py {opts} {extra_opts}"

    cmd  = cmd.format(main_dir    = main_dir,
                      application = application,
                      version     = version,
                      opts        = opts,
                      extra_opts  = extra_opts)

    with open(os.path.join(main_dir, "stdout.log"), "w") as stdout, \
         open(os.path.join(main_dir, "stderr.log"), "w") as stderr:
        logger.info("Running the following command:")
        print(cmd)
    
        process = subprocess.Popen(cmd, shell = True, stdout = stdout, stderr = stderr)

        if process.wait() != 0:
            _, err = process.communicate()
            raise RuntimeError("Job finished with errors:\n{}".format(err))
        else:
            logger.info("Job finalized successfully")

    return output_data



if __name__ == "__main__":
    gaussdata = runner(application = "Gauss",
                       version     = "v49r21",
                       path        = "./",
                       config      = config)
    booledata = runner(application = "Boole",
                       version     = "v30r3",
                       path        = "./",
                       config      = config,
                       input_data  = gaussdata)
    config["step"] = "l0"
    l0data    = runner(application = "Moore",
                       version     = "v25r4",
                       path        = "./",
                       config      = config,
                       input_data  = booledata)
    config["step"] = "hlt1"
    hlt1data  = runner(application = "Moore",
                       version     = "v25r4",
                       path        = "./",
                       config      = config,
                       input_data  = l0data)
    config["step"] = "hlt2"
    hlt2data  = runner(application = "Moore",
                       version     = "v25r4",
                       path        = "./",
                       config      = config,
                       input_data  = hlt1data)
    config.pop("step")
    bruneldata = runner(application = "Brunel",
                        version     = "v50r2",
                        path        = "./",
                        config      = config,
                        input_data  = hlt2data)
