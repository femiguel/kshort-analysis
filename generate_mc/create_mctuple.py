from Configurables import MCDecayTreeTuple, DaVinci, EventSelector
from DecayTreeTuple.Configuration import addBranches
from GaudiPython import AppMgr

decay    = "KS0 => ^mu+ ^mu-"
branches = {"K0S": "^(KS0 => mu+ mu-)",
            "mu1": "KS0 => ^mu+ mu-",
            "mu2": "KS0 => mu+ ^mu-"}
ifilename = "Brunel/brunel.ldst"
ofilename = "MCtest.root"


mctuple       = MCDecayTreeTuple("MCTruthTuple")
mctuple.Decay = decay
mctuple.addBranches(branches)
mctuple.ToolList = ["MCTupleToolKinematic",
                    "MCTupleToolHierarchy",
                    "MCTupleToolPID"]

EventSelector().Input = ["DATAFILE='{}' TYP='POOL_ROOTTREE' Opt='READ'".format(ifilename)]
DaVinci().UserAlgorithms = [mctuple]
DaVinci().TupleFile      = ofilename
DaVinci().Simulation     = True
DaVinci().DataType       = "2016"

gaudi = AppMgr()
gaudi.initialize()
gaudi.run(-1)
gaudi.stop()
gaudi.finalize()
