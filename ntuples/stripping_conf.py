class Channels(object):
    def __init__(self, mother, d1, d2 = None):
        '''
        :mother: str, mother particle
        :d1: str, particles of the first pair of daughters
        :d2: str, particles of the second pair of daughters
        '''
        super(Channels, self).__init__()

        self.mother     = mother
        self.d1         = d1
        self.d2         = d2
        self.daughters  = [self.d1, self.d2] if self.d2 is not None else [self.d1]
        self.mother_key = "KS0" if mother == "K0S" else mother

        self.descriptor = self.mother_key + " -> "
        for s in ["+", "-"]:
            for d in self.daughters:
                self.descriptor += d+s+" "

    @property
    def branches(self):
        '''
        Get the names of the branches as a dictionary to pass to the method addBranches()
        '''
        br = {self.mother: "^("+self.descriptor+")"}
        k  = 1
        for d in self.daughters:
            for s in ["+", "-"]:
                br.update({d+str(k): self.descriptor.replace(d+s, "^"+d+s, 1) if k <= 2 else \
                           self.descriptor.replace(d+s, "^"+d+s).replace("^"+d+s, d+s, 1)})
                if len(self.daughters) == 1 or self.daughters[0] == self.daughters[1] or k < 2:
                    k += 1
                else:
                    k  = 1

        return br

    @property
    def branches_mcmatch(self):
        '''
        Turn the branches from branches() into MC-matching branches, aimed for the MC-matching tuple
        '''
        return {k: v.replace("->", "=>", 1) for k,v in self.branches.items()}

    @property
    def decay(self):
        decay = self.descriptor
        for d in self.daughters:
            if "^"+d in decay: continue
            decay = decay.replace(d, "^"+d)

        return decay

    @property
    def decay_mcmatch(self):
        return self.decay.replace("->", "=>")

    @property
    def descriptor_mcmatch(self):
        return self.descriptor.replace("->", "=>")


class ConfigureStripping(Channels):
    def __init__(self, name, stream, mother, d1, d2 = None,
                 reco = False, input = None, recombine = None):
        '''
        Set up a stripping configuration

        :name:      str, name assigned to the stripping configuration
        :stream:    str, name of the stripping stream
        :mother:    str, name of the mother of the decay being studied
        :d1:        str, particles of the first pair of daughters
        :d2:        str, particles of the second pair of daughters
        :reco:      bool, True if this is configuring a reconstruction tuple
        :input:     str, type of the daughter particles (Loose, if they are LooseParticles or NoPID particles)
        :recombine:
        '''
        self.name   = name
        self.stream = stream
        self.reco   = reco

        super(ConfigureStripping, self).__init__(mother = mother,
                                                 d1     = d1,
                                                 d2     = d2)

        if self.reco:
            self.setup_reco_tuple(input)

        if recombine is not None:
            recombine()

    def combine(self, name, inputs, desc, locs = None,
                _return = False, **kwargs):
        '''
        Create a CombineParticles object and append it to a locations dict, if indicated so

        :name:    str, name of the CombineParticles object. It should be unique
        :inputs:  array of str, input particles
        :desc:    str, decay descriptor
        :locs:    dict or None, if a dict is given, this will be a dictionary that contains some locations from the configuration
        :_return: bool, True if the CombineParticles object is to be returned
        '''
        from Configurables         import CombineParticles
        from CommonParticles.Utils import updateDoD

        combPart = CombineParticles(name,
                                    Inputs            = inputs,
                                    DecayDescriptor = desc,
                                    **kwargs)

        if locs is not None:
            locs.update(updateDoD(combPart))

        if _return: return combPart

    def data_location(self, ismc):
        '''
        Return the TES location of the stripping line corresponding to this configuration

        :ismc: bool, True if the configuration is to set up a MC tuple
                     (in this case, the stripping is re-run and the prefix is not necessary)
        '''
        name = self.name if not self.reco else self.linename
        prefix = "/Event/"+self.stream+"/" if not ismc and not self.reco else ""

        return prefix+"Phys/{}/Particles".format(name)

    def setup_reco_tuple(self, input):
        '''
        Set up a reconstruction tuple

        :input: str, type of the daughter particles (Loose, if they are LooseParticles or NoPID particles)
        '''
        if input == "Loose":
            from CommonParticles import (StdAllLooseMuons,
                                         StdAllLoosePions,
                                         StdAllLooseElectrons)
        elif input == "NoPID":
            from CommonMCParticles import (StandardMCMuons,
                                           StandardMCPions,
                                           StandardMCElectrons)
        else:
            raise Exception("Unknown input {}".format(input))

        input_prefix = "StdMC" if input == "NoPID" else "StdAllLoose"

        loc    = lambda p: "Phys/{0}{1}/Particles".format(input_prefix, p.capitalize())
        suffix = lambda p: "ons" if p.lower() == "pi" or p.lower() == "mu" else "lectrons"
        add_p  = lambda inputs, p: inputs.append(loc(p+suffix(p)))

        inputs = []
        add_p(inputs, self.d1)
        if self.d1 != self.d2 and self.d2 is not None: add_p(inputs, self.d2)

        locations = {}
        name  = self.name+"Reco" if "Reco" not in self.name else self.name
        match = "(mcMatch('{}'))".format(self.descriptor_mcmatch)

        self.combine(name      = name,
                     inputs    = inputs,
                     desc      = self.descriptor,
                     MotherCut = match,
                     Preambulo = ["from LoKiPhysMC.decorators import *"],
                     locs      = locations)

        self.linename = name

class StrippingLineBuilder(Channels):
    def __init__(self, name, mother, d1, d2 = None):
        self.name   = name
        self.mother = mother
        self.d1     = d1
        self.d2     = d2

        super(StrippingLineBuilder, self).__init__(mother = mother,
                                                   d1     = d1,
                                                   d2     = d2)

        self.line = self.setupLine()

    def data_location(self, *args, **kwargs):
        return self.line.outputLocations()[0]

    def setupLine(self):
        from Configurables       import CombineParticles
        from PhysConf.Selections import Selection
        from PhysConf.Selections import SelectionSequence

        combiner = CombineParticles("Combine"+self.name,
                                    DecayDescriptors = [self.descriptor],
                                    DaughtersCuts    = StrippingCuts(self.name).dc,
                                    CombinationCut   = StrippingCuts(self.name).cc,
                                    MotherCut        = StrippingCuts(self.name).mc)

        inputs = self.add_inputs()

        sel    = Selection("Sel"+self.name,
                           Algorithm          = combiner,
                           RequiredSelections = inputs)
        selseq = SelectionSequence("SelSeq"+self.name, TopSelection = sel)

        return selseq

    def add_inputs(self):
        from StandardParticles   import (StdAllLooseMuons, StdAllLooseElectrons, StdAllLoosePions,
                                         StdAllNoPIDsMuons, StdAllNoPIDsElectrons, StdAllNoPIDsPions)
        from Configurables       import FilterDesktop
        from PhysConf.Selections import Selection

        if self.name == "K0s2Mu4Custom":
            
            cuts = StrippingCuts(self.name).input_cuts
            fltr = FilterDesktop("FilterParticles", Code = cuts)

            name_par1 = "Muons" if self.d1 == "mu" else "Electrons" if self.d1 == "e" else "Pions"
            inputs = [Selection("SelMy"+name_par1,
                                Algorithm         = fltr,
                                RequiredSelections = [StdAllLooseMuons if self.d1 == "mu" else StdAllLooseElectrons if self.d1 == "e" else StdAllLoosePions])]
            if self.d1 != self.d2 and self.d2 is not None:
                name_par2 = "Muons" if self.d2 == "mu" else "Electrons" if self.d2 == "e" else "Pions"
                inputs += [Selection("SelMy"+name_par2,
                                     Algorithm         = fltr,
                                     RequiredSelections = [StdAllLooseMuons if self.d2 == "mu" else StdAllLooseElectrons if self.d2 == "e" else StdAllLoosePions])]

        elif self.name == "Ks2PiPiForRnSNoPtCutsLine":
            inputs = [StdAllNoPIDsMuons if self.d1 == "mu" else StdAllNoPIDsElectrons if self.d1 == "e" else StdAllNoPIDsPions]
            if self.d1 != self.d2 and self.d2 is not None:
                inputs += [StdAllNoPIDsMuons if self.d2 == "mu" else StdAllNoPIDsElectrons if self.d2 == "e" else StdAllNoPIDsPions]

        elif self.name == "K0s2PiMuLine":
            inputs = [StdAllNoPIDsPions, StdAllLooseMuons]
        
        return inputs

class StrippingCuts:
    __linenames__ = ["K0s2Mu4Custom",
                     "Ks2PiPiForRnSNoPtCutsLine",
                     "K0s2PiMuLine"]

    def __init__(self, name):
        self.name = name
        if self.name not in self.__linenames__:
            #printerror("Cuts for the given linename not defined")
            raise NotImplementedError("Cuts for line {} not defined".format(self.name))

        self.dc         = self.daughters_cuts()
        self.cc         = self.combination_cuts()
        self.mc         = self.mother_cuts()
        self.input_cuts = self.input_cuts()
        
    def daughters_cuts(self):
        if self.name == "K0s2Mu4Custom":
            return {"mu+": "ALL", "mu-": "ALL"}
        elif self.name == "Ks2PiPiForRnSNoPtCutsLine":
            return {"pi+": "(MIPCHI2DV(PRIMARY) > 100)"}
        elif self.name == "K0s2PiMuLine":
            dc = "(MIPDV(PRIMARY) > 0.5*mm) & (MIPCHI2DV(PRIMARY) > 60.0) & (TRGHOSTPROB < 0.1)"
            return {"pi+": dc, "mu-": dc}

    def mother_cuts(self):
        if self.name == "K0s2Mu4Custom":
            return "(M < 600.0) & "\
                   "(BPVVDZ > 0.0) & "\
                   "(BPVDIRA > 0.9999) & "\
                   "(VFASPF(VZ) < 650.0) & "\
                   "(BPVLTIME() > 0.00459280157946) & "\
                   "(MIPDV(PRIMARY) < 1.0) & "\
                   "(MIPCHI2DV(PRIMARY) < 100) & "\
                   "(VFASPF(VCHI2PDOF) < 50)"
        elif self.name == "Ks2PiPiForRnSNoPtCutsLine":
            return "(M > 400*MeV) & "\
                   "(M < 600*MeV) & "\
                   "(BPVDIRA > 0) & "\
                   "((BPVVDSIGN*M/P) > 1.610411922) & "\
                   "(MIPDV(PRIMARY) < 0.4*mm)"
        
        elif self.name == "K0s2PiMuLine":
            return "(VFASPF(sqrt(VX*VX + VY*VY)) > 4*mm) & "\
                   "(VFASPF(VZ) < 650*mm) & "\
                   "((MIPDV(PRIMARY)/BPVVDZ) < 0.0166666666667) & "\
                   "(MM < 800*MeV) & "\
                   "(VFASPF(VCHI2PDOF) < 9) & "\
                   "(DOCAMAX < 0.1*mm) & "\
                   "(BPVVDZ > 0*mm) & "\
                   "(BPVDIRA > 0)"
        

    def combination_cuts(self):
        if self.name == "K0s2Mu4Custom":
            return "(AMAXDOCA('') < 2.0)"
        elif self.name == "Ks2PiPiForRnSNoPtCutsLine":
            return "(ADAMASS('KS0') < 100*MeV) & (AMAXDOCA('') < 0.1*mm)"
        elif self.name == "K0s2PiMuLine":
            return "(AMAXDOCA('') < 0.1*mm)"

    def input_cuts(self):
        if self.name == "K0s2Mu4Custom":
            return "(TRCHI2DOF < 3) & (TRGHOSTPROB < 0.4)"

def DaVinci_version_and_platform(year):
    '''
    Get the optimal DaVinci version and platform for a given year

    :year: str or int, year being studied
    '''
    from book_info import load_info

    info = load_info()

    return [info["Data{}".format(year)][key] for key in ("dvversion", "platform")]

def stripping_lines_by_stream():
    '''
    Return a dictionary with the stripping lines for each stream defined in the mode.
    '''
    dct = {}
    for s in stripping_lines:
        if isinstance(s, StrippingLineBuilder): continue ## Custom stripping lines are not appended to any stream
        if s.stream not in dct:
            dct[s.stream] = [s]
        else:
            dct[s.stream].append(s)

    return dct

    
stripping_lines = [ConfigureStripping(name   = "K0s2MuMuMuMuLine",
                                      stream = "Dimuon",
                                      mother = "K0S",
                                      d1     = "mu",
                                      d2     = "mu",
                                      reco   = False),
                   
                   ConfigureStripping(name   = "Ks2PiPiForRnSLine",
                                      stream = "Dimuon",
                                      mother = "K0S",
                                      d1     = "pi"),
                   
                   ConfigureStripping(name   = "TriggerTestLine",
                                      stream = "Dimuon",
                                      mother = "J/psi(1S)",
                                      d1     = "mu"),

                   StrippingLineBuilder(name   = "K0s2Mu4Custom",
                                        mother = "K0S",
                                        d1     = "mu",
                                        d2     = "mu"),

                   StrippingLineBuilder(name   = "Ks2PiPiForRnSNoPtCutsLine",
                                        mother = "K0S",
                                        d1     = "pi",
                                        d2     = None)
                   ]
