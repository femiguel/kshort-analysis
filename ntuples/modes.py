import os
import stripping_conf
import book
from   collections import namedtuple
mcMatchInfo = namedtuple("mcMatchInfo", ["name", "descriptor", "branches"])

class Mode(stripping_conf.Channels):
    def __init__(self, name, data, mother, d1, d2 = None):
        self.name = name
        self.data = data
        self.d1   = d1
        self.d2   = d2
        self.mother = mother

        super(Mode, self).__init__(mother = self.mother,
                                   d1     = self.d1,
                                   d2     = self.d2)

    @property
    def mcmatch_info(self):
        return mcMatchInfo(self.name,
                           self.decay_mcmatch,
                           self.branches_mcmatch)

    @property
    def stripping_version(self):
        return self.data.stripping_version()

class ModeConfiguration(stripping_conf.Channels):
    def __init__(self, mode, polarity):
        self.mode = get_mode(mode)
        self.pol  = polarity
        self.year = self.mode.name[-4:]

        super(ModeConfiguration, self).__init__(*get_particles(self.mode.name))

    @property
    def data_sample(self):
        return self.dataset[self.pol]

    @property
    def dataset(self):
        return self.mode.data

    @property
    def ismb(self):
        return ("MB" in self.name)

    @property
    def ismc(self):
        return ("MC" in self.name)

    @property
    def name(self):
        return self.mode.name

    def setup_DaVinci(self, dv, path):
        self.setup_DaVinci_without_output(dv)

        name = "{}_Mag{}".format(self.name, self.pol)
        dv.TupleFile     = os.path.join(path, "{}.root".format(name))
        dv.HistogramFile = os.path.join(path, "{}_DVHistos.root".format(name))

    def setup_DaVinci_without_output(self, dv):
        dv.PrintFreq  = 3000
        dv.EvtMax     = -1
        dv.SkipEvents = 0
        dv.DDDBtag    = self.data_sample.dddb
        dv.CondDBtag  = self.data_sample.conddb
        dv.InputType  = self.data_sample.input_type()
        dv.Simulation = self.ismc
        dv.Lumi       = not self.ismc
        dv.DataType   = self.year

    @classmethod
    def setup_test_sample(cls, mode):
        m = get_mode(mode)

        return cls(m.name, "Down")

    @property
    def stripversion(self):
        return self.dataset.stripping_version()

def get_particles(modename):
    if "Data" in modename:
        return ["K0S", "mu", "mu"]

    _return_pars = ["K0S"]
    key = modename[len("MC")+len("K0S2"):-4]
    key_split = key.split("2")
    if len(key_split) == len([key]):
        key = key.replace("4", "")
        _return_pars += [key for _ in range(2)]
    else:
        key_split.remove("") if "" in key_split else key_split
        if len(key_split) == 1: key_split += [None] ## Case for K0S -> pi+ pi- (or two particles in general)
        _return_pars += key_split

    return _return_pars

__modes__ = {}

for name in book.available_samples():
    sample = book.access_sample(name)

    mother, d1, d2 = get_particles(name)

    __modes__[name] = Mode(name, sample, mother, d1, d2)

def get_mode(name):
    return __modes__[name]

def modes_available():
    return list(__modes__.keys())
