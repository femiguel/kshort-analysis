import modes

def build_stream(mode):
    '''
    Re-run the stripping

    :mode: ModeConfiguration object
    '''
    from Configurables               import EventNodeKiller, ProcStatusCheck, StrippingReport
    from StrippingSettings.Utils     import strippingConfiguration
    from StrippingArchive.Utils      import buildStream
    from StrippingArchive            import strippingArchive
    from StrippingConf.Configuration import StrippingConf

    from stripping_conf              import stripping_lines_by_stream

    nodekiller = EventNodeKiller("StripKiller")
    nodekiller.Nodes = ["/Event/Strip", "/Event/AllStreams"]

    sv = "stripping{}".format(mode.stripversion)

    all_streams = []
    
    for stream,striplines in list(stripping_lines_by_stream().items()):
        if "/Event/"+stream not in nodekiller.Nodes:
            nodekiller.Nodes.append("/Event/"+stream)

        bs = buildStream(streamName = stream,
                         stripping  = strippingConfiguration(sv),
                         archive    = strippingArchive(sv))

        stripline_names = [s.name for s in striplines]

        lines = []
        for l in bs.lines:
            if l.name().replace("Stripping","") in stripline_names:
                l._prescale  = 1.0
                l._postscale = 1.0
                l.RelatedInfoTools = []
                
                lines.append(l)

        bs.lines = lines

        all_streams.append(bs)

        for s in striplines:
            s.stream = None
    

    sc   = StrippingConf(Streams           = all_streams,
                         MaxCandidates     = 2000,
                         AcceptBadEvents   = False,
                         BadEventSelection = ProcStatusCheck(),
                         TESPrefix         = "Strip",
                         HDRLocation       = "Phys/DecReports")
    
    srep = StrippingReport(Selections      = sc.selections(),
                           ReportFrequency = 10000)

    return nodekiller, sc.sequence(), srep

def mb_filter():
    '''
    Return the filter used to select Minimum Bias events
    '''
    from PhysConf.Filters import LoKi_Filters
    
    fltr = LoKi_Filters(HLT2_Code="HLT_PASS('Hlt2MBNoBiasDecision')")

    return fltr.filters("Filters")
    

def configure_mode_test(mode):
    '''
    Parses the input data from a test sample
    
    :mode: str, channel and year to run
    '''
    from Gaudi.Configuration import importOptions, FileCatalog

    channel = modes.ModeConfiguration.setup_test_sample(mode)
    
    test_file = channel.data_sample.test_file
    
    #importOptions(test_file.path)
    path = test_file.path.replace('/', '.').replace('.py', '')
    import importlib
    importlib.import_module(path)
    FileCatalog().Catalogs = ["xmlcatalog_file:{}".format(test_file.catalog)]

    return channel

def run(func, description):
    '''
    Function that handles the input arguments

    :func:        main function to be called after the inputs have been parsed
    :description: str, description of the argument parser
    '''
    import argparse
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument("--mode", "-m", type = str, required = True,
                        choices = modes.modes_available(), help = "Channel to study")
    parser.add_argument("--path", "-p", type = str, default = "./",
                        help = "Path to the output files")

    subparsers = parser.add_subparsers(help = "Mode to run")

    ## Run full
    parser_full = subparsers.add_parser("full", help = "Run full")
    parser_full.set_defaults(which="full")
    parser_full.set_defaults(nevents = -1)
    parser_full.add_argument("--polarity", "-p", type = str, required = True,
                             choices = ["Down", "Up"], help = "Magnet polarity")

    ## Run a test
    parser_test = subparsers.add_parser("test", help = "Run a test")
    parser_test.set_defaults(which="test")
    parser_test.add_argument("--nevents", "-n", type = int, default = -1,
                             help = "Number of events to run")

    args = parser.parse_args()

    if args.which == "test":
        mode = configure_mode_test(args.mode)
    else:
        mode = modes.ModeConfiguration(args.mode, args.polarity)

    func(mode, path = args.path)

    run_gaudipython(args.nevents)

def run_gaudipython(nevts = -1):
    '''
    Run GaudiPython for a given number of events
    
    :nevts: int, number of events
    '''
    import GaudiPython
    gaudi = GaudiPython.AppMgr(outputlevel=3)
    gaudi.initialize()
    gaudi.run(nevts)
    gaudi.stop()
    gaudi.finalize()
