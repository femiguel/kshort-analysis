import argparse
import modes
import stripping_conf
import os
import importlib

cwd = os.getcwd()
os.environ["PYTHONPATH"] = os.path.join(os.environ["PYTHONPATH"], cwd)

parser = argparse.ArgumentParser(description = "Send a ganga job")
parser.add_argument("--mode", "-m", required = True, choices = modes.modes_available(), help = "Mode to be run")
parser.add_argument("--polarity", "-p", required = True, choices = ["Down", "Up"], help = "Magnet polarity")
parser.add_argument("--test", "-t", action = "store_true", default = False, help = "Run only over a few files")
parser.add_argument("--fpj", type=int, default = None,
                    help = "Number of files processed per job")
args = parser.parse_args()

## Configuration of the mode
config = modes.ModeConfiguration(args.mode, args.polarity)

## Application
extra_args = "--mode {} full --polarity {}".format(args.mode, args.polarity)

davinci_version, platform = stripping_conf.DaVinci_version_and_platform(config.year)
path_to_davinci = os.path.join(os.environ["HOME"], "cmtuser/DaVinciDev_"+davinci_version)
if os.path.isdir(os.path.expanduser(path_to_davinci)):
    application = GaudiExec()
    application.directory = path_to_davinci
else:
    application = prepareGaudiExec("DaVinci", davinci_version)

application.platform    = platform
application.options     = [os.path.join(cwd, "optsfile.py")]
application.useGaudiRun = False
application.extraArgs   = extra_args.split()

jobname = "{}_Mag{}".format(args.mode, args.polarity)

ipaths = [os.path.join(cwd, f) for f in (
    "book.py",
    "book_info.py",
    "book_info.yaml",
    "modes.py",
    "stripping_conf.py",
    "ntuple_utils.py",
    "trigger_conf.py"
    )]
ifiles = list(map(LocalFile, ipaths))
if config.data_sample.local_data:
    path    = config.data_sample.data.replace('.py', '').replace('/', '.')
    files   = list(map(DiracFile, importlib.import_module(path).lfns))
    dataset = LHCbDataset(files)
else:
    dataset = BKQuery(config.data_sample.data).getDataset()

backend = Dirac()
if args.test:
    dataset = dataset[0:1]

if args.fpj is not None:
    fpj = args.fpj
else:
    if len(dataset) < 1000:
        fpj = 10 
    else:
        fpj = 100

splitter = SplitByFiles(filesPerJob = fpj, maxFiles = -1, ignoremissing = False)

## Create and submit the job
j = Job(
    name = jobname,
    application = application,
    splitter    = splitter,
    outputfiles = ["*.root"],
    parallel_submit = True,
    backend         = backend,
    inputfiles      = ifiles,
    inputdata       = dataset,
)
j.application.prepare()
j.submit()
