from collections import namedtuple

TestFiles = namedtuple("TestFiles", ["path", "catalog"])
_Sample   = namedtuple("_Sample"  , ["polarity", "data", "dddb", "conddb", "test_file"])

class Sample(_Sample):
    def __new__(cls, polarity, data, dddb, conddb, test_file = None):
        return super(Sample, cls).__new__(cls, polarity, data, dddb, conddb, test_file)

    def input_type(self):
        sl_place = self.data.rfind("/") ## Finds the last place of the data path where a "/" is found
        pt_place = self.data.rfind(".") ## Finds the last place of the data path where a "." is found

        pos   = max(sl_place, pt_place)
        itype = self.data[pos+1:].upper()

        if itype == "PY": ## case for local data
            return "DST"
        else:
            return itype

    @property 
    def local_data(self):
        #print(self.data)
        return self.data.lower().endswith(".py")

    def make_partner(self):
        '''
        Method to simplify the code. The idea is to set up a sample for one of the magnet polarities,
        and automatically set up the other polarity using this method.
        '''
        polarity = "Down" if self.polarity == "Up" else "Up"
        sample_cdb = "m{}".format(self.polarity[0].lower())
        new_cdb    = "m{}".format(polarity[0].lower())

        data = self.data.replace(self.polarity, polarity)
        dddb = self.dddb
        conddb = self.conddb.replace(sample_cdb, new_cdb)

        return Sample(polarity, data, dddb, conddb)

class Dataset(object):
    def __init__(self, mag_up = None, mag_down = None):
        '''
        Build from the two Sample objects (MagUp and MagDown)

        :mag_up:   Sample object, containing the Magnet Up data
        :mag_down: Sample object, containing the Magnet Down data
        '''
        self.data = {"Up": mag_up, "Down": mag_down}

    def __getitem__(self, pol):
        '''
        Get the sample by magnet polarity
        '''
        return self.data[pol]

    def stripping_version(self):
        '''
        Return the stripping version of this dataset
        '''
        data = list(self.data.values())[0].data

        p1 = data.find("Stripping")
        if p1 != -1:
            p1 += len("Stripping")
            p2  = data.find("/", p1)

            while not data[p2-1].isdigit():
                p2 -= 1

            return data[p1:p2]
        elif data.find("catalogs/") != -1:
            return "28" ## stripping version for 2016 private MC for KS-> pi+ pi-
        else:
            print(data)
            raise RuntimeError("Stripping version not found")


################################################################
########################### SET BOOK ###########################
################################################################

import os
import yaml ## We use yaml to avoid unicode characters instead of strings when loading the dict
from   book_info import years, mc_channels, load_info

info = load_info()

__book__ = {}

keys = ["Data"+y for y in years]+["MC"+ch+y for ch in list(mc_channels.keys()) for y in years]
try:
    keys.remove("MCK0S2pi2e22017")
except:
    pass
    
for key in keys:
    datatype     = "MC" if "MC" in key else "Data"
    reducedkey   = key[2:] if datatype == "MC" else key
    testfilename = "catalogs/test_{0}{1}".format("{}_".format(key[2:-4])*("MC" in key)+datatype, key[-4:])

    magdown = Sample(polarity = "Down",
                     data        = info[reducedkey]["path"],
                     dddb        = info[reducedkey]["dddb"],
                     conddb      = info[reducedkey]["conddb"],
                     test_file = TestFiles(*([testfilename+ext for ext in [".py", ".xml"]]))
                     )
    magup   = magdown.make_partner()

    __book__[key] = Dataset(mag_up   = magup,
                            mag_down = magdown)

def access_sample(name):
    '''
    Get the sample associated to the given name
    '''
    return __book__[name]

def available_samples():
    '''
    Return the list of available samples
    '''
    return list(__book__.keys())
