import os
from logging import error as printerror
years = ["2016", "2017", "2018"]

production_ids = {"2016": "00103400",
                  "2017": "00071700",
                  "2018": "00076476"}

mc_channels = {
    "K0S2mu4"   : "34114102",
    "K0S2e4"    : "34124102",
    "K0S2e2mu2" : "34114121",
    "K0S2pi2e2" : "34124016",
    "K0S2pi2"   : "30000000",
    }

to_ignore = ["K0S2pi22016", "K0S2pi22017", "K0S2pi22018", "K0S2pi2e22017", ]

platforms = {
    "v44r10p5": "x86_64-centos7-gcc7-opt",
    "v42r7p2" : "x86_64+avx2+fma-centos7-gcc62-opt",
    "v44r4"   : "x86_64+avx2+fma-centos7-gcc62-opt",
    }

def add_mb_info(info):
    '''
    We manually insert the info for Minimum Bias MC. Two reasons:
    1) MB 2016 is a private generation
    2) MB info takes too long to download

    :info: dict, information with previous channels already studied
    '''
    info["K0S2pi22016"] = {"path"    : "catalogs/info_K0S2pi2_MC2016.py",
                           "dddb"    : "dddb-20170721-3",
                           "conddb"  : "sim-20180411-vc-md100",
                           "numdsts" : 189,
                           "nevts"   : 0,
                           "prod_id" : None,
                           "platform": None}

    info["K0S2pi22017"] = {"path"    : "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/30000000/ALLSTREAMS.DST",
                           "dddb"    : "dddb-20170721-3",
                           "conddb"  : "sim-20180411-vc-md100",
                           "numdsts" : 290,
                           "nevts"   : 1003119,
                           "prod_id" : 89059,
                           "platform": None}
    info["K0S2pi22018"] = {"path"    : "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09k/Trig0x617d18a4/Reco18/30000000/LDST",
                           "dddb"    : "dddb-20170721-3",
                           "conddb"  : "sim-20190430-vc-md100",
                           "numdsts" : 29199,
                           "nevts"   : 95901834,
                           "prod_id" : 131864,
                           "platform": None}

    return info

def get_platform(dv_version):
    return platforms[dv_version]

def get_data_info(year):
    import re

    whitespaces = re.compile("\s+")

    def get_var(line, pattern):
        aux = line.split(pattern)
        while "" in aux: aux.remove("")
        aux = aux[1]

        return re.sub(whitespaces, "", aux)

    pid = production_ids[year]
    logfile = "logs/prod_info_{}.log".format(pid)
    if os.system("lb-dirac dirac-bookkeeping-production-information {0} > {1}".format(pid, logfile)) != 0:
        printerror("Something went wrong when getting the bookkeeping production information on {0}".format(pid))

    with open(logfile, "r") as f:
        lines = f.readlines()

        for line in lines:
            if "ApplicationVersion" in line:
                dvversion = get_var(line, pattern = "ApplicationVersion :")
            if "Collision"+year[-2:] in line and "DIMUON.DST" in line:
                path      = line
            if " DDB " in line:
                dddb      = get_var(line, pattern = ":")
            if "CONDDB" in line:
                conddb    = get_var(line, pattern = ":")
            if "DIMUON.DST:" in line:
                numdsts   = get_var(line, pattern = ":")
            if "DIMUON.DST " in line and "90000000" in line:
                nevts     = get_var(line, pattern = "  ")

    return_vars = []
    for v in (path,dddb,conddb,numdsts,nevts,dvversion):
        return_vars.append(str(v.replace("\n","")))

    return_vars.append(get_platform(dvversion))

    return return_vars

def get_mc_info(evttype, year):
    logfile = "logs/mc_info_{}.log".format(evttype)
    if os.system("lb-dirac dirac-bookkeeping-decays-path {0} > {1}".format(evttype, logfile)) != 0:
        printerror("Something went wrong when getting the bookkeeping decays path of MC {0}".format(evttype))

    with open(logfile, "r") as f:
        lines = f.readlines()
        for line in lines:
            if "Reco"+year in line and "Down" in line and "Beam6500GeV" in line and "Flagged" in line:
                return [l.replace("\n", "") for l in line.split(" ")]+[None]

        printerror("Something went wrong when getting the MC info from event type {}".format(evttype))

def get_info(key):
    isMC    = True if "MC" in key else False
    channel = "" if not isMC else key[2:-4]
    year    = key[-4:]

    func   = get_mc_info if isMC else get_data_info
    params = [mc_channels[channel], year[-2:]] if isMC else [year]

    info = {}
    info["path"], info["dddb"], info["conddb"], info["numdsts"], info["nevts"], info["prod_id" if isMC else "dvversion"], info["platform"] = func(*params)

    return info

def get_info_all():
    info = {}
    for y in years:
        for c, id in mc_channels.items():
            if c+y in to_ignore:
                continue
            info[c+y] = get_info("MC"+c+y)
        info["Data"+y] = get_info(y)

    info = add_mb_info(info)

    return info

def load_info():
    import yaml

    fname = "book_info.yaml"
    if not os.path.exists(fname):
        info = get_info_all()
        with open(fname, "w") as f:
            yaml.dump(info, f)
    else:
        with open(fname, "r") as f:
            info = yaml.safe_load(f)

    return info


if __name__ == "__main__":
    load_info()
