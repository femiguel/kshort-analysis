'''
Get the different catalogs that are available
'''

import os, inspect

options = os.listdir(
    os.path.dirname(
        os.path.abspath(
            inspect.getfile(
                inspect.currentframe()
                ))))

options = set(f[5:f.find('.')] for f in options if '__init__.py' not in f)
