#-- GAUDI jobOptions generated on Fri Feb 25 11:39:51 2022
#-- Contains event types : 
#--   90000000 - 13214 files - 510300935 events - 47671.78 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco18/Stripping34' 

#--  StepId : 133756 
#--  StepName : Stripping34-Merging-DV-v44r4-AppConfig-v3r361-LZMA4-Compression 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r4 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20171030-3 
#--  CONDDB : cond-20180202 
#--  ExtraPackages : AppConfig.v3r361;Det/SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision18/DIMUON.DST/00076476/0000/00076476_00007750_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision18/DIMUON.DST/00076476/0001/00076476_00010362_1.dimuon.dst',
], clear=True)
