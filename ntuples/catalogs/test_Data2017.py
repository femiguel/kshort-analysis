#-- GAUDI jobOptions generated on Tue Mar  8 20:34:11 2022
#-- Contains event types : 
#--   90000000 - 10864 files - 410599072 events - 38945.39 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco17/Stripping29r2' 

#--  StepId : 133315 
#--  StepName : Stripping29r2-Merging-DV-v42r7p2-AppConfig-v3r353-LZMA4-Compression 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v42r7p2 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20170721-3 
#--  CONDDB : cond-20170724 
#--  ExtraPackages : AppConfig.v3r353;Det/SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000201_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000335_1.dimuon.dst',
], clear=True)
