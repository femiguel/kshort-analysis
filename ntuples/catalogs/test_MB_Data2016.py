#-- GAUDI jobOptions generated on Tue Mar  8 20:46:24 2022
#-- Contains event types : 
#--   96000000 - 1710 files - 28754992 events - 1431.83 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco16' 

#--  StepId : 133902 
#--  StepName : FULL-Reco16-cond-20160517-preTesla 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v50r1 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2016.py;$APPCONFIGOPTS/Conditions/2016-pp-InitialTime.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20160517 
#--  ExtraPackages : AppConfig.v3r365;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco16' 

#--  StepId : 133934 
#--  StepName : FULL-Reco16-cond-20160522-preTesla (superseded 129884) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v50r3 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2016.py;$APPCONFIGOPTS/Conditions/2016-pp-InitialTime.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20160522 
#--  ExtraPackages : AppConfig.v3r365;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco16' 

#--  StepId : 133935 
#--  StepName : FULL-Reco16-cond-20161004-preTesla (supersedes 130085) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v50r3 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2016.py;$APPCONFIGOPTS/Conditions/2016-pp-InitialTime.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20161004 
#--  ExtraPackages : AppConfig.v3r365;Det/SQLDDDB.v7r10 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision16/FULL.DST/00085293/0000/00085293_00000291_1.full.dst',
'LFN:/lhcb/LHCb/Collision16/FULL.DST/00085293/0000/00085293_00000239_1.full.dst',
], clear=True)
