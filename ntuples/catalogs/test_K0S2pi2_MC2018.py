#-- GAUDI jobOptions generated on Mon Mar 14 00:18:44 2022
#-- Contains event types : 
#--   30000000 - 29199 files - 95901834 events - 13238.50 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18' 

#--  StepId : 137780 
#--  StepName : Digi14c for 2015 - 25ns spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v30r4 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18' 

#--  StepId : 138052 
#--  StepName : L0 emulation for 2018 - TCK 0x18a4 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x18a4.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2017.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18' 

#--  StepId : 137782 
#--  StepName : TCK-0x517a18a4 (HLT1) Flagged for 2018 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x517a18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18' 

#--  StepId : 137783 
#--  StepName : TCK-0x0x617d18a4 (HLT2) Flagged for 2018 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x617d18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : Y 

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18' 

#--  StepId : 142941 
#--  StepName : Reco18 for MC 2018 - LDST 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v54r4 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2018.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/ldst.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r400 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2018/LDST/00131864/0000/00131864_00000002_5.ldst',
'LFN:/lhcb/MC/2018/LDST/00131864/0000/00131864_00000057_5.ldst',
], clear=True)
