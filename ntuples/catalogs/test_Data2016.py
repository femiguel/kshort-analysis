#-- GAUDI jobOptions generated on Tue Mar  8 20:46:36 2022
#-- Contains event types : 
#--   90000000 - 15394 files - 562295732 events - 55693.26 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco16/Stripping28r2' 

#--  StepId : 140616 
#--  StepName : Stripping28r2-Merging-DV-v44r10p5-AppConfig-v3r393 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r10p5 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20190206-3 
#--  CONDDB : cond-20191004-1 
#--  ExtraPackages : AppConfig.v3r393;Det/SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision16/DIMUON.DST/00103400/0000/00103400_00000048_1.dimuon.dst',
'LFN:/lhcb/LHCb/Collision16/DIMUON.DST/00103400/0000/00103400_00002080_1.dimuon.dst',
], clear=True)
