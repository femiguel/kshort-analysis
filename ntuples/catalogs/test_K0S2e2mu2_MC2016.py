#-- GAUDI jobOptions generated on Tue Mar  8 20:57:52 2022
#-- Contains event types : 
#--   34114121 - 92 files - 254584 events - 38.67 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 137780 
#--  StepName : Digi14c for 2015 - 25ns spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v30r4 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 130088 
#--  StepName : L0 emulation for 2016 - TCK 0x160F - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x160F.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r297 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 130089 
#--  StepName : TCK-0x5138160F (HLT1) Flagged for 2016 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x5138160F.py;$APPCONFIGOPTS/Moore/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r297 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 133524 
#--  StepName : TCK-0x6139160F (HLT2) Flagged for 2016 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v25r4 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x6139160F.py;$APPCONFIGOPTS/Moore/DataType-2016.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r355 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 142636 
#--  StepName : Reco16 for MC 2016 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v50r7 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2016.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r401 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 139393 
#--  StepName : Turbo lines (MC), Turbo 2016 - Stripping28 - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v41r5 
#--  OptionFiles : $APPCONFIGOPTS/Turbo/Tesla_2016_LinesFromStreams_MC.py;$APPCONFIGOPTS/Turbo/Tesla_PR_Truth_2016.py;$APPCONFIGOPTS/Turbo/Tesla_Simulation_2016.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r387;TurboStreamProd.v4r2p9 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged' 

#--  StepId : 142638 
#--  StepName : Stripping28r2-NoPrescalingFlagged for Sim09 - pp at 13 TeV - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r10p5 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping28r2-Stripping-MC-NoPrescaling-DST.py;$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping_28_24.py;$APPCONFIGOPTS/DaVinci/DataType-2016.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py;$APPCONFIGOPTS/DaVinci/DV-RawEventJuggler-4_3-to-4_3.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r401;TMVAWeights.v1r16 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2016/ALLSTREAMS.DST/00154409/0000/00154409_00000063_7.AllStreams.dst',
'LFN:/lhcb/MC/2016/ALLSTREAMS.DST/00154409/0000/00154409_00000072_7.AllStreams.dst',
], clear=True)
