#-- GAUDI jobOptions generated on Fri Feb 25 11:41:30 2022
#-- Contains event types : 
#--   96000000 - 2971 files - 33974066 events - 1595.16 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco18' 

#--  StepId : 133536 
#--  StepName : FULL-Reco18-cond-20180202 (for LOWMULT and Nobias) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v54r1 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2018.py 
#--  DDDB : dddb-20171030-3 
#--  CONDDB : cond-20180202 
#--  ExtraPackages : AppConfig.v3r357;Det/SQLDDDB.v7r10 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision18/FULL.DST/00076473/0000/00076473_00000008_1.full.dst',
'LFN:/lhcb/LHCb/Collision18/FULL.DST/00076473/0000/00076473_00000037_1.full.dst',
], clear=True)
