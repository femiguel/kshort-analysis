'''
LFNs for the K0S-> pi+ pi- private productions
'''
#import os

#path = "LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000"
#name = "MC2016_Priv_md_30000000_{}.ldst"

#lfns = [os.path.join(path, name.format(k)) for k in range(189)]
'''
Define the input data taking the private MB for KS0 -> pi+ pi- production.
'''

# Python
import os

path = 'LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000'
name = 'MC2016_Priv_md_30000000_{}.ldst'

lfns = [os.path.join(path, name.format(i)) for i in range(189)]
