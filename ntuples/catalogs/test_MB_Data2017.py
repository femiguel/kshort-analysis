#-- GAUDI jobOptions generated on Tue Mar  8 20:33:56 2022
#-- Contains event types : 
#--   96000000 - 2201 files - 25193200 events - 1174.22 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 131873 
#--  StepName : FULL-Reco17 (cond-20170724 for LOWMULT and Nobias after MD) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r5 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170724 
#--  ExtraPackages : AppConfig.v3r323;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 131531 
#--  StepName : FULL-Reco17-cond-20170510 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r5 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170510 
#--  ExtraPackages : AppConfig.v3r323;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 132054 
#--  StepName : FULL-Reco17-cond-20170724 (for LOWMULT and Nobias) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r6p1 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170724 
#--  ExtraPackages : AppConfig.v3r323;Det/SQLDDDB.v7r10 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066588/0000/00066588_00000012_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066588/0000/00066588_00000025_1.full.dst',
], clear=True)
