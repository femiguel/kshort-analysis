#-- GAUDI jobOptions generated on Tue Mar  8 21:06:19 2022
#-- Contains event types : 
#--   34124102 - 95 files - 258736 events - 41.86 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 137780 
#--  StepName : Digi14c for 2015 - 25ns spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v30r4 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 133514 
#--  StepName : L0 emulation for 2017 - TCK 0x1709 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v26r6p1 
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x1709.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2017.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r356 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 133515 
#--  StepName : TCK-0x51611709 (HLT1) Flagged for 2017 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v26r6p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x51611709.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r356 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 137501 
#--  StepName : TCK-0x62661709 (HLT2) Flagged for 2017 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v26r6p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x62661709.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r369 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 143005 
#--  StepName : Reco17 for MC 2017 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r10 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r400 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 137692 
#--  StepName : Turbo lines (MC) including TurCal, Turbo 2017 - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v42r8p3 
#--  OptionFiles : $APPCONFIGOPTS/Turbo/Tesla_2017_LinesFromStreamsAndTurCal_MC.py;$APPCONFIGOPTS/Turbo/Tesla_Simulation_2017.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r372;TurboStreamProd.v4r2p7 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged' 

#--  StepId : 141947 
#--  StepName : Stripping29r2-NoPrescalingFlagged for Sim09 - pp at 13 TeV - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v42r7p3 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping29r2-Stripping-MC-NoPrescaling-DST.py;$APPCONFIGOPTS/DaVinci/DataType-2017.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r356;TMVAWeights.v1r9 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2017/ALLSTREAMS.DST/00154491/0000/00154491_00000015_7.AllStreams.dst',
'LFN:/lhcb/MC/2017/ALLSTREAMS.DST/00154491/0000/00154491_00000010_7.AllStreams.dst',
], clear=True)
