#-- GAUDI jobOptions generated on Tue Mar  8 21:13:40 2022
#-- Contains event types : 
#--   34314001 - 91 files - 258027 events - 33.72 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 137780 
#--  StepName : Digi14c for 2015 - 25ns spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v30r4 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 138052 
#--  StepName : L0 emulation for 2018 - TCK 0x18a4 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x18a4.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2017.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 137782 
#--  StepName : TCK-0x517a18a4 (HLT1) Flagged for 2018 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x517a18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : N 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 137783 
#--  StepName : TCK-0x0x617d18a4 (HLT2) Flagged for 2018 - DIGI 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v28r3p1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x617d18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 138796 
#--  StepName : Reco18 for MC 2018 - DST 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v54r2 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2018.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374;Det/SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 137695 
#--  StepName : Turbo lines (MC) including TurCal, Turbo 2018 - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r7 
#--  OptionFiles : $APPCONFIGOPTS/Turbo/Tesla_2018_LinesFromStreamsAndTurCal_MC.py;$APPCONFIGOPTS/Turbo/Tesla_Simulation_2018.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r374;TurboStreamProd.v4r2p10 
#--  Visible : Y 

#--  Processing Pass: '/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged' 

#--  StepId : 141487 
#--  StepName : Stripping34-NoPrescalingFlagged for Sim09 - pp at 13 TeV - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r7 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping34-Stripping-MC-NoPrescaling-DST.py;$APPCONFIGOPTS/DaVinci/DataType-2018.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r376;TMVAWeights.v1r10 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2018/ALLSTREAMS.DST/00154527/0000/00154527_00000069_7.AllStreams.dst',
'LFN:/lhcb/MC/2018/ALLSTREAMS.DST/00154527/0000/00154527_00000053_7.AllStreams.dst',
], clear=True)
