####
## Privately generated sample
####
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__name__)))

## Gaudi
from Gaudi.Configuration import * 
from GaudiConf import IOHelper
#from catalogs.info_K0S2pi2_MC2016 import lfns
from info_K0S2pi2_MC2016 import lfns

IOHelper('ROOT').inputFiles(lfns[:2], clear=True)
