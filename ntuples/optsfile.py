import sys
sys.path.insert(0, ".")

import ntuple_utils
import trigger_conf
from   stripping_conf import stripping_lines

def configure(mode, path = "."):
    from Configurables import DaVinci
    
    dv = DaVinci()
    mode.setup_DaVinci(dv, path)

    algorithms = []

    if mode.ismc:
        algorithms += mc_setup(mode, stripping_lines, dv)
    else:
        if mode.ismb:
            DaVinci().EventPreFilters = ntuple_utils.mb_filter()
        algorithms += make_datatuple(mode, stripping_lines)

    dv.UserAlgorithms += algorithms

def make_mctruth_tuple(mcmatch_info):
    '''
    Create a MCDecayTreeTuple

    :mcmatch_info: mcMatchInfo object (from modes.py), contains the MC-match information of the decay
    '''
    from Configurables                import MCDecayTreeTuple
    from DecayTreeTuple.Configuration import addBranches
    
    mctuple          = MCDecayTreeTuple("MCDecayTreeTuple")
    mctuple.Decay    = mcmatch_info.descriptor
    mctuple.addBranches(mcmatch_info.branches)
    mctuple.ToolList = ["MCTupleToolHierarchy",
                        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"]
    mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True

    return mctuple

def mc_setup(mode, striplines, dv):
    '''
    Set up the ntuples for MC modes.

    :mode:       ModeConfiguration object that contains the information necessary for the ntuples
    :striplines: array of ConfigureStripping objects from stripping_conf.py
    :dv:         DaVinci() object being used in the ntuples production
    '''
    from   Configurables  import GaudiSequencer, LoKi__VoidFilter
    from   stripping_conf import ConfigureStripping, StrippingLineBuilder
    import ntuple_utils
    mctuple = make_mctruth_tuple(mode.mode.mcmatch_info)

    ## Reconstruction with Loose Particles (PID cuts applied -- isMuon==1, PIDe>-2, and others)
    recoline1 = ConfigureStripping(name   = "RecoTuple_LooseParticles",
                                   stream = "Dimuon",
                                   mother = "K0S",
                                   d1     = mode.d1,
                                   d2     = mode.d2,
                                   reco   = True,
                                   input  = "Loose")

    ## Reconstruction with NoPIDs Particles (matched, but no PID cuts applied)
    recoline2 = ConfigureStripping(name   = "RecoTuple_NoPIDsParticles",
                                   stream = "Dimuon",
                                   mother = "K0S",
                                   d1     = mode.d1,
                                   d2     = mode.d2,
                                   reco   = True,
                                   input  = "NoPID")

    seq1       = []
    mctuple    = make_mctruth_tuple(mode.mode.mcmatch_info)
    myvf       = LoKi__VoidFilter("hasPV", Code = "CONTAINS('Rec/Vertex/Primary')>0")

    recotuple1 = make_tuple(mode, recoline1, suffix = "")
    recotuple2 = make_tuple(mode, recoline2, suffix = "")

    seq1 += [mctuple, myvf, recotuple2]

    nk, seq, srep = ntuple_utils.build_stream(mode)
    dv.appendToMainSequence([nk, seq])
    '''
    for linename in ("K0s2MuMuMuMuLine",
                     "Ks2PiPiForRnSLine",
                     ):
        striplines += [ConfigureStripping(name   = linename,
                                          stream = "Dimuon",
                                          mother = mode.mother,
                                          d1     = mode.d1,
                                          d2     = mode.d2,
                                          reco   = False)]
    for customline in ("K0s2Mu4Custom",
                       "Ks2PiPiForRnSNoPtCutsLine",
                       "K0s2PiMuLine",):
        striplines += [StrippingLineBuilder(name   = customline,
                                            mother = mode.mother,
                                            d1     = mode.d1,
                                            d2     = mode.d2)]
    '''
    gs_array = []
    for k,line in enumerate(striplines):
        _seq = seq1[:] ## copy the list as it is
        if isinstance(line, StrippingLineBuilder):
            _seq += [line.line.sequence()]
        _seq += [make_tuple(mode, line, suffix = "Strip")]

        gs = GaudiSequencer("seq"+str(k))
        gs.Members += _seq

        gs_array += [gs]

    return gs_array

def make_datatuple(mode, striplines):
    seqs = []
    for strip in striplines:
        t = make_tuple(mode, strip)
        seqs.append(t)

    return seqs

def make_tuple(mode, strip, suffix = ""):
    '''
    Configure a tuple object
    '''
    from Configurables import (DecayTreeTuple, TupleToolStripping,
                               TupleToolTISTOS, TupleToolTrigger,
                               TupleToolL0Data,
                               LoKi__Hybrid__TupleTool)
    from DecayTreeTuple.Configuration import addBranches
    
    ntuple           = DecayTreeTuple("Data"+strip.name+suffix)
    ntuple.NTupleDir = strip.name
    ntuple.Decay     = strip.decay
    ntuple.addBranches(strip.branches)

    ntuple.ToolList  = ["TupleToolKinematic",
                        "TupleToolPid",
                        "TupleToolANNPID",
                        "TupleToolGeometry"]
    print(strip.data_location(mode.ismc))
    ntuple.Inputs    = [strip.data_location(mode.ismc)]

    ## Configure the tuple tools
    mother = getattr(ntuple, strip.mother)

    #Non-default tools
    ntuple.ToolList += ["TupleToolAngles",
                        "TupleToolEventInfo",
                        "TupleToolMuonPid",
                        "TupleToolPropertime",
                        "TupleToolTrackInfo",
                        "TupleToolIsolationTwoBody",
                        "TupleToolVtxIsoln",
                        "TupleToolRecoStats",]

    mainTools = mother.addTupleTool("LoKi::Hybrid::TupleTool/MainVarsFor"+strip.name)
    mainTools.Variables = {
        "MinDght_P"        : "PFUNA(AMINCHILD(P))",
        "MaxDght_P"        : "PFUNA(AMAXCHILD(P))",
        "MinDght_PT"       : "PFUNA(AMINCHILD(PT))",
        "MaxDght_PT"       : "PFUNA(AMAXCHILD(PT))",
        "MinDght_IP"       : "PFUNA(AMINCHILD(MIPDV(PRIMARY)))",
        "MaxDght_IP"       : "PFUNA(AMAXCHILD(MIPDV(PRIMARY)))",
        "MinDght_IPCHI2"   : "PFUNA(AMINCHILD(MIPCHI2DV(PRIMARY)))",
        "MaxDght_IPCHI2"   : "PFUNA(AMAXCHILD(MIPCHI2DV(PRIMARY)))",
        "MinDght_TRCHI2DOF": "PFUNA(AMINCHILD(TRCHI2DOF))",
        "MaxDght_TRCHI2DOF": "PFUNA(AMAXCHILD(TRCHI2DOF))",
        "MinDght_TRPCHI2"  : "PFUNA(AMINCHILD(TRPCHI2))",
        "MaxDght_TRPCHI2"  : "PFUNA(AMAXCHILD(TRPCHI2))",
        }
    
    ## Extra LoKi variables for the mother particle
    mother_tool = mother.addTupleTool("LoKi::Hybrid::TupleTool/MotherVarsFor"+strip.name)
    mother_tool.Variables = {
        "DOCA"    : "PFUNA(AMAXDOCA(''))",
        "DOCACHI2": "DOCACHI2MAX",
        "ADMASS"  : "ADMASS('KS0')",
        "ETA"     : "ETA",
        }
    
    if mode.ismc:
        ## MC tools
        mctt = ntuple.addTupleTool("TupleToolMCTruth")
        mctt.ToolList += [
            "MCTupleToolKinematic",
            "MCTupleToolPrompt",
            "MCTupleToolHierarchy",
            "MCTupleToolPID",
            "MCTupleToolReconstructed"]
        mother.ToolList += [
            "TupleToolMCBackgroundInfo"]
        
        mcmatch_tool = mother.addTupleTool("LoKi::Hybrid::TupleTool/mcMatchVarsFor"+strip.name)
        mcmatch_functor = lambda d: "switch(mcMatch('{}'), 1, 0)".format(d)
        mcmatch_tool.Preambulo = ["from LoKiPhysMC.decorators import *", 
                                  "from LoKiPhysMC.functions import mcMatch"]
        mcmatch_tool.Variables = {
            'MCMATCH_K0S2MU4': mcmatch_functor('KS0 => mu+ mu- mu+ mu-'),
            'MCMATCH_K0S2PI2': mcmatch_functor('KS0 => pi+ pi-'),
            'MCMATCH_K0S2E4' : mcmatch_functor('KS0 => e+ e- e+ e-'),
            'MCMATCH_K0S2E2MU2': mcmatch_functor('KS0 => e+ e- mu+ mu-')
            }
                
    ## Extra LoKi Variables for the daughters
    for b in filter(lambda s: s != strip.mother, ntuple.Branches):
        getattr(ntuple, b).addTupleTool("LoKi::Hybrid::TupleTool/{}VarsFor{}".format(b,strip.name)).Variables = {"ETA": "ETA",
                                                                                                                 "CL"  : "CL"}

    ## Add variables as seen by L0
    tl0 = ntuple.addTupleTool(TupleToolL0Data)
    tl0.DataList = ["ALL"]

    ## Stripping and trigger lines
    l0lines, hlt1lines, hlt2lines = trigger_conf.phys_lines()

    tts = ntuple.addTupleTool(TupleToolStripping)
    tts.StrippingList = ["Stripping{}Decision".format(s.name)\
                         for s in stripping_lines]

    ## Define trigger lines
    triggerlist = list(set(
        l0lines +
        hlt1lines +
        hlt2lines +
        trigger_conf.nobias_lines() +
        trigger_conf.non_phys_l0_lines()
    ))

    ## General trigger info
    tistos = ntuple.addTupleTool(TupleToolTISTOS)
    tistos.Verbose = True
    tistos.TriggerList = triggerlist

    ttt = ntuple.addTupleTool(TupleToolTrigger)
    ttt.Verbose = True
    ttt.TriggerList = triggerlist

    ## Custom trigger info ( new tuple tools
    ##      that do not interfere with the previous ones)
    custom_tistos = TupleToolTISTOS("CustomTISTOSFor"+strip.name)
    custom_tistos.ExtraName = "Custom"
    custom_tistos.Verbose   = False
    custom_tistos.TriggerList = []

    custom_tistos.Hlt1Phys = "({})".format("|".join(hlt1lines))
    custom_tistos.Hlt2Phys = "({})".format("|".join(hlt2lines))

    mother.addTupleTool(custom_tistos)

    return ntuple


if __name__ == "__main__":
    ntuple_utils.run(configure, description = __doc__)
