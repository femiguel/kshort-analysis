def general_l0_lines():
    '''
    General L0 lines.
    '''
    return [
        'L0ElectronDecision',
        'L0PhotonDecision',
        'L0HadronDecision',
        'L0MuonDecision',
        'L0DiMuonDecision',
        ]


def non_phys_l0_lines():
    '''
    Return the low multiplicity L0 lines.
    '''
    return [
        'L0DiHadron,lowMultDecision',
        'L0HRCDiHadron,lowMultDecision',
        'L0Muon,lowMultDecision',
        'L0DiMuon,lowMultDecision',
        'L0Photon,lowMultDecision',
        'L0DiEM,lowMultDecision',
        'L0Electron,lowMultDecision',
        'L0DiEM,lowMultDecision',
        ]


def general_hlt1_lines():
    '''
    General HLT1 lines.
    '''
    return [
        'Hlt1DiMuonLowMassDecision',
        'Hlt1TrackMuonDecision',
        'Hlt1SingleMuonNoIPDecision',
        ]


def general_hlt2_lines():
    '''
    General HLT2 lines.
    '''
    return [
        'Hlt2DiMuonDetachedDecision',
        'Hlt2SingleMuonLowPTDecision',
        ]


def nobias_lines():
    '''
    Return the HLT1 and HLT2 NoBias lines.
    '''
    return ['Hlt1MBNoBiasDecision', 'Hlt2MBNoBiasDecision']


def phys_lines():
    '''
    Definition of physical trigger lines.
    '''
    l0_lines   = general_l0_lines()
    hlt1_lines = general_hlt1_lines()
    hlt2_lines = general_hlt2_lines()

    # Run-II lines
    hlt1_lines.append('Hlt1DiMuonNoL0Decision')
    hlt2_lines.append('Hlt2DiMuonSoftDecision')

    # Lines for TIS-TOS-ing with KS0 -> pi+ pi- (best ranked for TIS and TOS
    # and for HLT1 and HLT2)

    HLT1_tis = [
        'Hlt1TrackMVADecision',
        'Hlt1TwoTrackMVADecision',
        'Hlt1TrackMuonDecision',
        'Hlt1DiMuonHighMassDecision',
        'Hlt1TrackMuonMVADecision',
        'Hlt1DiMuonLowMassDecision',
        'Hlt1IncPhiDecision',
        'Hlt1SingleMuonHighPTDecision',
        'Hlt1DiMuonNoIPDecision',
        'Hlt1DiMuonNoL0Decision',
        'Hlt1SingleElectronNoIPDecision',
        'Hlt1DiProtonDecision',
        'Hlt1MultiDiMuonNoIPDecision',
        'Hlt1SingleMuonNoIPDecision',
        'Hlt1B2GammaGammaDecision',
        'Hlt1Bottomonium2KstarKstarDecision',
        ]

    HLT1_tos = [
        'Hlt1TrackMVADecision',
        'Hlt1TrackMuonDecision',
        'Hlt1TrackMuonMVADecision',
        'Hlt1DiMuonNoL0Decision',
        'Hlt1DiMuonLowMassDecision',
        'Hlt1SingleMuonNoIPDecision',
        'Hlt1TwoTrackMVADecision',
        ]

    HLT2_tis = [
        'Hlt2Topo2BodyDecision',
        'Hlt2Topo3BodyDecision',
        'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision',
        'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
        'Hlt2Topo4BodyDecision',
        'Hlt2TopoMu2BodyDecision',
        'Hlt2DiMuonJPsiHighPTDecision',
        'Hlt2DiMuonBDecision',
        'Hlt2TopoE2BodyDecision',
        'Hlt2TopoMu3BodyDecision',
        'Hlt2RadiativeIncHHGammaDecision',
        'Hlt2TopoE3BodyDecision',
        'Hlt2DiMuonDetachedHeavyDecision',
        'Hlt2DiMuonPsi2SHighPTDecision',
        'Hlt2DiMuonJPsiDecision',
        'Hlt2RadiativeIncHHHGammaDecision',
        'Hlt2TopoE4BodyDecision',
        ]

    HLT2_tos = [
        'Hlt2ExoticaRHNuDecision',
        'Hlt2DiMuonSoftDecision',
        'Hlt2SingleMuonDecision',
        'Hlt2StrangeLFVMuonElectronSoftDecision',
        'Hlt2ExoticaDisplDiMuonDecision',
        'Hlt2RadiativeIncHHGammaDecision',
        ]

    hlt1_lines = list(set(hlt1_lines + HLT1_tis + HLT1_tos))
    hlt2_lines = list(set(hlt2_lines + HLT2_tis + HLT2_tos))

    return l0_lines, hlt1_lines, hlt2_lines
