# Repository for the $`K_S^0 \rightarrow \mu^+ \mu^- \mu^+ \mu^-`$ analysis

Scripts for the analysis of the decay $`K_S^0 \rightarrow \mu^+ \mu^- \mu^+ \mu^-`$. The code has been written in python 3 and ROOT v6.24. The use of LCG101 is recommended, this can be done by running:

```bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc11-opt/setup.sh
```

Additionally, the execution of the code must come after adding this path to the PYTHONPATH, by running:

```bash
source setup.sh
```

The n-tuples of the analysis are located in a path given in [`scripts/__init__.py`](scripts/__init__.py), in the method `get_tuples_path()`. When running this analysis, this path should be changed so that the latest version of the tuples created remains where it is.

The code is structured as follows:

- [ntuples](ntuples) contains the scripts for the creation of the n-tuples used in the analysis.
- [scripts](scripts) contains the main scripts to execute the different steps of the analysis.
- [utils](utils) contains different modules with useful functions for each step of the analysis.
- [pid](pid) contains the modules for the PID calibration and the computation of the PID efficiencies.
- [temp_files](temp_files) may be used as a directory to store temporary files.
- [test_files](test_files) may contain testing files for some parts of the analysis.

