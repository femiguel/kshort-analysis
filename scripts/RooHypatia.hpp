/** Implementation of a Hypatia PDF.
 *
 * Author: Miguel Ramos Pernas, based on code made by Diego Martinez Santos
 *
 */

#ifndef ROOHYPATIA
#define ROOHYPATIA

#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooHypatia : public RooAbsPdf {

public:

  RooHypatia() { };
  RooHypatia( const char *name, const char *title,
	      RooAbsReal& _x,
	      RooAbsReal& _l,
	      RooAbsReal& _zeta,
	      RooAbsReal& _fb,
	      RooAbsReal& _sigma,
	      RooAbsReal& _mu,
	      RooAbsReal& _a,
	      RooAbsReal& _n,
	      RooAbsReal& _a2,
	      RooAbsReal& _n2 );
  RooHypatia( const RooHypatia& other, const char* name = 0 );

  TObject* clone( const char* newname ) const
  {
    return new RooHypatia(*this, newname);
  }
  inline ~RooHypatia() { }

  Int_t getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0 ) const;
  Double_t analyticalIntegral( Int_t code, const char* rangeName = 0 ) const;

protected:

  RooRealProxy x;
  RooRealProxy l;
  RooRealProxy zeta;
  RooRealProxy fb;
  RooRealProxy sigma;
  RooRealProxy mu;
  RooRealProxy a;
  RooRealProxy n;
  RooRealProxy a2;
  RooRealProxy n2;

  Double_t evaluate() const;

private:

  ClassDef(RooHypatia, 1)
};

#endif
