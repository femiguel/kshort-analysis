/** Implementation file for class RooPowerLaw.hpp
 *
 * Author: Miguel Ramos Pernas
 *
 */

#include <math.h>

#include "Riostream.h"
#include "RooFit.h"
#include "RooAbsReal.h"
#include "TMath.h"

#include "Math/SpecFunc.h"

#include "RooHypatia.hpp"

ClassImp(RooHypatia)

const Double_t SQ2PI = TMath::Sqrt(2.*M_PI);
const Double_t SQ_1_2PI = 1./SQ2PI;
const Double_t LOG_SQ2PI = TMath::Log(SQ2PI);

//_____________________________________________________________________________
Double_t low_x_BK(Double_t nu,Double_t x){
  return TMath::Gamma(nu)*TMath::Power(2., nu - 1.)*TMath::Power(x, -nu);
}

//_____________________________________________________________________________
Double_t low_x_LnBK(Double_t nu, Double_t x){
  return TMath::Log(TMath::Gamma(nu)) + (nu - 1.)*M_LN2 - nu*TMath::Log(x);
}

//_____________________________________________________________________________
Double_t BK(Double_t ni, Double_t x) {

  Double_t nu = TMath::Abs(ni);

  if ( x < 1.e-06 && nu > 0.)
    return low_x_BK(nu, x);

  if ( x < 1.e-04 && nu > 0. && nu < 55.)
    return low_x_BK(nu, x);

  if ( x < 0.1 && nu >= 55.)
    return low_x_BK(nu, x);

  return ROOT::Math::cyl_bessel_k(nu, x);
}

//_____________________________________________________________________________
Double_t LnBK(double ni, double x) {

  Double_t nu = TMath::Abs(ni);

  if ( x < 1.e-06 && nu > 0.)
    return low_x_LnBK(nu, x);

  if ( x < 1.e-04 && nu > 0. && nu < 55.)
    return low_x_LnBK(nu, x);

  if ( x < 0.1 && nu >= 55.)
    return low_x_LnBK(nu, x);

  return TMath::Log(ROOT::Math::cyl_bessel_k(nu, x));
}

//_____________________________________________________________________________
Double_t LogEval(Double_t d, Double_t l, Double_t alpha, Double_t beta, Double_t delta) {

  Double_t gamma = alpha;
  Double_t dg    = delta*gamma;
  Double_t thing = delta*delta + d*d;
  Double_t logno = l*TMath::Log(gamma/delta) - LOG_SQ2PI -LnBK(l, dg);

  return TMath::Exp(logno + beta*d + (0.5 - l)*(TMath::Log(alpha) - 0.5*TMath::Log(thing)) + LnBK(l - 0.5, alpha*TMath::Sqrt(thing)));
}

//_____________________________________________________________________________
Double_t diff_eval(Double_t d, Double_t l, Double_t alpha, Double_t beta, Double_t delta){

  Double_t gamma   = alpha;
  Double_t dg      = delta*gamma;
  Double_t thing   = delta*delta + d*d;
  Double_t sqthing = TMath::Sqrt(thing);
  Double_t alphasq = alpha*sqthing;
  Double_t no      = TMath::Power(gamma/delta, l)/BK(l, dg)*SQ_1_2PI;
  Double_t ns1     = 0.5 - l;

  return no*TMath::Power(alpha, ns1)*TMath::Power(thing, l/2. - 1.25)*
    (-d*alphasq*(BK(l - 1.5, alphasq) + BK(l + 0.5, alphasq)) +
     (2.*(beta*thing + d*l) - d)*BK(ns1, alphasq))*TMath::Exp(beta*d)/2.;
}

//_____________________________________________________________________________
Double_t Gauss2F1(Double_t a, Double_t b, Double_t c, Double_t x){

  if (fabs(x) <= 1)
    return ROOT::Math::hyperg(a, b, c, x);
  else
    return ROOT::Math::hyperg(c - a, b, c, 1. - 1./(1. - x))/TMath::Power(1 - x, b);
}

//_____________________________________________________________________________
Double_t stIntegral(Double_t d1, Double_t delta,Double_t l){

  return d1*Gauss2F1(0.5, 0.5 - l, 3./2, -d1*d1/(delta*delta));
}

//_____________________________________________________________________________
RooHypatia::RooHypatia( const char *name, const char *title,
			RooAbsReal& _x,
			RooAbsReal& _l,
			RooAbsReal& _zeta,
			RooAbsReal& _fb,
			RooAbsReal& _sigma,
			RooAbsReal& _mu,
			RooAbsReal& _a,
			RooAbsReal& _n,
			RooAbsReal& _a2,
			RooAbsReal& _n2 ) :
  RooAbsPdf(name, title),
  x("x", "x", this, _x),
  l("l", "l", this, _l),
  zeta("zeta", "zeta", this, _zeta),
  fb("fb", "fb", this, _fb),
  sigma("sigma", "sigma", this, _sigma),
  mu("mu", "mu", this, _mu),
  a("a", "a", this, _a),
  n("n", "n", this, _n),
  a2("a2", "a2", this, _a2),
  n2("n2", "n2", this, _n2)
{
}

//_____________________________________________________________________________
RooHypatia::RooHypatia( const RooHypatia& other, const char* name ) :
  RooAbsPdf(other, name),
  x("x", this, other.x),
  l("l", this, other.l),
  zeta("zeta", this, other.zeta),
  fb("fb", this, other.fb),
  sigma("sigma", this, other.sigma),
  mu("mu", this, other.mu),
  a("a", this, other.a),
  n("n", this, other.n),
  a2("a2", this, other.a2),
  n2("n2", this, other.n2)
{
}

//_____________________________________________________________________________
Double_t RooHypatia::evaluate() const
{
  Double_t d = x - mu;

  Double_t asigma  = a*sigma;
  Double_t a2sigma = a2*sigma;

  if ( zeta != 0. ) {

    // Careful if zeta -> 0. You can implement a function for the ratio, but
    // careful again that |nu + 1| != |nu| + 1 so you have to deal with the
    // signs.
    Double_t cons0 = TMath::Sqrt(zeta);
    Double_t phi   = BK(l + 1., zeta)/BK(l, zeta);
    Double_t cons1 = sigma/TMath::Sqrt(phi);
    Double_t alpha = cons0/cons1;
    Double_t beta  = fb;
    Double_t delta = cons0*cons1;

    if (d < -asigma){

      Double_t k1 = LogEval(-asigma, l, alpha, beta, delta);
      Double_t k2 = diff_eval(-asigma, l, alpha, beta, delta);
      Double_t B = -asigma + n*k1/k2;
      Double_t A = k1*TMath::Power(B + asigma, n);

      return A*TMath::Power(B - d, -n);
    }
    else if ( d > a2sigma ) {

      Double_t k1 = LogEval(a2sigma, l, alpha, beta, delta);
      Double_t k2 = diff_eval(a2sigma, l, alpha, beta, delta);

      Double_t B = -a2sigma - n2*k1/k2;
      Double_t A = k1*TMath::Power(B + a2sigma, n2);

      return A*TMath::Power(B + d, -n2);

    }
    else
      return LogEval(d, l, alpha, beta, delta);
  }
  else if ( l < 0. ) {

    Double_t beta = fb;

    Double_t delta;
    if ( l <= -1.0 )
      delta = sigma*sqrt(-2 + -2.*l);
    else {
      printf("ERROR (Hypatia): zeta ==0 and l > -1; rms is not defined\n");
      delta = sigma;
    }

    Double_t delta2 = delta*delta;

    if ( d < -asigma ) {

      Double_t cons1 = TMath::Exp(-beta*asigma);
      Double_t phi = 1. + asigma*asigma/delta2;
      Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
      Double_t k2 = beta*k1- cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2*asigma/delta2;
      Double_t B = -asigma + n*k1/k2;
      Double_t A = k1*TMath::Power(B + asigma, n);

      return A*TMath::Power(B - d, -n);
    }
    else if ( d > a2sigma ) {

      Double_t cons1 = TMath::Exp(beta*a2sigma);
      Double_t phi = 1. + a2sigma*a2sigma/delta2;
      Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
      Double_t k2 = beta*k1 + cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2.*a2sigma/delta2;
      Double_t B = -a2sigma - n2*k1/k2;
      Double_t A = k1*TMath::Power(B + a2sigma, n2);

      return  A*TMath::Power(B + d, -n2);
    }
    else
      return TMath::Exp(beta*d)*TMath::Power(1. + d*d/delta2, l - 0.5);
  }
  else
    printf("ERROR (Hypatia): zeta = 0 only suported for l < 0, while l = %e\n", l.arg().getVal());

  return 0.;
}

//_____________________________________________________________________________
Int_t RooHypatia::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* ) const  
{
  if ( matchArgs(allVars, analVars, x) && fb == 0. && zeta == 0 && l < 0)
    return 1;
  return 0;
}

//_____________________________________________________________________________
Double_t RooHypatia::analyticalIntegral( Int_t code, const char* rangeName ) const  
{
  assert(code == 1);

  switch ( code ) {
  case 1:
    {
      Double_t asigma  = a*sigma;
      Double_t a2sigma = a2*sigma;
      Double_t beta    = 0; // Always zero and not set to anything? Below is multiplying some factors.

      Double_t d0 = x.min(rangeName) - mu;
      Double_t d1 = x.max(rangeName) - mu;

      Double_t cons1 = -2.*l;

      Double_t delta;
      if ( l <= -1.0 )
	delta = sigma *sqrt(-2 + cons1);
      else
	delta = sigma;

      Double_t delta2 = delta*delta;

      if ( d0 > -asigma && d1 < a2sigma )
	return stIntegral(d1, delta, l) - stIntegral(d0, delta, l);

      if ( d0 > a2sigma ) {

	cons1 = 1.;
	Double_t phi = 1. + a2sigma*a2sigma/delta2;
	Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
	Double_t k2 = beta*k1+ cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2.*a2sigma/delta2;
	Double_t B = -a2sigma - n2*k1/k2;
	Double_t A = k1*TMath::Power(B + a2sigma, n2);
	return A*(TMath::Power(B + d1, 1 - n2)/(1 - n2) -TMath::Power(B + d0, 1 - n2)/(1 - n2));
      }

      Double_t I0, I1;
      if ( d1 < -asigma ) {

	cons1 = 1.;
	Double_t phi = 1. + asigma*asigma/delta2;
	Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
	Double_t k2 = beta*k1- cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2*asigma/delta2;
	Double_t B = -asigma + n*k1/k2;
	Double_t A = k1*TMath::Power(B + asigma, n);
	I0 = A*TMath::Power(B - d0, 1 - n)/(n - 1);
	I1 = A*TMath::Power(B - d1, 1 - n)/(n - 1);

	return I1 - I0;
      }

      Double_t I1a = 0;
      if ( d0 < -asigma ) {

	cons1 = 1.;
	Double_t phi = 1. + asigma*asigma/delta2;
	Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
	Double_t k2 = beta*k1- cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2*asigma/delta2;
	Double_t B = -asigma + n*k1/k2;
	Double_t A = k1*TMath::Power(B + asigma, n);
	I0 = A*TMath::Power(B - d0, 1 - n)/(n - 1);
	I1a = A*TMath::Power(B + asigma, 1 - n)/(n - 1) - stIntegral(-asigma, delta, l);
      }
      else
	I0 = stIntegral(d0, delta, l);

      Double_t I1b = 0;
      if ( d1 > a2sigma ) {

	cons1 = 1.;
	Double_t phi = 1. + a2sigma*a2sigma/delta2;
	Double_t k1 = cons1*TMath::Power(phi, l - 0.5);
	Double_t k2 = beta*k1+ cons1*(l - 0.5)*TMath::Power(phi, l - 1.5)*2.*a2sigma/delta2;
	Double_t B = -a2sigma - n2*k1/k2;
	Double_t A = k1*TMath::Power(B + a2sigma, n2);
	I1b = A*(TMath::Power(B + d1, 1 - n2)/(1 - n2) -TMath::Power(B + a2sigma, 1 - n2)/(1 - n2)) - stIntegral(d1, delta, l) +  stIntegral(a2sigma, delta, l);
      }
      I1 = stIntegral(d1, delta, l) + I1a + I1b;

      return I1 - I0;
    }
  }

  return 0;
}
