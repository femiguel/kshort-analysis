try:
    from scripts         import printinfo, printerror, printwarning, printdebug
    from utils.mva_utils import setup_mvatuples, get_filenames, compute_ROC, has_classifier
except ModuleNotFoundError:
    raise Exception("You must first source setup.sh")
from ROOT        import TFile, TMVA, TList, TTree, kGreen, kRed
from collections import namedtuple
from array       import array
import os

class TMVAInfo:
    methods = {
        "Likelihood": [TMVA.Types.kLikelihood, "H:!V:TransformOutput:PDFInterpol=Spline2:NSmoothSig[0]=20:NSmoothBkg[0]=20:NSmoothBkg[1]=10:NSmooth=1:NAvEvtPerBin=50"],
        "KNN"       : [TMVA.Types.kKNN, "H:nkNN=20:ScaleFrac=0.8:SigmaFact=1.0:Kernel=Gaus:UseKernel=F:UseWeight=T:!Trim"],
        "HMatrix"   : [TMVA.Types.kHMatrix, "!H:!V"],
        "Fisher"    : [TMVA.Types.kFisher, "H:!V:Fisher:CreateMVAPdfs:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=60:NsmoothMVAPdf=10"],
        "BDT"       : [TMVA.Types.kBDT, "!H:!V:NTrees=400:MaxDepth=3:BoostType=AdaBoost:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning"],
        "BDTD"      : [TMVA.Types.kBDT, "!H:!V:NTrees=250:nEventsMin=400:MaxDepth=3:BoostType=AdaBoost:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning:VarTransform=Decorrelate"],
        "BDTG"      : [TMVA.Types.kBDT, "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.30:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:PruneMethod=CostComplexity:PruneStrength=50:NNodesMax=5"],
        "BDTB"      : [TMVA.Types.kBDT, "!H:!V:NTrees=400:BoostType=Bagging:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning"]}

    factoryOpts   = "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D"

    signalWeight = 1
    bkgWeight    = 1

    ClassifierBranches = "{classifier}_{triggercat}_{parity}"
    name_dataset = "dataset_{triggercat}_{iteration}"

class ObtainTMVA:
    def __init__(self, methodName, WeightsFile, variables, spectators):
        self.myreader = TMVA.Reader("!Color:!Silent")
        self.invars   = {}
        self.variables = variables
        for var in variables:
            self.invars[var] = array("f", [0])
            self.myreader.AddVariable(var, self.invars[var])
        for spec in spectators:
            self.invars[spec] = array("f", [0])
            self.myreader.AddSpectator(spec, self.invars[spec])
        self.myreader.BookMVA(methodName, WeightsFile)

        self.methodName = methodName
        self.nvars      = len(variables)

    def calculate(self, *vals):
        for k,var in enumerate(self.variables):
            self.invars[var][0] = vals[k]
        return self.myreader.EvaluateMVA(self.methodName)            

@setup_mvatuples
def mainTMVA(variables, spectators, triggercat, nfolds,
             _trainTMVA = True, _drawROC = True, _FOM = None, _applyClassifier = False, _drawClassifier = False, **kwargs):
    from utils.pyutils   import file_exists
    from utils.mva_utils import compute_FOM
    
    methods   = TMVAInfo.methods

    ## Train classifiers
    if _trainTMVA or not file_exists(get_filenames("TMVA_MergedTest").format(**locals())):
        trainTMVA(methods     = methods,
                  factoryOpts = TMVAInfo.factoryOpts,
                  variables   = variables,
                  spectators  = spectators,
                  triggercat  = triggercat,
                  nfolds      = nfolds)

    mergedtest = get_filenames("TMVA_MergedTest").format(**locals())

    ## Draw ROC curve
    if _drawROC:
        clf = compute_ROC(ifilename   = mergedtest,
                          itreename   = "DecayTree",
                          classifiers = list(methods.keys()),
                          ofilename   = get_filenames("TMVA_ROC").format(**locals()))
    else:
        clf = None

    ## Draw Classifier distribution or Figure of Merit
    if _drawClassifier or _FOM:
        if (_drawClassifier == "" or isinstance(_drawClassifier, bool)) and clf is None:
            printwarning("No classifier given. Applying BDT by default.")
            clf = "BDT"
        elif clf is None:
            clf = _drawClassifier
        printinfo("Applying classifier {} on FOM data".format(clf))
        for iteration in range(2):
            parity = "even" if iteration==0 else "odd"
            for name in ["Signal", "Bkg"]:
                applyClassifier(ifilename  = get_filenames("FOM").format(mode = "FOM", **locals()),
                                itreename  = "DecayTree_Iter{}".format(iteration),
                                variables  = variables,
                                spectators = spectators,
                                classifier = clf,
                                nfolds     = nfolds,
                                triggercat = triggercat)

            if _drawClassifier:
                drawClassifier(clf, iteration, triggercat)

            if _FOM:
                printinfo("Computing Figure of Merit")
                ofilename = get_filenames("TMVA_FoM").format(FoM = _FOM,classifier=clf,**locals())
                compute_FOM(FOM         = _FOM,
                            bdt_varname = TMVAInfo.ClassifierBranches,
                            triggercat  = triggercat,
                            iteration   = iteration,
                            classifier  = clf,
                            ofilename   = ofilename)

    ## Apply classifier to those tuples
    if _applyClassifier:
        if (_applyClassifier == "" or isinstance(_applyClassifier, bool)) and clf is None:
            printwarning("No classifier given. Applying BDT by default.")
            clf = "BDT"
        elif clf is None:
            clf = _applyClassifier

        from utils.file_utils import get_ofilename
        from utils.rootutils  import copytree, merge_files, merge_trees
        from utils.ana_utils  import years

        mode = "Apply"
        printinfo("Applying classifier {} on Apply data".format(clf))        
        for name in ["Signal", "Bkg"]:
            flist = []
            for year in years:
                ifilename = get_filenames(mode+year).format(**locals())
                ofilename = get_ofilename("Classifier", ifilename)
                _copytree = not file_exists(ofilename)
                
                tlist = []
                for iteration in range(nfolds):
                    treename = "DecayTree_Iter{}".format(iteration)
                    tlist    += [treename]
                    if _copytree:
                        copytree(ifilename = ifilename,
                                 itreename = treename,
                                 ofilename = ofilename,
                                 otreename = treename,
                                 sel       = "")
                    else:
                        printdebug("Apply {year} with classifier ROOT file already exists".format(**locals()))

                    applyClassifier(ifilename  = ofilename, ## we are applying the classifier to the new copy of the file
                                    itreename  = treename,
                                    variables  = variables,
                                    spectators = spectators,
                                    classifier = clf,
                                    nfolds     = nfolds,
                                    triggercat = triggercat)

                ## Merge all folds
                fullofile = get_filenames(mode+year+"Full").format(**locals())
                fullofile = get_ofilename("Classifier", fullofile)
                if not file_exists(fullofile):
                    merge_trees(ifilename  = ofilename,
                                itreenames = tlist,
                                ofilename  = fullofile,
                                otreename  = "DecayTree")
                else:
                    printdebug("The full ROOT file with classifier already exists")
                
                flist += [fullofile]

            _mergedfilename = get_filenames(mode+"Full").format(**locals())
            mergedfilename  = get_ofilename("Classifier", _mergedfilename)
            if not file_exists(mergedfilename):
                merge_files(ofilename = mergedfilename, ifiles = flist)
            else:
                printdebug("Merged {name} file with classifier already exists".format(**locals()))

                                                              
def drawClassifier(classifier, iteration, triggercat):
    '''
    Draw the distribution of the given classifier for a given iteration and trigger category

    :classifier: str, name of the classifier to draw
    :iteration:  int, number of the iteration to draw
    :triggercat: str, name of the trigger category, ("TIS", "xTOS")
    '''
    from utils.plot_utils import draw_hist, PlotOptions
    from utils.mva_utils  import get_selection

    parity = "even" if iteration == 0 else "odd"
    
    ifilenames, itreenames, branchnames, plotOptions, sel = {}, {}, {}, {}, {}
    for name in ["Signal", "Bkg"]:
        ifilenames[name]  = get_filenames("FOM").format(mode = "FOM", **locals())
        itreenames[name]  = "DecayTree_Iter{}".format(iteration)
        branchnames[name] = {TMVAInfo.ClassifierBranches.format(**locals()): [-1, 1]}
        color             = kGreen if name == "Signal" else kRed
        plotOptions[name] = PlotOptions(linecolor      = color,
                                        fillcolor      = color,
                                        fillstyle      = 3004,
                                        legendName     = name.replace("Bkg", "Background"),
                                        legendSize     = (0.4, 0.75, 0.6, 0.85),
                                        legendTextSize = 0.04,
                                        nbins          = 75)
        sel[name]        = get_selection(name      = name,
                                         mode      = "FOM",
                                         iteration = iteration)

    ofilename   = get_filenames("ClassifierDistribution").format(**locals())

    draw_hist(ifiles        = ifilenames,
              itrees        = itreenames,
              branches      = branchnames,
              plotOpts      = plotOptions,
              ofilename     = ofilename,
              _drawNorm     = True,
              sel           = sel,
              includeLegend = True,
              xtitle        = classifier)

@has_classifier
def applyClassifier(ifilename, itreename, variables, spectators, classifier, nfolds, triggercat):
    '''
    Apply a given classifier to a given treename from a given file

    :ifilename: str, path to the input filename
    :itreename: str, name of the input TTree
    :variables: list of str, names of the variables. They must match the ones used for training
    :spectators: list of str, names of the spectators. They must match the ones used for training
    :classifier: str, name of the classifier being used
    :nfolds:     int, number of iterations used in the classifier
    :triggercat: str, ("TIS", "xTOS")
    '''
    from utils.mva_utils import add_column_to_tree
    
    for iteration in range(nfolds):
        weights_file = os.path.join(TMVAInfo.name_dataset.format(**locals()), "weights/TMVAClassification_{}.weights.xml".format(classifier))

        g = ObtainTMVA(classifier, weights_file, variables, spectators)

        parity = "even" if iteration%2==0 else "odd"

        add_column_to_tree(ifilename       = ifilename,
                           itreename       = itreename,
                           variable_name   = TMVAInfo.ClassifierBranches.format(**locals()),
                           variables       = variables,
                           applyClassifier = True,
                           g               = g)

def trainTMVA(variables, spectators, methods, factoryOpts, triggercat, nfolds):
    '''
    Function that handles the TMVA training, creating a Factory, and training the Train and Test samples from both signal and background.

    :variables:   array of str, names of the variables to be trained
    :spectators:  array of str, names of the variables to act as spectators (variables from the signal and bkg tuples that will be kept after the training, but not used for it)
    :methods:     dict, where the keys are the names of the classifiers to be trained, and the values are an array, where the first item is the TMVA object with the corresponding classifier, and the second item is the options to be applied to said classifier
    :factoryOpts: str, options for the Factory creation
    :triggercat:  str, trigger category ("TIS", "xTOS")
    '''
    from utils.pyutils   import make_dirs
    from utils.rootutils import merge_trees

    outflist = []
    for iteration in range(nfolds):
        ofilename = get_filenames("TMVA_Output").format(**locals())
        outf  = TFile.Open(ofilename, "recreate"); outflist += [ofilename]

        printdebug("Creating TMVA Factory")

        factory = TMVA.Factory("TMVAClassification", outf, factoryOpts)
        dataset = TMVA.DataLoader(TMVAInfo.name_dataset.format(**locals()))

        verbose = False
        factory.SetVerbose(verbose)

        for var in variables:
            dataset.AddVariable(var, var, "units", "F")
        for spec in spectators:
            dataset.AddSpectator(spec, spec, "units")

        printdebug("Loading tuples into factory")

        flist, tlist = [], []
        for name, func, weight in [("Signal", dataset.AddSignalTree, TMVAInfo.signalWeight),
                                   ("Bkg"   , dataset.AddBackgroundTree, TMVAInfo.bkgWeight)]:
            for mode in ["Train", "Test"]:
                ifilename = get_filenames(mode).format(**locals())
                itreename = "DecayTree_Iter{0}".format(iteration)

                f = TFile.Open(ifilename)
                t = f.Get(itreename)

                modeing = mode+"ing" if mode == "Train" else mode
                flist.append(f); tlist.append(t)

                func(t, weight, modeing)

        printdebug("Loading methods into factory")
        
        for meth in list(methods.keys()):
            _type, opts = methods[meth]
            factory.BookMethod(dataset, _type, meth, opts)

        printdebug("Training")
        factory.TrainAllMethods()

        printdebug("Testing")
        factory.TestAllMethods()

        printdebug("Evaluating")
        factory.EvaluateAllMethods()

        outf.Close()

        for f in flist:
            f.Close()

    ofilelist = [TFile.Open(of) for of in outflist]
    otreelist = []
    for iteration in range(nfolds):
        otreelist += [ofilelist[iteration].Get(TMVAInfo.name_dataset.format(**locals())+"/TestTree")]

    merge_trees(itrees    = otreelist,
                ofilename = get_filenames("TMVA_MergedTest").format(**locals()),
                otreename = "DecayTree")

def add_arguments(parser):
    parser.add_argument("--triggercat", type = str, default = "TIS",
                        choices = ["TIS", "xTOS"], help = "Trigger category to train")
    parser.add_argument("--makeTuples", action = "store_true", default = False,
                        help = "Create the tuples for the MVA training")
    parser.add_argument("--trainTMVA", action = "store_true", default = False,
                        help = "Create train and test tuples")
    parser.add_argument("--drawROC", action = "store_true", default = False,
                        help = "Whether or not to draw the ROC Curve")
    parser.add_argument("--applyClassifier", action = "store_true", default = False,
                        help = "Whether or not to apply the classifier to the Apply tuples")
    parser.add_argument("--drawClassifier", action = "store_true", default = False,
                        help = "Draw the classifier distribution.")
    parser.add_argument("--FOM", default = None, choices = ["Punzi", "CLs", "Feldman"],
                        help = "Compute the given figure of merit")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    mainTMVA(triggercat       = args.triggercat,
             create_tuples    = args.makeTuples,
             _trainTMVA       = args.trainTMVA,
             _drawROC         = args.drawROC,
             _FOM             = args.FOM,
             _applyClassifier = args.applyClassifier,
             _drawClassifier  = args.drawClassifier)
