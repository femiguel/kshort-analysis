import os
import time
import numpy as np
import functools
from scripts         import printinfo, printerror
from collections.abc import Mapping

_global_config = {
    "assume_finite": bool(os.environ.get("SKLEARN_ASSUME_FINITE", False)),
    "working_memory": int(os.environ.get("SKLEARN_WORKING_MEMORY", 1024)),
    "print_changed_only": True,
    "display": "diagram",
    "pairwise_dist_chunk_size": int(
    os.environ.get("SKLEARN_PAIRWISE_DIST_CHUNK_SIZE", 256)
        ),
    "enable_cython_pairwise_dist": True,
    }

_DEFAULT_TAGS = {
    "non_deterministic": False,
    "requires_positive_X": False,
    "requires_positive_y": False,
    "X_types": ["2darray"],
    "poor_score": False,
    "no_validation": False,
    "multioutput": False,
    "allow_nan": False,
    "stateless": False,
    "multilabel": False,
    "_skip_test": False,
    "_xfail_checks": False,
    "multioutput_only": False,
    "binary_only": False,
    "requires_fit": True,
    "preserves_dtype": [np.float64],
    "requires_y": False,
    "pairwise": False,
    }

class StratifiedKFold:
    def __init__(self, n_splits=5, *, shuffle=False, random_state=None):
        self.n_splits = n_splits
        self.shuffle  = shuffle
        self.random_state = random_state

    def split(self, X, y, groups = None):
        X, y, groups = indexable(X, y, groups)
        n_samples = _num_samples(X)
        if self.n_splits > n_samples:
            raise ValueError("")
        indices = np.arange(n_samples)
        for test_index in self._iter_test_masks(X, y, groups):
            train_index = indices[np.logical_not(test_index)]
            test_index  = indices[test_index]

            yield train_index, test_index

    def _iter_test_masks(self, X, y, groups = None):
        test_folds = self._make_test_folds(X, y)
        for i in range(self.n_splits):
            yield test_folds == i

    def _make_test_folds(self, X, y):
        n_classes = 2 ## 0 and 1
        y_counts = np.bincount(y)
        min_groups = np.min(y_counts)
        
        ## We didvide the original sample in n_splits and get the amount of events of each class for each of the splits
        allocation = np.asarray([np.bincount(y[i :: self.n_splits], minlength=n_classes) for i in range(self.n_splits)])
        test_folds = np.empty(len(y), dtype="i")
        for k in range(n_classes):
            folds_for_class = np.arange(self.n_splits).repeat(allocation[:, k])
            test_folds[y == k] = folds_for_class
        return test_folds

class ParameterGrid:
    def __init__(self, param_grid):
        if isinstance(param_grid, Mapping):
            param_grid = [param_grid]

        self.param_grid = param_grid

    def __len__(self):
        from functools import partial, reduce
        import operator

        product = partial(reduce, operator.mul)
        return sum(product(len(v) for v in p.values()) if p else 1 for p in self.param_grid)

    def __iter__(self):
        for p in self.param_grid:
            items = sorted(p.items())
            if not items:
                yield {}
            else:
                keys, values = zip(*items)
                for v in product(*values):
                    params = dict(zip(keys, v))
                    yield params

    def __getitem__(self, ind):
        '''
        Definition to get an item from the parameter grid
        It outputs an array where the keys are the parameters
        and the values are one specific value from that parameter\'s grid
        '''
        for sub_grid in self.param_grid:
            keys, values_lists = zip(*sorted(sub_grid.items())[::-1])
            sizes = [len(v_list) for v_list in values_lists]
            total = np.product(sizes)
            if ind >= total:
                ind -= total
            else:
                out = {}
                for key, v_list, n in zip(keys, values_lists, sizes):
                    ind, offset = divmod(ind, n) ## returns ind/n -- divmod(5,5) = (1,0); divmod(7,5) = (1,2)
                    out[key] = v_list[offset]
                return out

        raise IndexError("ParameterGrid index out of range")

def check_random_state(seed):
    if seed is None or seed is np.random:
        return np.random.mtrand._rand

    printerror("Seed not None not implemented")

class ParameterSampler:
    def __init__(self, param_distributions, n_iter, *, random_state=None):
        if isinstance(param_distributions, Mapping):
            param_distributions = [param_distributions]

        self.n_iter              = n_iter
        self.random_state        = random_state
        self.param_distributions = param_distributions

    def _is_all_lists(self):
        return all(all(not hasattr(v, "rvs") for v in dist.values()) for dist in self.param_distributions)

    def __iter__(self):
        from sklearn.utils.random import sample_without_replacement

        rng = check_random_state(self.random_state)

        if self._is_all_lists():
            param_grid = ParameterGrid(self.param_distributions)
            grid_size = len(param_grid) ## total combinations of parameter configurations
            n_iter = self.n_iter if grid_size > self.n_iter else grid_size ## number of combinations to be ran in this iteration

            ## selects n_iter parameter combinations out of grid_size
            for i in sample_without_replacement(grid_size, n_iter, random_state=rng):
                yield param_grid[i]

        else:
            printerror("Not all the parameter distributions are lists. Need to implement this part.")

    def __len__(self):
        from functools import partial, reduce
        import operator
        
        if self._is_all_lists():
            product = partial(reduce, operator.mul)
            grid_size = sum(product(len(v) for v in p.values()) if p else 1 for p in self.param_distributions)

            return min(self.n_iter, grid_size)
        else:
            printinfo("not is all lists")
            return self.n_iter

def is_classifier(estimator):
    return getattr(estimator, "_estimator_type", None) == "classifier"

def check_cv(cv, y, classifier):
    import numbers
    
    cv = 5 if cv is None else cv
    if isinstance(cv, numbers.Integral):
        if (
            classifier
            and (y is not None)
            #and (type_of_target(y, input_name="y") in ("binary", "multiclass"))
            ):
            return StratifiedKFold(cv)

    raise NotImplementedError("non-numeric cv has not been implemented")

def _check_estimator_name(estimator):
    if estimator is not None:
        if isinstance(estimator, str):
            return estimator
        else:
            return estimator.__class__.__name__
    return None

def column_or_1d(y, warn = False):
    y = np.asarray(y)
    shape = np.shape(y)
    if len(shape) == 1:
        return np.ravel(y)
    if len(shape) == 2 and shape[1] == 1:
        return np.ravel(y)
        
    raise ValueError(
        "y should be a 1d array, got an array of shape {} instead.".format(shape)
        )

def _num_features(X):
    type_ = type(X)
    if type_.__module__ == "builtins":
        type_name = type_.__qualname__
    else:
        type_name = f"{type_.__module__}.{type_.__qualname__}"

    message = f"Unable to find the number of features from X of type {type_name}"
    if not hasattr(X, "__len__") and not hasattr(X, "shape"):
        if not hasattr(X, "__array__"):
            raise TypeError(message)
        # Only convert X to a numpy array if there is no cheaper, heuristic
        # option.
        X = np.asarray(X)

    if hasattr(X, "shape"):
        if not hasattr(X.shape, "__len__") or len(X.shape) <= 1:
            message += f" with shape {X.shape}"
            raise TypeError(message)
        return X.shape[1]
        
    first_sample = X[0]
        
    # Do not consider an array-like of strings or dicts to be a 2D array
    if isinstance(first_sample, (str, bytes, dict)):
        message += f" where the samples are of type {type(first_sample).__qualname__}"
        raise TypeError(message)

    try:
        # If X is a list of lists, for instance, we assume that all nested
        # lists have the same length without checking or converting to
        # a numpy array to keep this function call as cheap as possible.
        return len(first_sample)
    except Exception as err:
        raise TypeError(message) from err

def _num_samples(x):
    """Return number of samples in array-like x."""
    if hasattr(x, "shape") and x.shape is not None:
        return x.shape[0]

    printerror("The rest of the code of _num_samples needs to be implemented")
            
def _check_y(y, multi_output = False, y_numeric = False, estimator = None):
    if multi_output:
        raise NotImplementedError("Please incorporate multi_output options")
    else:
        estimator_name = _check_estimator_name(estimator)
        y = column_or_1d(y, warn=True)
        #_assert_all_finite(y, input_name="y", estimator_name=estimator_name)
        #_ensure_no_complex_data(y)

    return y

def _passthrough_scorer(estimator, *args, **kwargs):
    return estimator.score(*args, **kwargs)

def _make_indexable(iterable):
    if hasattr(iterable, "__getitem__") or hasattr(iterable, "iloc") or iterable is None:
        return iterable

    printerror("Other options need to be implemented")

def check_consistent_length(*arrays):
    lengths = [_num_samples(X) for X in arrays if X is not None]
    uniques = np.unique(lengths)
    if len(uniques) > 1:
        raise ValueError("")

def indexable(*iterables):
    result = [_make_indexable(X) for X in iterables]
    check_consistent_length(*result)

    return result

def _is_arraylike(x):
    """Returns whether the input is array-like."""
    return hasattr(x, "__len__") or hasattr(x, "shape") or hasattr(x, "__array__")

def _array_indexing(array, key, key_dtype, axis):
    from scipy.sparse import issparse

    if issparse(array) and key_dtype == "bool":
        key = np.asarray(key)
    if isinstance(key, tuple):
        key = list(key)
    return array[key] if axis == 0 else array[:, key]

def _determine_key_type(key, accept_slice=True):
    dtype_to_str = {int: "int", str: "str", bool: "bool", np.bool_: "bool"}
    array_dtype_to_str = {
        "i": "int",
        "u": "int",
        "b": "bool",
        "O": "str",
        "U": "str",
        "S": "str",
        }

    if isinstance(key, tuple(dtype_to_str.keys())):
        return dtype_to_str[type(key)]

    if isinstance(key, slice):
        printerror("Is slice")
    if isinstance(key, (list, tuple)):
        printerror("Is list or tuple")
    if hasattr(key, "dtype"):
        return array_dtype_to_str[key.dtype.kind]
        
def _pandas_indexing(X, key, key_dtype, axis):
    """Index a pandas dataframe or a series."""
    if hasattr(key, "shape"):
        # Work-around for indexing with read-only key in pandas
        # FIXME: solved in pandas 0.25
        key = np.asarray(key)
        key = key if key.flags.writeable else key.copy()
    elif isinstance(key, tuple):
        key = list(key)

    if key_dtype == "int" and not (isinstance(key, slice) or np.isscalar(key)):
        # using take() instead of iloc[] ensures the return value is a "proper"
        # copy that will not raise SettingWithCopyWarning
        return X.take(key, axis=axis)
    else:
        # check whether we should index with loc or iloc
        indexer = X.iloc if key_dtype == "int" else X.loc
        return indexer[:, key] if axis else indexer[key]

def _safe_indexing(X, indices, axis=0):
    if indices is None:
        return X

    indices_dtype = _determine_key_type(indices)
    if hasattr(X, "iloc"):
        #printerror("Has iloc")
        return _pandas_indexing(X, indices, indices_dtype, axis=axis)
    elif hasattr(X, "shape"):
        return _array_indexing(X, indices, indices_dtype, axis=axis)
    else:
        printerror("Doesnt have anything")
        #return _list_indexing(X, indices, indices_dtype)

def _check_fit_params(X, fit_params, indices = None):
    fit_params_validated = {}
    for param_key, param_value in fit_params.items():
        printinfo("param key = {}".format(param_key))
        printinfo("param value = {}".format(param_value))
        if not _is_arraylike(param_value) or _num_samples(param_value) != _num_samples(X):
            fit_params_validated[param_key] = param_value
        else:
            fit_params_validated[param_key] = _make_indexable(param_value)
            fit_params_validated[param_key] = _safe_indexing(fit_params_validated[param_key], indices)

    return fit_params_validated


def clone(estimator, *, safe=True):
    import copy
    
    estimator_type = type(estimator)
    if estimator_type in (list, tuple, set, frozenset):
        return estimator_type([clone(e, safe=safe) for e in estimator])
    elif not hasattr(estimator, "get_params") or isinstance(estimator, type):
        if not safe: ## for None and str objects
            return copy.deepcopy(estimator)
        else:
            printerror("This part of clone() needs to be implemented")

    klass = estimator.__class__
    new_object_params = estimator.get_params(deep=False)
    for name, param in new_object_params.items():
        new_object_params[name] = clone(param, safe=False)
    new_object = klass(**new_object_params)
    params_set = new_object.get_params(deep=False)

    # quick sanity check of the parameters of the clone
    for name in new_object_params:
        param1 = new_object_params[name]
        param2 = params_set[name]
        if param1 is not param2:
            raise RuntimeError(
                "Cannot clone object %s, as the constructor "
                "either does not set or modifies parameter %s" % (estimator, name)
                )
    return new_object

def _top_k(results, k, itr):
    # Return the best candidates of a given iteration
    if results is None:
        printerror("Results is None")
    iteration, mean_test_score, params = (
        np.asarray(a)
        for a in (results["iter"], results["mean_test_score"], results["params"])
        )
    iter_indices = np.flatnonzero(iteration == itr)
    sorted_indices = np.argsort(mean_test_score[iter_indices])

    return np.array(params[iter_indices][sorted_indices[-k:]])

def resample(*arrays, replace=True, n_samples=None, random_state=None, stratify=None):
    from scipy.sparse import issparse
    
    max_n_samples = n_samples
    random_state = check_random_state(random_state)

    if len(arrays) == 0:
        return None
    first = arrays[0]
    n_samples = first.shape[0] if hasattr(first, "shape") else len(first)
    if max_n_samples is None:
        max_n_samples = n_samples
    
    check_consistent_length(*arrays)
    if stratify is None:
        if replace:
            indices = random_state.randint(0, n_samples, size=(max_n_samples,))
        else:
            indices = np.arange(n_samples)
            random_state.shuffle(indices)
            indices = indices[:max_n_samples]
    else:
        printerror("Stratify not None not Implemented")

    arrays = [a.tocsr() if issparse(a) else a for a in arrays]
    resampled_arrays = [_safe_indexing(a, indices) for a in arrays]
    if len(resampled_arrays) == 1:
        # syntactic sugar for the unit argument case
        return resampled_arrays[0]
    else:
        return resampled_arrays
            
class _SubsampleMetaSplitter:
    """Splitter that subsamples a given fraction of the dataset"""
    
    def __init__(self, *, base_cv, fraction, subsample_test, random_state):
        self.base_cv        = base_cv
        self.fraction       = fraction
        self.subsample_test = subsample_test
        self.random_state   = random_state

    def split(self, X, y, groups=None):
        for train_idx, test_idx in self.base_cv.split(X, y, groups):
            #printinfo("Train idx before resample: {}".format(train_idx))
            #printinfo("Test idx before resample: {}".format(test_idx))
            #printinfo("len train idx before resample: {}".format(len(train_idx)))
            #printinfo("len test idx before resample: {}".format(len(test_idx)))
            train_idx = resample(
                train_idx,
                replace      = False,
                random_state = self.random_state,
                n_samples    = int(self.fraction * train_idx.shape[0]),
                )
            if self.subsample_test:
                test_idx = resample(
                    test_idx,
                    replace=False,
                    random_state=self.random_state,
                    n_samples=int(self.fraction * test_idx.shape[0]),
                    )
            #printinfo("Train idx after resample: {}".format(train_idx))
            #printinfo("Test idx after resample: {}".format(test_idx))
            #printinfo("Len train idx after resample: {}".format(len(train_idx)))
            #printinfo("Len test idx after resample: {}".format(len(test_idx)))
            #print()
            yield train_idx, test_idx

import threading
_threadlocal = threading.local()

def _get_threadlocal_config():
    """Get a threadlocal **mutable** configuration. If the configuration
    does not exist, copy the default global configuration."""
    if not hasattr(_threadlocal, "global_config"):
        _threadlocal.global_config = _global_config.copy()
    return _threadlocal.global_config

def get_config():
    return _get_threadlocal_config().copy()

def set_config(assume_finite=None,
               working_memory=None,
               print_changed_only=None,
               display=None,
               pairwise_dist_chunk_size=None,
               enable_cython_pairwise_dist=None,):
    local_config = _get_threadlocal_config()

    if assume_finite is not None:
        local_config["assume_finite"] = assume_finite
    if working_memory is not None:
        local_config["working_memory"] = working_memory
    if print_changed_only is not None:
        local_config["print_changed_only"] = print_changed_only
    if display is not None:
        local_config["display"] = display
    if pairwise_dist_chunk_size is not None:
        local_config["pairwise_dist_chunk_size"] = pairwise_dist_chunk_size
    if enable_cython_pairwise_dist is not None:
        local_config["enable_cython_pairwise_dist"] = enable_cython_pairwise_dist

from contextlib import contextmanager as contextmanager

@contextmanager
def config_context(*,
                   assume_finite=None,
                   working_memory=None,
                   print_changed_only=None,
                   display=None,
                   pairwise_dist_chunk_size=None,
                   enable_cython_pairwise_dist=None,):
    old_config = get_config()
    set_config(
        assume_finite=assume_finite,
        working_memory=working_memory,
        print_changed_only=print_changed_only,
        display=display,
        pairwise_dist_chunk_size=pairwise_dist_chunk_size,
        enable_cython_pairwise_dist=enable_cython_pairwise_dist,
        )

    try:
        yield
    finally:
        set_config(**old_config)

class _FuncWrapper:
    def __init__(self, function):
        self.function = function
        self.config   = get_config()
        functools.update_wrapper(self, self.function)

    def __call__(self, *args, **kwargs):
        with config_context(**self.config):
            return self.function(*args, **kwargs)

def delayed(function):
    """Decorator used to capture the arguments of a function."""
    
    @functools.wraps(function)
    def delayed_function(*args, **kwargs):
        return _FuncWrapper(function), args, kwargs
    
    return delayed_function

def _safe_tags(estimator, key=None):
    if hasattr(estimator, "_get_tags"):n
        tags_provider = "_get_tags()"
        tags = estimator._get_tags()
    elif hasattr(estimator, "_more_tags"):
        printinfo("tags 2")
        tags_provider = "_more_tags()"
        tags = {**_DEFAULT_TAGS, **estimator._more_tags()}
    else:
        printinfo("tags 3")
        tags_provider = "_DEFAULT_TAGS"
        tags = _DEFAULT_TAGS

    if key is not None:
        if key not in tags:
            raise ValueError(
                f"The key {key} is not defined in {tags_provider} for the "
                f"class {estimator.__class__.__name__}."
                )
        return tags[key]
    return tags

def _safe_split(estimator, X, y, indices, train_indices=None):
    if _safe_tags(estimator, key="pairwise"):
        if not hasattr(X, "shape"):
            raise ValueError(
                "Precomputed kernels or affinity matrices have "
                "to be passed as arrays or sparse matrices."
                )
        # X is a precomputed square kernel matrix
        if X.shape[0] != X.shape[1]:
            raise ValueError("X should be a square kernel matrix")
        if train_indices is None:
            X_subset = X[np.ix_(indices, indices)]
        else:
            X_subset = X[np.ix_(indices, train_indices)]

    else:
        X_subset = _safe_indexing(X, indices)

    if y is not None:
        y_subset = _safe_indexing(y, indices)

    return X_subset, y_subset

from contextlib import suppress
def _score(estimator, X_test, y_test, scorer, error_score="raise"):
    import numbers
    if isinstance(scorer, dict):
        # will cache method calls if needed. scorer() returns a dict
        scorer = _MultimetricScorer(**scorer)
        
    try:
        if y_test is None:
            scores = scorer(estimator, X_test)
        else:
            scores = scorer(estimator, X_test, y_test)
    except Exception:
        raise

    if isinstance(scores, dict):
        for name, score in scores.items():
            if hasattr(score, "item"):
                with suppress(ValueError):
                    score = score.item()
            if not isinstance(score, numbers.Number):
                raise ValueError("")
            scores[name] = score
    else:  # scalar
        if hasattr(scores, "item"):
            with suppress(ValueError):
                scores = scores.item()
        if not isinstance(scores, numbers.Number):
            raise ValueError("")
    return scores
                
def _fit_and_score(estimator,
                   X,
                   y,
                   scorer,
                   train,
                   test,
                   verbose,
                   parameters,
                   fit_params,
                   return_train_score=False,
                   return_parameters=False,
                   return_n_test_samples=False,
                   return_times=False,
                   return_estimator=False,
                   split_progress=None,
                   candidate_progress=None,
                   error_score=np.nan,):
    fit_params = fit_params if fit_params is not None else {}
    fit_params = _check_fit_params(X, fit_params, train)

    if parameters is not None:
        cloned_parameters = {}
        for k, v in parameters.items():
            cloned_parameters[k] = clone(v, safe=False)

        estimator = estimator.set_params(**cloned_parameters)

    X_train, y_train = _safe_split(estimator, X, y, train)
    X_test, y_test = _safe_split(estimator, X, y, test, train)
    printerror("")
    result = {}
    start_time = time.time()
    try:
        estimator.fit(X_train, y_train, **fit_params)
    except Exception:
        raise
    else:
        result["fit_error"] = None

        fit_time = time.time() - start_time
        test_scores = _score(estimator, X_test, y_test, scorer, error_score)
        score_time = time.time() - start_time - fit_time
        if return_train_score:
            train_scores = _score(estimator, X_train, y_train, scorer, error_score)

    result["test_scores"] = test_scores
    if return_train_score:
        result["train_scores"] = train_scores
    if return_n_test_samples:
        result["n_test_samples"] = _num_samples(X_test)
    if return_times:
        result["fit_time"] = fit_time
        result["score_time"] = score_time
    if return_parameters:
        result["parameters"] = parameters
    if return_estimator:
        result["estimator"] = estimator

    return result

def _aggregate_score_dicts(scores):
    return {
        key: np.asarray([score[key] for score in scores])
        if isinstance(scores[0][key], numbers.Number)
        else [score[key] for score in scores]
        for key in scores[0]
    }

        
class ParameterSearch:
    def __init__(self, estimator, param_distributions, n_candidates = "exhaust",
                 factor = 3, cv = 5, n_jobs = None, resource = "n_samples", scoring = None, random_state = None):
        self.estimator = estimator
        self.param_distributions = param_distributions
        self.factor              = factor
        self.cv                  = cv
        self.resource            = resource
        self.scoring             = scoring
        self.n_jobs              = n_jobs
        self.n_candidates        = n_candidates
        self.random_state        = random_state
        
    def _check_input_parameters(self, X, y, groups):
        ## Min resources
        n_splits = self._checked_cv_orig.n_splits
        magic_factor = 2
        self.min_resources = n_splits * magic_factor

        if is_classifier(self.estimator):
            y = self._validate_data(y = y)
            n_classes = np.unique(y).shape[0]
            self.min_resources *= n_classes

        self.max_resources = _num_samples(X)

    def _check_n_features(self, X, reset):
        try:
            n_features = _num_features(X)
        except TypeError as e:
            if not reset and hasattr(self, "n_features_in_"):
                raise ValueError

            return

        if reset:
            self.n_features_in_ = n_features
            return

        if not hasattr(self, "n_features_in_"):
            return

        if n_features != self.n_features_in_:
            raise ValueError

    def _validate_data(self, X="no_validation", y="no_validation", reset=True, validate_separately=False, **check_params):
        default_check_params = {"estimator": self}
        check_params = {**default_check_params, **check_params}

        not_validate_X = isinstance(X, str) and X == "no_validation"
        not_validate_y = isinstance(y, str) and y == "no_validation"

        validate_X, validate_y = not not_validate_X, not not_validate_y

        if not_validate_X and validate_y:
            y = _check_y(y, **check_params)
            out = y
        else:
            raise NotImplementedError("Data validation for X and not y has not been implemented")

        if not_validate_X and check_params.get("ensure_2d", True):
            self._check_n_features(X, reset=reset)

        return out

    def _run_search(self, evaluate_candidates):
        from math import ceil, log, floor
        
        candidate_params = self._generate_candidate_params()
        # n_required_iterations is the number of iterations needed so that the
        # last iterations evaluates less than `factor` candidates.
        n_required_iterations = 1 + floor(log(len(candidate_params), self.factor))

        # To exhaust the resources, we want to start with the biggest
        # min_resources possible so that the last (required) iteration
        # uses as many resources as possible
        last_iteration = n_required_iterations - 1
        self.min_resources = max(self.min_resources,
                                 self.max_resources // self.factor**last_iteration)

        n_possible_iterations = 1 + floor(log(self.max_resources // self.min_resources, self.factor))
        n_iterations          = min(n_possible_iterations, n_required_iterations)

        printinfo("*"*30)
        printinfo("n iterations: {}".format(n_iterations))
        

        self.n_resources_ = []
        self.n_candidates_ = []

        for itr in range(n_iterations):
            power = itr
            n_resources = int(self.factor**power * self.min_resources)
            n_resources = min(n_resources, self.max_resources)
            self.n_resources_.append(n_resources)

            n_candidates = len(candidate_params)
            self.n_candidates_.append(n_candidates)

            printinfo("n_resources = {}".format(n_resources))
            printinfo("self._n_samples_orig = {}".format(self._n_samples_orig))
            printinfo("fraction = {}".format(n_resources/self._n_samples_orig))
            
            cv = _SubsampleMetaSplitter(base_cv        = self._checked_cv_orig,
                                        fraction       = n_resources / self._n_samples_orig,
                                        #fraction       = 1,
                                        subsample_test = True,
                                        random_state   = self.random_state)


            more_results = {"iter"       : [itr] * n_candidates,
                            "n_resources": [n_resources] * n_candidates}
            
            results = evaluate_candidates(candidate_params, cv, more_results=more_results)
            
            #n_candidates_to_keep = ceil(n_candidates / self.factor)
            #candidate_params = _top_k(results, n_candidates_to_keep, itr)
        printerror("")
        self.n_remaining_candidates_ = len(candidate_params)
        self.n_required_iterations_  = n_required_iterations
        self.n_possible_iterations_  = n_possible_iterations
        self.n_iterations_           = n_iterations

    def _generate_candidate_params(self):
        n_candidates_first_iter = self.n_candidates
        if n_candidates_first_iter == "exhaust":
            # This will generate enough candidate so that the last iteration
            # uses as much resources as possible
            n_candidates_first_iter = self.max_resources // self.min_resources

        return ParameterSampler(self.param_distributions,
                                n_candidates_first_iter,
                                random_state = self.random_state,)

    def _format_results(self, candidate_params, n_splits, out, more_results=None):
        n_candidates = len(candidate_params)
        out = _aggregate_score_dicts(out)

        results = dict(more_results or {})
        for key, val in results.items():
            # each value is a list (as per evaluate_candidate's convention)
            # we convert it to an array for consistency with the other keys
            results[key] = np.asarray(val)

        printinfo("results = {}".format(results))

        printerror("")

    def fit(self, X, y, groups = None, **fit_params):
        from joblib      import Parallel
        from collections import defaultdict

        self._checked_cv_orig = check_cv(self.cv, y, classifier = is_classifier(self.estimator))
        n_splits = self._checked_cv_orig.n_splits
        self._check_input_parameters(X = X, y = y, groups = groups)

        self._n_samples_orig = X.shape[0]

        ## super().fit()
        refit_metric = "score"
        scorers = _passthrough_scorer

        if X.shape[0] != y.shape[0]:
            printerror("X and y have different sizes")

        fit_params = _check_fit_params(X, fit_params)
        printinfo("Estimator = {}".format(self.estimator))
        base_estimator = clone(self.estimator)

        parallel = Parallel(n_jobs=self.n_jobs)

        fit_and_score_kwargs = dict(scorer                = scorers,
                                    fit_params            = fit_params,
                                    return_train_score    = False,
                                    return_n_test_samples = True,
                                    return_times          = True,
                                    return_parameters     = False,
                                    error_score           = "raise",
                                    verbose               = 1,
                                )
        results = {}
        with parallel:
            all_candidate_params = []
            all_out              = []
            all_more_results     = defaultdict(list)

            def evaluate_candidates(candidate_params, cv=None, more_results=None):
                from itertools import product
                
                candidate_params = list(candidate_params)
                n_candidates     = len(candidate_params)
                printinfo("Fitting {0} folds for each of {1} candidates, total of {2} fits".format(n_splits, n_candidates, n_candidates*n_splits))

                out = parallel(delayed(_fit_and_score)(clone(base_estimator),
                                                       X,
                                                       y,
                                                       train=train,
                                                       test=test,
                                                       parameters=parameters,
                                                       split_progress=(split_idx, n_splits),
                                                       candidate_progress=(cand_idx, n_candidates),
                                                       **fit_and_score_kwargs,
                                                       )
                               for (cand_idx, parameters), (split_idx, (train, test)) in product(enumerate(candidate_params), enumerate(cv.split(X, y, groups))))
                
                all_candidate_params.extend(candidate_params)
                all_out.extend(out)

                if more_results is not None:
                    for key, value in more_results.items():
                        all_more_results[key].extend(value)
                        
                nonlocal results
                results = self._format_results(all_candidate_params, n_splits, all_out, all_more_results)
                
                return results

            self._run_search(evaluate_candidates)


            
