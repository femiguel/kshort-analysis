import os
import ROOT

from utils.fit_utils import create_var
from scripts import printinfo, printerror, printwarning, printdebug
#from ROOT import TMath, Math, RooAbsPdf

class Named(object):
    def __init__(self):
        pass

    @staticmethod
    def _format_name(prefix, name):
        '''
        Append names

        :prefix: str, prefix for the name we want to give
        :name:   str, name we want
        '''
        return "{}_{}".format(prefix, name)

    def format_name(self, name):
        '''
        Return a new name, formatted as NameOfTheModel_name
        '''
        return Named._format_name(self.name(), name)

    def name(self):
        '''
        Return the name. This is a method created for all subclasses that
        attempt to call format_name()
        '''
        raise NotImplementedError("Attempt to call a method from an abstract class")

class Category(Named):
    def __init__(self, name, model, data):
        self._name = name
        self.model = model
        
        self.data  = data

        self.model._define_names(prefix=self._name)
        self.data.SetName(self.format_name(self.data.GetName()))

    @classmethod
    def make_simultaneous(cls, name, category, categories, mass, weight = None):
        '''
        Make a category to perform simultaneous fits
        '''
        printinfo("Creating simultaneous category with name {}".format(name))

        args = (name, name, ROOT.RooArgSet(mass))
        if weight is not None:
            args += (ROOT.RooFit.WeightVar(weight),)

        #data = ROOT.RooDataSet(*args)
        imports = ()
        for c in categories:
            category.defineType(c.name())
            #data = ROOT.RooDataSet(*args, ROOT.RooFit.Index(category), ROOT.RooFit.Import(data), ROOT.RooFit.Import(c.name(), c.data))
            imports += (ROOT.RooFit.Import(c.name(), c.data),)
        data = ROOT.RooDataSet(*args, ROOT.RooFit.Index(category), *imports)
        model = SimModel(name, category, categories)

        return cls(name,model,data)

    def name(self):
        return self._name

    def save(self, name = None):
        '''
        Save the parameters in this class to a ROOT file
        '''
        name  = name or self.name()

        ws = ROOT.RooWorkspace(name)
        ws.load(self.model.pdf)
        ws.load(self.data)
        ws.Write()

class FitModel(Named):
    def __init__(self, pdf, params = None):
        '''
        '''
        super(FitModel, self).__init__()
        self.pdf = pdf
        
        if params is not None:
            self.params = tuple(params)
        else:
            self.params = ()

    def _define_names(self, prefix = None):
        '''
        Define the names of the objects in this class

        :prefix: str, given prefix to the name
        '''
        if prefix is not None:
            self.pdf.SetName(Named._format_name(prefix, self.name()))
        for p in self.params:
            p.SetName(self.format_name(p.GetName()))

    def name(self):
        '''
        Return the name of the model
        '''
        return self.pdf.GetName()

    def par(self, name):
        '''
        Return a parameter

        :name: str, name of the parameter
        '''
        for p in self.pars():
            if p.GetName() == name:
                return p

        printwarning("Parameter with name {} was not found".format(name))

    def pars(self):
        '''
        Return all the parameters of the PDFs in this class
        '''
        return self.params

class SingleModel(FitModel):
    def __init__(self, pdf, mass, params):
        self.mass = mass
        super(SingleModel, self).__init__(pdf, params)

    @classmethod
    def create_pdf(cls, constr, name, mass, *args):
        '''
        Build a model from a PDF constructor

        :constr: ROOT object, PDF constructor
        :name:   str, name of the constructor
        :mass:   RooRealVar, variable to fit
        :args:   additional arguments
        '''
        pdf = constr(name, name, mass, *args)
        
        return cls(pdf, mass, args)

    @classmethod
    def create_extended_pdf(cls, name, mass, bkg, nbkg, rangeName, *args):
        pdf = ROOT.RooExtendPdf(name, name, bkg, nbkg, rangeName)
        args += (bkg,)
        return cls(pdf, mass, args)

class MultiModel(FitModel):
    def __init__(self, pdf, models, yields = None):
        '''
        Object representing a model with various submodels
        '''
        self.models = models
        
        super(MultiModel, self).__init__(pdf, yields)

    def _define_names(self, prefix = None):
        '''
        Define the names of the objects in this class
        '''
        FitModel._define_names(self, prefix)
        for m in self.models:
            m._define_names(prefix=prefix)

    def components(self):
        '''
        Returns a list with the names of the components
        '''
        return [m.pdf.GetName() for m in self.models]

    def get_model(self, name):
        '''
        Return model from name

        :name: str, name of the model
        '''
        for m in self.models:
            if m.name() == name:
                return m

    def pars(self):
        '''
        Return all the parameters of the PDFs in this class
        '''
        from itertools import chain
        
        self_pars = super(MultiModel, self).pars()
        models_pars = tuple(chain.from_iterable(m.pars() for m in self.models))

        return self_pars + models_pars

class CombinedModel(MultiModel):
    def __init__(self, name, models, yields = None, coef = None):
        pdfs = tuple(m.pdf for m in models)
        
        if yields is not None:
            pdf = ROOT.RooAddPdf(name, name, ROOT.RooArgList(*pdfs), ROOT.RooArgList(*yields))
            super(CombinedModel, self).__init__(pdf, models, yields)
        else:
            if coef is None:
                coef = create_var("coef", 0.5, 0, 1)
            pdf = ROOT.RooAddPdf(name, name, *pdfs, coef)
            super(CombinedModel, self).__init__(pdf, models, [coef])
        
class ConvolutionModel(MultiModel):
    pass

class SimModel(FitModel):
    def __init__(self, name, category, categories):
        self.category = category
        pdf = ROOT.RooSimultaneous(name, name, category)

        for c in categories:
            pdf.addPdf(c.model.pdf, c.name())

        super(SimModel,self).__init__(pdf)

class SignalModel:
    __model_name__ = "sig"
    def default(var, mean, params, name = "", constant = False):
        '''
        Build a double CB as the default model
        '''
        name = __model_name__ if name == "" else name
        model = SignalModel.draw_double_CB(var, mean, params, name)
        
        if constant:
            for p in model.pars():
                p.setConstant(True)

        return model

    def draw_CB(var, mean, params, prefix = "", suffix = ""):
        '''
        Returns a Crystal Ball PDF for the given var and the given parameters

        :var:    RooRealVar, mass variable to fit
        :mean:   RooRealVar, mean of the CB
        :params: dict, where the keys are the parameters of the CB (a, n, sigma) and the values are arrays with the mean and (optionally) limits of the parameters.
        :prefix: str, prefix for the names of the parameters
        :suffix: str, suffix for the names of the parameters
        '''
        printinfo("Creating {} variable".format(prefix+"sigma"+suffix))

        sigma = create_var(prefix+"sigma"+suffix, *params["sigma"])
        a     = create_var(prefix+"a"+suffix    , *params["a"])
        n     = create_var(prefix+"n"+suffix    , *params["n"])

        return SingleModel.create_pdf(ROOT.RooCBShape, "CB"+suffix, var, mean, sigma, a, n)

    def draw_double_CB(var, mean, params, name = "DoubleCB", prefix = ""):
        '''
        Draw a double Crystal Ball, i.e., two crystal balls added, with the same mean, and one with a positive alpha parameter and the other with negative alpha parameter

        :var:    RooRealVar, mass variable to fit
        :mean:   RooRealVar, shared mean of the two CBs
        :params: dict, where the keys are the parameters of the two CBs, each one with the suffix "1" and "2", respectively
        :name:   str, name of the final PDF
        :prefix: str, prefix for the name
        '''
        printinfo("Creating double Crystal Ball with the following set of parameters: {}".format(params))
        params1, params2 = {}, {}
        for k,v in list(params.items()):
            if k[-1] == "1":
                params1[k[:-1]] = v
            elif k[-1] == "2":
                params2[k[:-1]] = v
            else:
                raise RuntimeError("Parameters of the double CB must end in '1' or '2', corresponding to each CB")
        
        if prefix != "": prefix += "_"
        printinfo("Creating first Crystal Ball with the following set of parameters: {}".format(params1))
        CB1 = SignalModel.draw_CB(var, mean, params1, prefix, "1")
        printinfo("Creating second Crystal Ball with the following set of parameters: {}".format(params2))
        CB2 = SignalModel.draw_CB(var, mean, params2, prefix, "2")
        
        coef = create_var('coef', 0.5, 0, 1)

        comb = CombinedModel(prefix+name, [CB1, CB2], [coef])

        return comb

    def draw_ipatia(var, mean, params, suffix):
        ROOT.gROOT.LoadMacro("{}/RooHypatia.cpp+".format(os.path.dirname(os.path.abspath(__file__))))
        printdebug("Parameters = {}".format(params))
        l     = create_var("l"+suffix    , *params["l"])
        zeta  = create_var("zeta"+suffix , *params["zeta"])
        beta  = create_var("beta"+suffix , *params["beta"])
        sigma = create_var("sigma"+suffix, *params["sigma"])
        a1    = create_var("a1"+suffix   , *params["a1"])
        n1    = create_var("n1"+suffix   , *params["n1"])
        a2    = create_var("a2"+suffix   , *params["a2"])
        n2    = create_var("n2"+suffix   , *params["n2"])
        
        return SingleModel.create_pdf(ROOT.RooHypatia,"Ipatia"+suffix,var, l, zeta, beta, sigma, mean, a1, n1, a2, n2)

class BkgModels:
    __model_name__ = "bkg"
    def draw_exp(var, params, name = "bkg"):
        '''
        Returns an exponential to fit the background

        :var: RooRealVar, variable we want to fit
        :params: array, values of the lims and/or the central value
        '''
        indx = create_var("indx", *params["indx"])

        printdebug("Creating exponential with name {}".format(name))

        return SingleModel.create_pdf(ROOT.RooExponential, name, var, indx)

    def sidebands(var, indx_lims, yield_param, name = "", name_bkg = "bkg"):
        '''
        Returns an exponential to fit the sidebands, with a hole in the Signal region defined in the definitions module

        :var:         RooRealVar, variable we want to fit
        :indx_lims:   array, values of the lims and/or the central value
                      for the index in the exponential
        :yield_param: RooRealVar, yield parameter for the fit
        :name:        str, name for the RooExtendPdf object
        :name_bkg:    str, name for the RooExponential object
        '''
        indx = create_var("indx", *indx_lims)
        bkg  = ROOT.RooExponential(name_bkg, name_bkg, var, indx)

        return SingleModel.create_extended_pdf(name, var, bkg, yield_param, "Signal", indx)

def create_main_model(data, mass, fit_params, triggercat, nsig):
    from utils.fit_utils    import create_yield_param, mass_values, create_var
    from ROOT               import RooFormulaVar, RooArgList

    nentries = data.sumEntries()
    nentries = max(nentries, 100) ## cannot have nentries = 0 for the fit to work
    nbkg = create_yield_param(nentries, name = "nbkg")

    printinfo("Create bkg model for data")
    cbkg_model = BkgModels.draw_exp(var    = mass.var,
                                    params = fit_params["bkg"],
                                    name   = "bkg")

    printinfo("Creating signal model for data")
    sig_mean  = create_var("mean", *fit_params["signal"]["mean"])
    sig_model = SignalModel.draw_ipatia(var    = mass.var,
                                        mean   = sig_mean,
                                        params = fit_params["signal"],
                                        suffix = "")

    printinfo("Creating combined model")
    model = CombinedModel(name   = "model",
                          models = [sig_model, cbkg_model],
                          yields = [nsig, nbkg])

    return model
