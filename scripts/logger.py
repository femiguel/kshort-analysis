import sys
import os
import inspect
import logging

if sys.version_info >= (3, ):
    unicode_type = str
    basestring_type = str
    xrange = range
else:
    # The names unicode and basestring don't exist in py3 so silence flake8.
    unicode_type = unicode  # noqa
    basestring_type = basestring  # noqa  

_TO_UNICODE_TYPES = (unicode_type, type(None))

def to_unicode(value):
    """
    Converts a string argument to a unicode string.
    If the argument is already a unicode string or None, it is returned
    unchanged.  Otherwise it must be a byte string and is decoded as utf8.
    """
    if isinstance(value, _TO_UNICODE_TYPES):
        return value
    if not isinstance(value, bytes):
        raise TypeError(
            "Expected bytes, unicode, or None; got %r" % type(value))
    return value.decode("utf-8")


def _safe_unicode(s):
    try:
        return to_unicode(s)
    except UnicodeDecodeError:
        return repr(s)
                                    
CSI = '\033['
class AnsiCodes(object):
    def __init__(self):
        # the subclasses declare class attributes which are numbers.
        # Upon instantiation we define instance attributes, which are the same
        # as the class attributes but wrapped with the ANSI escape sequence
        for name in dir(self):
            if not name.startswith('_'):
                value = getattr(self, name)
                setattr(self, name, self.code_to_chars(value))               
                
    def code_to_chars(self, code):
        return CSI + str(code) + 'm'

class ForegroundColors(AnsiCodes):
    BLACK = 30
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    WHITE = 37
    RESET = 39

DEFAULT_FORMAT      = '%(color)s[%(asctime)s %(module)s:%(lineno)d] - %(levelname)s:%(end_color)s %(message)s'
#DEFAULT_DATE_FORMAT = '%y%m%d %H:%M:%S'
DEFAULT_DATE_FORMAT = None
DEFAULT_COLORS      = {
    logging.DEBUG    : ForegroundColors().CYAN,
    logging.INFO     : ForegroundColors().GREEN,
    logging.WARNING  : ForegroundColors().YELLOW,
    logging.ERROR    : ForegroundColors().RED,
    logging.CRITICAL : ForegroundColors().RED
    }
class LogFormatter(logging.Formatter):
    """
    Log formatter used in Tornado. Key features of this formatter are:
    * Color support when logging to a terminal that supports it.
    * Timestamps on every log line.
    * Robust against str/bytes encoding problems.
    """
    def __init__(self,
                 color   = True,
                 fmt     = DEFAULT_FORMAT,
                 datefmt = DEFAULT_DATE_FORMAT,
                 colors  = DEFAULT_COLORS):
        r"""
        :arg bool color: Enables color support.
        :arg string fmt: Log message format.
        It will be applied to the attributes dict of log records. The
        text between ``%(color)s`` and ``%(end_color)s`` will be colored
        depending on the level if color support is on.
        :arg dict colors: color mappings from logging level to terminal color
        code
        :arg string datefmt: Datetime format.
        Used for formatting ``(asctime)`` placeholder in ``prefix_fmt``.
        .. versionchanged:: 3.2
        Added ``fmt`` and ``datefmt`` arguments.
        """
        logging.Formatter.__init__(self, datefmt=datefmt)
        
        self._fmt = fmt
        self._colors = {}
        self._normal = ''
        
        if color:
            self._colors = colors
            self._normal = ForegroundColors().RESET

    def format(self, record):
        try:
            message = record.getMessage()
            assert isinstance(message,
                              basestring_type)  # guaranteed by logging
            record.message = _safe_unicode(message)
        except Exception as e:
            record.message = "Bad message (%r): %r" % (e, record.__dict__)

        record.asctime = self.formatTime(record, self.datefmt)

        if record.levelno in self._colors:
            record.color = self._colors[record.levelno]
            record.end_color = self._normal
        else:
            record.color = record.end_color = ''

        try:
            funcname = record.__dict__["funcName"]
            stack = inspect.stack()
            frame = list(filter(lambda f: funcname in f.code_context[0], stack))
            path_to_module = frame[0].filename
            record.module = path_to_module[path_to_module.rfind("/")+1:]
            record.lineno = frame[0].lineno
        except:
            pass
            
        formatted = self._fmt % record.__dict__
            
        if record.exc_info:
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            # exc_text contains multiple lines.  We need to _safe_unicode
            # each line separately so that non-utf8 bytes don't cause
            # all the newlines to turn into '\n'.
            lines = [formatted.rstrip()]
            lines.extend(
                _safe_unicode(ln) for ln in record.exc_text.split('\n'))
            formatted = '\n'.join(lines)

        return formatted.replace("\n", "\n    ")
