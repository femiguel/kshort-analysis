'''
Module that sets up all the selections needed in this analysis.
It feeds from the utils.selutils module, where many useful methods are created.
'''
from utils.selutils  import *
from utils.ana_utils import __blind_region__
from utils.ana_utils import get_daughters_names as _daughters
from utils.ana_utils import get_mother_name     as _mother

cuts = {"l0seltis"          : l0sel("TIS"),
        "l0selxtos"         : l0sel("TOS"),
        "hlt1sel"           : hlt1sel(),
        "hlt2sel"           : hlt2sel(),
        "trigseltis"        : triggersel("TIS"),
        "trigselxtos"       : triggersel("TOS"),
        "mctruthsig"        : mcMatch(mother    = _mother(data_type = "MC", channel = "K0S2mu4"),
                                      daughters = _daughters(data_type = "MC", channel = "K0S2mu4")),
        "mctruthnorm"       : mcMatch(mother    = _mother(data_type = "MC", channel = "K0S2pi2"),
                                      daughters = _daughters(data_type = "MC", channel = "K0S2pi2")),
        "trackcutssig"      : track_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2mu4"),
                                         isMC      = False),
        "trackcutssigMC"    : track_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2mu4"),
                                         isMC      = True),
        "trackcutsnorm"     : track_cuts(daughters = _daughters(data_type = "Data", channel = "K0S2pi2"),
                                         isMC      = False),
        "trackcutsnormMC"   : track_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2pi2"),
                                         isMC      = True),
        "extracuts_normMC"  : norm_cuts(data_type = "MC"),
        "extracuts_norm"    : norm_cuts(data_type = "Data"),
        "inAcc"             : additional_daughters_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2mu4"),
                                                        cut = "InMuonAcc==1"),
        "isMuon"            : additional_daughters_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2mu4"),
                                                        cut = "isMuon==1"),
        "pidmu"             : additional_daughters_cuts(daughters = _daughters(data_type = "MC", channel = "K0S2mu4"),
                                                        cut = "PIDmu>0"),
        "strippingsig"      : stripping(line = "K0s2MuMuMuMuLine"),
        "strippingnorm"     : stripping(line = "Ks2PiPiForRnSLine"),
        "strippingnorm_nopt": stripping(line = "Ks2PiPiForRnSNoPtCutsLine"),
        "blind"             : Selection("(K0S_M<{0})||(K0S_M>{1})".format(*__blind_region__)),
        }
