import os

from scripts import printinfo, printdebug, printwarning, printerror

from utils.ana_utils import years, trigger_categories

def prospects_graphics(classifier, classifier_eff):
    '''
    Create the prospects graphics plots
    '''
    from scripts.expected_lim import prepare_fit_data, create_fake_unblind, temp_alpha, compute_expected_limit
    from scripts.fits         import setup_signal_parameters, fit_main
    from utils.ana_utils      import get_mother_name, get_ana_filenames
    from utils.mva_utils      import get_filenames
    from utils.pyutils        import load_data_file, file_exists

    alpha, s0, alphaTIS, alphaTOS, sL0TIS, sL0TOS = temp_alpha()

    alphas = {"alpha"    : [alpha*1e12, alpha*s0*1e12],
              "alphaTIS" : [alphaTIS/alpha, sL0TIS*alphaTIS/alpha],
              "alphaxTOS": [alphaTOS/alpha, sL0TOS*alphaTOS/alpha]}

    mother = get_mother_name(data_type = "Data", channel = "K0S2mu4")

    ## Fix the Ipatia parameters for the fit
    parameters_filename = get_ana_filenames("SignalFitParameters")
    if file_exists(parameters_filename):
        signal_parameters = load_data_file(parameters_filename)
    else:
        signal_parameters = setup_signal_parameters(classifier, classifier_eff, parameters_filename)

    lumis_array = [1, 3, 5, 10, 20, 30, 40, 50, 75, 100, 125, 150, 200, 250, 300]

    prospects_filenames = get_filenames("Prospects")
    mother = get_mother_name(data_type = "Data", channel = "K0S2mu4")

    files_unblind, trees_unblind = {},{}
    array_limits = {}
    for lumi in lumis_array:
        for triggercat in trigger_categories:
            ifilename, itreename = prepare_fit_data(triggercat, classifier[triggercat], classifier_eff[triggercat])
            files_unblind[triggercat], trees_unblind[triggercat] = create_fake_unblind(ifilename = ifilename,
                                                                                       itreename = itreename,
                                                                                       mother    = mother,
                                                                                       ofilename = prospects_filenames.format(**locals()),
                                                                                       otreename = "DecayTree_{}".format(triggercat),
                                                                                       lumi      = lumi,
                                                                                       sigma     = 1)

        x, y = fit_main(ifilenames = files_unblind,
                        itreenames = trees_unblind,
                        alphas     = alphas,
                        ipa_params = signal_parameters,
                        ofilename  = None,
                        unblind    = True,
                        show       = False)

        lim = compute_expected_limit(x, y)
        printinfo("limit = {}".format(lim))

        array_limits[lumi] = lim

    printinfo("Array of limits: {}".format(array_limits))



if __name__ == "__main__":
    from scripts import setup_debug
    from utils.mva_utils import MVAInfo
    
    setup_debug()

    prospects_graphics(classifier = MVAInfo.optimal_classifiers,
                       classifier_eff = MVAInfo.optimal_efficiency)
