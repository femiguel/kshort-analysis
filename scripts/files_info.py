'''
Module that contains the basic information of the original tuples used in the analysis.
'''
import os
import functools

from scripts import get_original_tuples_path, printerror, printinfo, printdebug, printwarning

years = ["2016", "2017", "2018"]
_join = lambda f: os.path.join(get_original_tuples_path(), "OriginalTuples", f)

info = {"Files":{
    "K0S2mu4":{"Data":{year: _join("Data{}.root".format(year)) for year in years},
               "MC"  :{year: _join("MC_K0S2mu4_{}.root".format(year)) for year in years},
               },
    "K0S2pi2":{"Data":{year: _join("MB_Data{}.root".format(year)) for year in years},
               "MC"  :{year: _join("MC_K0S2pi2_{}.root".format(year)) for year in years},
               }
    },
        
        "Trees":{
    "K0S2mu4":{"Data":{year:{"K0S2mu4"         : "K0s2MuMuMuMuLine/DecayTree",
                             "TriggerTestLine" : "TriggerTestLine/DecayTree",
                             "K0S2pi2"         : "Ks2PiPiForRnSLine/DecayTree"} for year in years},
               "MC"  :{year:{"MCTruth" : "MCDecayTreeTuple/MCDecayTree",
                             "Reco"    : "RecoTuple_NoPIDsMuons/DecayTree",
                             "K0S2mu4" : "K0s2MuMuMuMuLine/DecayTree",
                             "K0S2pi2" : "Ks2PiPiForRnSLine/DecayTree"} for year in years}
               },
    "K0S2pi2":{"Data":{year:{"K0S2pi2"         : "Ks2PiPiForRnSLine/DecayTree",
                             "K0S2pi2_NoPTCuts": "Ks2PiPiForRnSNoPtCutsLine/DecayTree",
                             "TriggerTestLine" : "TriggerTestLine/DecayTree",
                             "K0S2mu4"         : "K0s2MuMuLine/DecayTree",
                             "K0S2PiMu"        : "K0s2PiMuLine/DecayTree"} for year in years},
               "MC"  :{year:{"MCTruth"         : "MCDecayTreeTuple/MCDecayTree",
                             "Reco"            : "RecoTuple_NoPIDsParticles/DecayTree",
                             "K0S2pi2"         : "Ks2PiPiForRnSLine/DecayTree",
                             "K0S2pi2_NoPTCuts": "Ks2PiPiForRnSNoPtCutsLine/DecayTree",
                             "K0S2PiMu"        : "K0s2PiMuLine/DecayTree",
                             "K0S2mu4"         : "K0s2Mu4Custom/DecayTree"} for year in years}
               }
    }
        }

def check_params(func):
    '''
    Decorates a function with input parameters "data_type", "year" and "channel"
    and checks that they are correct.
    '''
    @functools.wraps(func)
    def wrapper(data_type, year, channel, **kwargs):
        '''
        Internal wrapper
        '''
        if data_type not in ["Data", "MC"]:
            printerror("Input parameter 'data_type' must be 'Data' or 'MC'. Input was {} on function {} defined in module {}".format(data_type, func.__name__, func.__globals__["__file__"]))
        if str(year) not in years:
            printerror("Input parameter 'year' must be '2016', '2017', '2018'. Input was {} on function {} defined in module {}.".format(year, func.__name__, func.__globals__["__file__"]))
        if channel not in list(info["Files"].keys()):
            printerror("Files for channel {} have not been implemented. Error located in function {} defined in module {}.".format(channel, func.__name__, func.__globals__["__file__"]))

        return func(data_type = data_type,
                    year      = str(year),
                    channel   = channel,
                    **kwargs)

    return wrapper

@check_params
def get_filename(data_type, year, channel):
    '''
    Function used to retrieve the original file name of a given set of data type, year and channel.
    It also outputs the relevant names of the TTrees (for MC, they are MC-DecayTree, the Reconstruction TTree, and the stripping TTree, while for Data, they are the stripping TTrees)
    The wrapper check_params() checks that the three input parameters are correct
    
    :data_type: str, can be "MC" or "Data"
    :year:      str or int, can be 2016, 2017 or 2018
    :channel:   str, can be "K0S2mu4" or "K0S2pi2"
    '''
    fname = info["Files"][channel][data_type][year]
    trees = info["Trees"][channel][data_type][year]
    tnames = []
    if data_type == "MC":
        tnames += [trees["MCTruth"], trees["Reco"]]
                   
    tnames += [trees[channel]]
    if channel == "K0S2pi2":
        tnames += [trees["K0S2pi2_NoPTCuts"]]

    return fname, tnames


if __name__ == "__main__":
    from utils.pyutils import save_data_file

    save_data_file("files_info.yaml", info)
