from utils.fit_utils import MassRange, MassVar, create_dataset, create_var
from scripts         import printinfo, printerror, printwarning

class Parameters:
    def __init__(self,mode):
        self.mode   = mode
        self.params = {}
        if mode == "K0S2mu4":
            pass
        elif mode == "K0S2pi2":
            self.params["signal"] = {"a1"    : [2, 1, 5],
                                     "n1"    : [5, 1, 10],
                                     "sigma1": [5, 1, 10],
                                     "a2"    : [-2, -5, -1],
                                     "n2"    : [5, 1, 10],
                                     "sigma2": [5, 1, 10]}
            self.params["bkg"] = {"indx": [-0.5, 0.5]}

    def get_params(self, fix_params_from_mc, mcfilename = None):
        '''
        Returns the parameters from the corresponding mode. If indicated to fix parameters from MC, it will fit the respective MC TTree and return the parameters from that.
        
        :fix_params_from_mc: bool, if True, do the MC fits
        :mcfilename:         str, path to the MC filename
        '''
        from scripts import fit_models
        
        if fix_params_from_mc:
            if mcfilename is None:
                raise RuntimeError("If parameters are to be fixed, a RooDataSet object must be provided")

            printinfo("Fixing parameters from MC")
            mass    = get_mass("KS0")
            mc_data = create_dataset(filename  = mcfilename,
                                     treename  = "Ks2PiPiForRnSLine/DecayTree",
                                     variables = [mass.var],
                                     cuts      = None)

            printinfo("Created MC dataset")

            mean     = create_var("mc_mean", __mass_values__["KS0"], *__mass_lims__["KS0"])
            mc_model = fit_models.SignalModel.default(var = mass.var,
                                                      mean = mean,
                                                      params = self.params["signal"],
                                                      name   = "MC_K0S2pi2")
            printinfo("Created MC model")
            #cat      = fit_models.Category("MC_K0S2pi2_Fit", model = mc_model, data = mc_data)
            from ROOT import RooFit
            res = mc_model.pdf.fitTo(mc_data, RooFit.Range("FullRange"), RooFit.Save())

            params = {}
            for k in range(len(mc_model.models)):
                model_pars = mc_model.models[k].params
                for p in list(model_pars):
                    if "mean" in p.GetName(): continue                    
                    params[p.GetName()] = [p.getVal(), self.params["signal"][p.GetName()][1], self.params["signal"][p.GetName()][2]] if "sigma" in p.GetName() else [p.getVal()]
            printinfo("Final form of the parameters reads: {}".format(params))
            return {"signal": params, "bkg": self.params["bkg"]}
            
        else:
            params = {"signal": {}, "bkg": self.params["bkg"]}
            for k,v in list(self.params["signal"].items()):
                if "sigma" in v:
                    params["signal"][k] = v
                else:
                    params["signal"][k] = v[0]

            return params
               

