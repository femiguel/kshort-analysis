'''
Module to compute the MC corrections used in this analysis.
'''
import os
import numpy
import re
import ROOT

from array           import array
from random          import random, seed
from scripts         import printinfo, printwarning, printerror, printdebug, get_project_path
from utils.rootutils import input_tree, mctruth_prepare, copytree, add_variables_into_tree, get_branch_titles
#from ROOT            import TFile, TTree

seed(20)

class HorizontalCorrections:
    def __init__(self, name, opMC, opData):
        self.name   = name
        self.opMC   = opMC
        self.opData = opData

    def __call__(self, X):
        return self.opData.inverse(self.opMC(X))

    def Draw(self):
        from ROOT import TCanvas, TGraph, kRed

        canvas = TCanvas()
        gr     = TGraph()
        ys     = map(self, self.opMC.xaxis)
        for i in range(ley(ys)):
            g.SetPoint(i+1, self.opMC.xaxis[i], ys[i])
        g.GetXaxis().SetTitle(self.name)
        g.GetYaxis().SetTitle(self.name + "_Corr")
        g.SetLineColor(kRed)
        g.Draw("AL*")
        canvas.SetGridx()
        canvas.SetGridy()

        input("Waiting for approval. Press ENTER to continue.")

@input_tree
@mctruth_prepare
def addArmenteros(itree, ofilename, otreename, odir, data_type, **kwargs):
    '''
    Add Armenteros variables to remove the Lambda -> p pi- contribution
    from KS->pi+ pi-

    :itree:     TTree, input tree
    :ofilename: str, path to the output file
    :otreename: str, name of the output TTree
    :odir:      str, name of the output directory
    '''
    newbranchnames = ["AP_pt", "AP_alpha"]

    printinfo("Adding Armenteros variables")

    copytree(itree     = itree,
             ofilename = ofilename,
             otreename = otreename,
             odir      = odir)

    add_variables_into_tree(ifilename = ofilename,
                            itreename = os.path.join(odir, otreename),
                            variables = newbranchnames,
                            data_type = data_type)

@input_tree
@mctruth_prepare
def cutArmenteros(itree, ofilename, otreename, odir, data_type, **kwargs):
    '''
    Apply Armenteros cut to remove the Lambda -> p pi- contribution from
    KS-> pi+ pi- data.

    :itree:     TTree, input tree
    :ofilename: str, path to the output file
    :otreename: str, name of the output TTree
    :odir:      str, name of the output directory
    :data_type: str, ("MC", "Data")
    '''
    from scripts.physics import read_pdg
    from utils.ana_utils import get_mother_name
    
    pdg = read_pdg()
    pname = "{}_P".format(get_mother_name(channel = "K0S2pi2", data_type = data_type))

    M_lambda = pdg.GetParticle("Lambda0").Mass()*1000
    M_proton = pdg.GetParticle("proton").Mass()*1000
    M_pi     = pdg.GetParticle("pi-").Mass()*1000

    p_star   = ((M_lambda**2 - M_proton**2 - M_pi**2)**2 - (2*M_proton*M_pi)**2)*1./(2*M_lambda)**2
    m_ratio  = (M_proton**2 - M_pi**2)*1./M_lambda**2
    Q        = "{0}*{2}*1./(2*sqrt({1})*sqrt({2}**2 + {0}**2))".format(M_lambda, p_star, pname)

    printdebug("p star = {}".format(p_star))
    printdebug("Mass ratio = {}".format(m_ratio))
    
    cut = "abs(((min(abs(AP_alpha+{0}), abs(AP_alpha-{0}))*{1})**2 + AP_pt**2/{2}) - 1) > 0.3".format(m_ratio, Q, p_star)
    if hasattr(itree, "VeloMatterVeto"):
        cut += " && (VeloMatterVeto>0.6)"

    copytree(itree     = itree,
             ofilename = ofilename,
             otreename = otreename,
             odir      = odir,
             sel       = cut)

@input_tree
@mctruth_prepare
def correct(itree, ofilename, otreename, odir, correction, **kwargs):
    '''
    Main function to apply MC corrections

    :itree:      TTree, input tree
    :ofilename:  str, path to the output file
    :otreename:  str, name of the output TTree
    :odir:       str, name of the output directory
    :correction: str, correction to be applied ("AcceptReject", "HorizontalCorrection")
    '''
    from utils.rootutils import type_correspondence
    from utils.pyutils   import load_data_file, save_data_file, file_exists
    from utils.ana_utils import get_daughters_names

    branchtitles = get_branch_titles(itree       = itree,
                                     branchnames = "*")

    branchnames, branchtypes = [], {}
    oldbranchvalues, newbranchvalues = {},{}
    for br in branchtitles:
        brname, brtype = br.split("/")

        oldbranchvalues[brname], newbranchvalues[brname] = [array(type_correspondence[brtype], [0]) for _ in range(2)]
        branchnames        += [brname]
        branchtypes[brname] = brtype
        itree.SetBranchAddress(brname, oldbranchvalues[brname])

    branches_horcor = []
    if correction == "AcceptReject":
        probmax = itree.GetMaximum("WeightValues")
    elif correction == "HorizontalCorrection":
        if "year" not in kwargs or "channel" not in kwargs:
            printerror("To set up horizontal corrections, please input 'year' and 'channel' parameters")

        year     = kwargs["year"]
        channel  = kwargs["channel"]
        pkl_path = os.path.join(get_project_path(), "scripts/HorizontalCorrectors_{}.pickle".format(year))

        if not file_exists(pkl_path):
            printinfo("Setting up horizontal correctors")
            setup_HorizontalCorrectors(year      = year,
                                       ofilename = pkl_path)

        ops = load_data_file(pkl_path)

        for key in ops.keys():
            aux       = key + "_Corr"
            particles = get_daughters_names(channel, data_type="MC")

            for par in particles:
                brname = aux.replace("d1", par)
                branches_horcor        += [brname]
                branchtypes[brname]     = branchtypes[brname.replace("_Corr", "")]
                newbranchvalues[brname] = array(type_correspondence[branchtypes[brname]], [0])
        branchnames += branches_horcor
    else:
        printerror("Correction '{}' not implemented".format(correction))

    printdebug("Creating output file")

    ofile = ROOT.TFile.Open(ofilename, "update")
    if odir != "":
        if not ofile.Get(odir):
            ofile.mkdir(odir).cd()
        else:
            ofile.Get(odir).cd()
    otree = ROOT.TTree(otreename, otreename)
    newbranches = {br: otree.Branch(br, newbranchvalues[br], br+"/"+branchtypes[br]) for br in branchnames}

    printfreq = int(itree.GetEntries()/15)
    
    for i,row in enumerate(itree):
        if i%printfreq==0:
            printdebug("Filling row {}".format(i))
        if correction == "AcceptReject":
            P = getattr(row, "WeightValues")
            if P < probmax*random(): continue
        for br in branchnames:
            if br in branches_horcor:
                old_brname = br.replace("_Corr", "")
                if channel == "K0S2pi2":
                    ops_brname = re.sub(r"pi[1-2]|d2", "d1", old_brname)
                else:
                    ops_brname = re.sub(r"mu[1-4]", "d1", old_brname)

                newbranchvalues[br][0] = ops[ops_brname](oldbranchvalues[old_brname][0])
            else:
                newbranchvalues[br][0] = oldbranchvalues[br][0]
            #newbranches[br].Fill()
        otree.Fill()
    printdebug("Writing output file with {} events".format(otree.GetEntries()))
    otree.Write()
    ofile.Close()

def setup_HorizontalCorrectors(year, ofilename):
    '''
    Function that sets up the horizontal corrections that will then be used to create the corrected track variables.
    If necessary, it will produce the corrected normalization tuples before setting up the actual objects.

    :year:      str or int, can be 2016, 2017 or 2018
    :ofilename: str, path to the output pickle file where the objects will be stored
    '''
    from scripts.files_info import get_filename
    from scripts.mc_correct import HorizontalCorrections ## this is because we store in a pickle file an HorizontalCorrections object, so it must have the root of the class to be able to be imported from another module
    from utils.pyutils      import UniFunc, file_exists, get_filename_from_path, get_path_from_filename, save_data_file
    from utils.file_utils   import setup_tuples, get_ofilename

    ofilenames, otreenames = {}, {}
    for data_type in ("MC", "Data"):
        ifilename, itreenames = setup_tuples(data_type = data_type,
                                             year      = year,
                                             channel   = "K0S2pi2")
        for func, corr in [(addArmenteros,"addArmenteros"), (cutArmenteros, "cutArmenteros"), (correct, "AcceptReject")]:
            if corr == "AcceptReject" and data_type == "Data": continue

            stepfilename = get_ofilename(corr, ifilename)
            if not file_exists(stepfilename):
                for itreename in itreenames:
                    func(ifilename  = ifilename,
                         itreename  = itreename,
                         ofilename  = stepfilename,
                         otreename  = get_filename_from_path(itreename),
                         odir       = get_path_from_filename(itreename),
                         data_type  = data_type,
                         correction = corr
                         )
                     
            ifilename = stepfilename

        ofilenames[data_type] = ifilename
        otreenames[data_type] = itreenames

    printdebug("Input files to compute Horizontal Corrections are: {}".format(ofilenames))
    
    varnames = ["ProbNNghost", "TRACK_GhostProb", "TRACK_CHI2NDOF"]
    vars_correct_mc   = ["pi1_{}".format(name) for name in varnames]
    vars_correct_data = ["d1_{}".format(name) for name in varnames]

    treename = list(filter(lambda x: "NoPtCuts" in x, otreenames["MC"]))
    printdebug("Input treename for Horizontal Corrections is {}".format(treename))
    mc_vars_array = get_rootfile_as_dataframe(ifilename  = ofilenames["MC"],
                                              itreenames = treename,
                                              branches   = vars_correct_mc)

    data_vars_array = get_rootfile_as_dataframe(ifilename  = ofilenames["Data"],
                                                itreenames = treename,
                                                branches   = vars_correct_data)

    ops = {}
    for k,var in enumerate(vars_correct_data):
        mc_varname = vars_correct_mc[k]

        ops[var] = HorizontalCorrections(name   = var,
                                         opMC   = UniFunc(mc_vars_array[mc_varname]),
                                         opData = UniFunc(data_vars_array[var]))

    printdebug("Horizontal Corrections computed, saving in output file {}".format(ofilename))

    save_data_file(ofilename, ops)

