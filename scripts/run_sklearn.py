import os
import pandas
from scripts import printinfo, printwarning, printerror, printdebug

from utils.mva_utils import MVAInfo
from utils.mva_utils import setup_mvatuples

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from xgboost import XGBClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingRandomSearchCV
from sklearn.metrics import roc_auc_score,accuracy_score
from sklearn import clone
from joblib import dump, load


@setup_mvatuples
def get_samples(mode, triggercat, variables, spectators, nfolds):
    from utils.rootutils import get_rootfile_as_dataframe
    from utils.mva_utils import get_filenames
    from sklearn.utils   import shuffle
    import pandas
    ntuples = []
    for iteration in range(nfolds):
        ntuples_iter = []
        for name in ["Signal", "Bkg"]:
            _ntuple = get_rootfile_as_dataframe(ifilename  = get_filenames(mode).format(**locals()),
                                                itreenames = ["DecayTree_Iter{}".format(iteration)],
                                                branches   = variables+spectators)
            _ntuple["is_sig"] = int(name=="Signal")

            ntuples_iter += [_ntuple]

        _ntuple_iter = pandas.concat(ntuples_iter, ignore_index = True)
        _ntuple_iter = shuffle(_ntuple_iter, random_state=10).copy()

        ntuples += [_ntuple_iter]

    return ntuples, variables, spectators, nfolds

search_space1 = {'classifier': [GradientBoostingClassifier()],
                 'classifier__n_estimators': [int(round(x,0))  for x in np.logspace(2,4,num=15)],
                 'classifier__max_features': range(1,11),
                 'classifier__max_depth': range(1,11),
                 'classifier__learning_rate': np.arange(.1,.91,0.1)}#,

search_space2 = {'classifier': [AdaBoostClassifier(DecisionTreeClassifier())],
                 'classifier__n_estimators': [int(round(x,0))  for x in np.logspace(2,4,num=15)],
                 'classifier__base_estimator__max_features': range(1,11),
                 'classifier__base_estimator__max_depth': range(1,11),
                 'classifier__learning_rate': np.arange(.1,.91,0.1)}

gammas = [0]
gammas.extend(np.logspace(0.55,1.55,num=10)/7)
search_space3 = {
    'classifier': [XGBClassifier(objective='binary:logistic', nthread=1, eval_metric='rmse')],         
    'classifier__min_child_weight': range(1,11),
    'classifier__n_estimators': [int(round(x,0))  for x in np.logspace(2,4,num=15)],
    'classifier__gamma': gammas,
    'classifier__subsample': [0.1*x for x in range(1,11)],
    'classifier__colsample_bytree': [0.1*x for x in range(1,11)],
    'classifier__max_depth': range(1,11),
    'classifier__reg_alpha': np.arange(.1,2.01,0.1),
    'classifier__reg_lambda': np.arange(.1,2.01,0.1),
    'classifier__scale_pos_weight': [1],
    'classifier__learning_rate': np.arange(.1,.91,0.1)}#,

def get_best_model(X_train, y_train, **kwargs):
    printinfo("Doing model scan.")

    if y_train[y_train==0].shape[0] == y_train.shape[0] or y_train[y_train==1].shape[0] == y_train.shape[0]:
        printerror(y_train)

    pipe = Pipeline([('classifier', GradientBoostingClassifier())])
    best_models = []
    for space in [search_space1,search_space2, search_space3]:
        ## for XGBOOST
        if space==search_space3:
            #myratio = X_train[X_train.target==0].shape[0]/X_train[X_train.target==1].shape[0]
            myratio = y_train[y_train==0].shape[0]/y_train[y_train==1].shape[0]
            space['classifier__scale_pos_weight'] = [myratio]

        #################################
        #from scripts.parameter_scan import ParameterSearch
        #rnd_search_cv = ParameterSearch(pipe, space, n_candidates = "exhaust",
        #                                factor = 9, cv = 4, n_jobs = 3)
        #rnd_search_cv.fit(X_train, y_train)

        #printerror("Reached the end of the line")
        #################################
            
        rnd_search_cv = HalvingRandomSearchCV(pipe, space,
                                              n_candidates='exhaust', factor=9,
                                              #min_resources=100,
                                              cv=15, n_jobs=3, verbose = 1, error_score = "raise")

        best_modelp = rnd_search_cv.fit(X_train,
                                        y_train)

        printerror("")
        best_models.append([best_modelp.best_score_,best_modelp])
        printinfo("Done with one space")

    # View best model
    try: best_models.sort(reverse=True)
    except: print("problem with sorting")
    best_model = best_models[0][1]
    print("Best model is")
    print(best_model.best_estimator_.get_params()['classifier'])
    print(best_model.best_score_)

    return best_model

def grid_search(X_train, y_train, **kwargs):
    from sklearn.model_selection import GridSearchCV
    pipe = Pipeline([('classifier', GradientBoostingClassifier())])

    for space in [search_space1, search_space2, search_space3]:
        printinfo("Initiating grid search")
        if space==search_space3:
            #myratio = X_train[X_train.target==0].shape[0]/X_train[X_train.target==1].shape[0]
            myratio = y_train[y_train==0].shape[0]/y_train[y_train==1].shape[0]
            space['classifier__scale_pos_weight'] = [myratio]

        grid_search = GridSearchCV(pipe, space, cv = 2, n_jobs = 6, return_train_score = True)
        best_model  = grid_search.fit(X_train, y_train)

        printinfo("Best model = {}".format(best_model))

    printerror("Grid search ended")

def train_sklearn(triggercat):
    from utils.pyutils   import save_data_file
    from utils.mva_utils import get_filenames

    train, variables, _, nfolds = get_samples(mode       = "Train",
                                              triggercat = triggercat)
    test = get_samples(mode = "Test", triggercat = triggercat)[0]
    for iteration in range(nfolds):
        X_train, y_train = [train[iteration][v] for v in (variables, "is_sig")]
        best_model = get_best_model(**locals())
        best_model = grid_search(**locals())
        model = clone(best_model.best_estimator_)

        fitr  = model.fit(X_train, y_train)

        test[iteration]["Classifier"] = model.predict(test[iteration][variables])
        test[iteration]["ClassifierProba"] = model.predict_proba(test[iteration][variables])[:,1]

        save_data_file(get_filenames("Sklearn_Output").format(**locals()), test[iteration])

    merged_test = pandas.concat([test[iteration] for iteration in range(nfolds)], ignore_index = True)

    save_data_file(get_filenames("Sklearn_MergedTest").format(**locals()), merged_test)
        
def main(triggercat, create_tuples, train, _drawROC, _FOM, _applyClassifier, _drawClassifier):
    from utils.mva_utils import get_filenames
    from utils.pyutils   import file_exists, load_data_file
    
    merged_test_file = get_filenames("Sklearn_MergedTest").format(**locals())
    
    if train or not file_exists(merged_test_file):
        train_sklearn(triggercat)

    merged_test = load_data_file(merged_test_file)
    if _drawROC:
        from sklearn.metrics import confusion_matrix, roc_curve, roc_auc_score

        fpr, tpr, _ = roc_curve(merged_test["is_sig"], merged_test["Classifier"])
        auc = roc_auc_score(merged_test["is_sig"], merged_test["Classifier"])

        printinfo("AUC = {}".format(auc))


def add_arguments(parser):
    parser.add_argument("--triggercat", type = str, default = "TIS",
                        choices = ["TIS", "xTOS"], help = "Trigger category to train")
    parser.add_argument("--makeTuples", action = "store_true", default = False,
                        help = "Create the tuples for the MVA training")
    parser.add_argument("--train", action = "store_true", default = False,
                        help = "Create train and test tuples")
    parser.add_argument("--drawROC", action = "store_true", default = False,
                        help = "Whether or not to draw the ROC Curve")
    parser.add_argument("--applyClassifier", action = "store_true", default = False,
                        help = "Whether or not to apply the classifier to the Apply tuples")
    parser.add_argument("--drawClassifier", action = "store_true", default = False,
                        help = "Draw the classifier distribution.")
    parser.add_argument("--FOM", default = None, choices = ["Punzi", "CLs", "Feldman"],
                        help = "Compute the given figure of merit")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    main(triggercat       = args.triggercat,
         create_tuples    = args.makeTuples,
         train            = args.train,
         _drawROC         = args.drawROC,
         _FOM             = args.FOM,
         _applyClassifier = args.applyClassifier,
         _drawClassifier  = args.drawClassifier)
