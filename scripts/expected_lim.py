import os
import math
import scipy
import numpy

from scripts         import printinfo, printwarning, printerror, printdebug
from utils.rootutils import input_tree
from numpy           import random

random.seed(20)

def temp_alpha():
    alpha = 2.1000424806088696e-12
    s0 = 0.12388385024504696
    alphaTIS = 4.717354254451351e-12
    alphaTOS = 3.771450498550089e-12
    sL0TIS = 0.1
    sL0TOS = 0.21

    return alpha, s0, alphaTIS, alphaTOS, sL0TIS, sL0TOS

def expected_lim(classifier, classifier_eff):
    '''
    Compute the expected limit.

    :classifier:     str, name of the classifier to cut at
    :classifier_eff: float, efficiency of the MVA at which we want to cut
    '''
    from utils.file_utils import get_ofilename
    from utils.mva_utils  import get_filenames
    from utils.pyutils    import file_exists, load_data_file, delete_files
    from utils.ana_utils  import trigger_categories, get_mother_name, get_ana_filenames
    from scripts.fits     import fit_main, fit_signal, setup_signal_parameters
    
    printinfo("Computing expected limit")

    mode = "Apply"
    name = "Bkg"
    num_toys = 120
    
    alpha, s0, alphaTIS, alphaTOS, sL0TIS, sL0TOS = temp_alpha()

    alphas = {"alpha"    : [alpha*1e12, alpha*s0*1e12],
              "alphaTIS" : [alphaTIS/alpha, sL0TIS*alphaTIS/alpha],
              "alphaxTOS": [alphaTOS/alpha, sL0TOS*alphaTOS/alpha]}

    mother = get_mother_name(data_type = "Data", channel = "K0S2mu4")

    ## Fix the Ipatia parameters for the fit
    parameters_filename = get_ana_filenames("SignalFitParameters")
    if file_exists(parameters_filename):
        signal_parameters = load_data_file(parameters_filename)
    else:
        signal_parameters = setup_signal_parameters(classifier, classifier_eff, parameters_filename)

    ## Run the toys with fake unblinding
    fake_unblind_filename = {triggercat: get_filenames("FakeUnblind").format(**locals()) for triggercat in trigger_categories}
    delete_files(*list(fake_unblind_filename.values()))

    array_limits = []
    for toy in range(num_toys):
        printinfo("Running toy {}".format(toy))
        files_unblind, trees_unblind = {}, {}
        for triggercat in trigger_categories:
            ifilename, itreename = prepare_fit_data(triggercat, classifier[triggercat], classifier_eff[triggercat])

            files_unblind[triggercat], trees_unblind[triggercat] = create_fake_unblind(ifilename = ifilename,
                                                                                       itreename = itreename,
                                                                                       mother    = mother,
                                                                                       ofilename = fake_unblind_filename[triggercat],
                                                                                       otreename = "DecayTree_"+str(toy))
            
        x, y = fit_main(ifilenames = files_unblind,
                        itreenames = trees_unblind,
                        alphas     = alphas,
                        ipa_params = signal_parameters,
                        ofilename  = None,
                        unblind    = True,
                        show       = False)

        lim = compute_expected_limit(x, y)
        printinfo("limit = {}".format(lim))

        array_limits += [lim]

    printinfo("Computed limits = {}".format(array_limits))
    printinfo("Median = {}".format(numpy.median(array_limits)))

def compute_expected_limit(x, y):
    from utils.fit_utils import NumericFunction
    
    F = NumericFunction(x, y, points = len(x))
    vals = list(F.database.keys())
    vals.sort()

    S, Norm = 0, 1/sum(y)
    ss = []
    for val in vals:
        S += F(val)*Norm
        ss.append(S)
        
    CF = NumericFunction(ss, x)

    return CF(0.9)

    
@input_tree
def create_fake_unblind(itree, mother, ofilename, otreename, run3 = False, sigma = 0, **kwargs):
    from utils.rootutils import get_entries
    from utils.ana_utils import __blind_region__, __mass_lims__, lumis
    from array           import array
    from ROOT            import TFile, TTree

    L = kwargs["lumi"] if "lumi" in kwargs else sum(lumis.values())

    k, bkgSF = -1e-4, L/sum(lumis.values())
    if run3:
        bkgSF /= 0.0965 ## why this number??

    mass_lims = __mass_lims__["K0S"]
    branchname = mother+"_M"
    Nsb = get_entries(itree = itree,
                      sel   = "({0} > {1}) && ({0} < {2})".format(branchname, *mass_lims)) if not sigma else 1.1479 ## why??
    num = math.exp(k*mass_lims[0]) - math.exp(k*mass_lims[1])
    den = math.exp(k*__blind_region__[0]) - math.exp(k*mass_lims[0]) + math.exp(k*__blind_region__[1]) - math.exp(k*mass_lims[1])
    Nbkg = scipy.random.poisson(num/den*Nsb*bkgSF)
    printdebug("Nbkg = {}".format(Nbkg))

    ofile = TFile.Open(ofilename, "update")
    otree = TTree(otreename, otreename)
    branchvalues = array("d", [0])
    branch = otree.Branch(branchname, branchvalues, branchname+"/D")

    for j in range(Nbkg):
        branchvalues[0] = generate_mass(k, *mass_lims)
        otree.Fill()
        
    otree.Write()
    ofile.Close()

    return ofilename, otreename

def generate_mass(k, lim1, lim2):
    R = math.exp(k*lim1)
    delta_mass = lim2 - lim1
    while 1:
        m = lim1 + delta_mass*random.random()
        y = math.exp(k*m)
        if y/R > random.random(): return m

@input_tree
def getSingleCands(itree, **kwargs):
    from utils.rootutils import check_duplicates
    
    d, d2, l = check_duplicates(itree = itree, mvar = "K0S_M")
    cands = {}
    out = []
    for key in d.keys():
        cands[key] = {}
        for evt in d[key]:
            cands[key][evt] = []

    for entry in itree:
        run, evt = entry.runNumber, entry.eventNumber
        cands[run][evt] += [[random.random(), entry.K0S_M]]

    for run in cands.keys():
        for evt in cands[run].keys():
            cands[run][evt].sort()
            ev = {"eventNumber":evt, "runNumber":run, "K0S_M": cands[run][evt][0][1]}
            out.append(ev)
    return out

def prepare_fit_data(triggercat, classifier, classifier_eff):
    '''
    Prepare the background data for fit by cutting it at a given efficiency for the classifier,
    to remove as much background as possible.

    :triggercat:     str, ("TIS", "xTOS")
    :classifier:     str, name of the classifier to cut at
    :classifier_eff: float, efficiency of the MVA at which we want to cut
    '''
    from utils.rootutils  import get_entries, merge_trees
    from utils.file_utils import get_ofilename
    from utils.mva_utils  import get_filenames
    from utils.ana_utils  import years
    from utils.pyutils    import file_exists

    mode = "Apply"
    name = "Bkg"

    printinfo("Preparing data for fit")
    flist, tlist = [], []
    for year in years:
        fname, tname = prepare_data_fit_by_year(year, triggercat, classifier, classifier_eff)
        if get_entries(ifilename = fname, itreename = tname) > 0:
            flist += [fname]; tlist += [tname]

    ofilename = get_filenames(mode+"Full").format(**locals())
    ofilename = get_ofilename("Fit",get_ofilename("Classifier",ofilename))
    otreename = tname

    if file_exists(ofilename):
        printdebug("File {} already exists".format(ofilename))
        return ofilename, otreename


    if len(flist) > 0:
        printdebug("Merging trees {} from {}".format(tlist, flist))
        merge_trees(ifilenames = flist,
                    itreenames = tlist,
                    ofilename  = ofilename,
                    otreename  = otreename)
    else:
        cmd = "cp {0} {1}".format(fname, ofilename)
        if os.system(cmd) != 0:
            printerror("Failed to run command {}".format(cmd))

    printinfo("Data for fit is now ready.")

    return ofilename, otreename

def prepare_data_fit_by_year(year, triggercat, classifier, classifier_eff):
    '''
    Prepare the background data for a given year for the fit,
    by cutting at a given classifier efficiency so that the maximum amount of background is removed

    :year:           str or int, (2016, 2017, 2018)
    :triggercat:     str, ("TIS", "xTOS")
    :classifier:     str, name of the classifier to cut at
    :classifier_eff: float, efficiency of the MVA at which we want to cut
    '''
    from utils.file_utils import get_ofilename
    from utils.mva_utils  import get_filenames, get_selection, getBDTsel
    from utils.rootutils  import copytree, get_entries
    from utils.pyutils    import file_exists

    printdebug("Preparing data for fit from year {}".format(year))
    mode = "Apply"
    name = "Bkg"

    ifilename = get_filenames(mode+year+"Full").format(**locals())
    ifilename = get_ofilename("Classifier", ifilename)
    itreename = "DecayTree"
    
    ofilename = get_ofilename("Fit", ifilename)
    otreename = "DecayTree"

    if not file_exists(ofilename):
        sel = " || ".join(["({0} && {1})".format(get_selection(name = name, mode = mode, iteration = iteration), 
                                                 getBDTsel(year, triggercat, parity, classifier, classifier_eff)) \
                           for iteration,parity in enumerate(["even", "odd"])])

        func = copytree if get_entries(ifilename = ifilename, itreename = itreename, sel = sel) == 0 else setup_fit_data_with_single_cands
        
        func(ifilename = ifilename,
             itreename = itreename,
             ofilename = ofilename,
             otreename = otreename,
             sel       = sel)
    else:
        printdebug("File {} already exists".format(ofilename))

    
    return ofilename, otreename

@input_tree
def setup_fit_data_with_single_cands(itree, ofilename, otreename, sel, **kwargs):
    import tempfile
    from array import array
    from ROOT import TFile, TTree
    from utils.rootutils import type_correspondence
    branchnames = ["K0S_M"]
    branchtypes = {"K0S_M": "D"}

    with tempfile.TemporaryDirectory() as tmpdir:
        tmpfilename = os.path.join(tmpdir, "eraseme.root")
        tmpfile = TFile.Open(tmpfilename, "recreate")
        tmptree = itree.CopyTree(sel)

    oldbranchvalues, newbranchvalues = {}, {}
    for brname in branchnames:
        brtype = branchtypes[brname]
        oldbranchvalues[brname], newbranchvalues[brname] = [array(type_correspondence[brtype], [0]) for _ in range(2)]
        tmptree.SetBranchAddress(brname, oldbranchvalues[brname])

    cand = getSingleCands(itree=tmptree)[0]

    ofile = TFile.Open(ofilename, "update")
    otree = TTree(otreename, otreename)
    newbranches = {br: otree.Branch(br, newbranchvalues[br], br+"/"+branchtypes[br]) for br in branchnames}
    break_from_loop = False
    for k,row in enumerate(tmptree):
        for br in branchnames:
            if oldbranchvalues[br][0] == cand[br]:
                printdebug("Getting event with {} = {}".format(br, oldbranchvalues[br][0]))
                newbranchvalues[br][0] = oldbranchvalues[br][0]
                break_from_loop = True
        otree.Fill()
        if break_from_loop:
            break

    printdebug("Writing output TTree with {} events".format(otree.GetEntries()))
    otree.Write()
    ofile.Close()

    tmpfile.Close()

if __name__ == "__main__":
    from scripts import setup_debug
    setup_debug()

    classifiers = {"TIS": "BDT", "xTOS": "BDT"}
    classifier_eff = {triggercat: 0.8 for triggercat in classifiers}

    expected_lim(classifier     = classifiers,
                 classifier_eff = classifier_eff)
