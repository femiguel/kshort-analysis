# Written by Philip Ilten and Mike Williams, 20/06/2017.
# Toolset to check if a secondary vertex is consistent with having
# originated from the VELO material. See README for an overview.
import ROOT
import os
from scripts import get_project_path

path = os.path.join(get_project_path(), "scripts/matter_veto/velo.C+")

ROOT.gROOT.LoadMacro(path)

ModuleMaterial = ROOT.ModuleMaterial
FoilMaterial   = ROOT.FoilMaterial
VeloMaterial   = ROOT.VeloMaterial
