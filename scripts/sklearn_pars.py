import os
import functools
import numpy
import threading
import pandas
import time

from scripts         import printinfo, printdebug, printerror, printwarning
from contextlib      import contextmanager
from collections.abc import Mapping

_global_config = {
    "assume_finite": bool(os.environ.get("SKLEARN_ASSUME_FINITE", False)),
    "working_memory": int(os.environ.get("SKLEARN_WORKING_MEMORY", 1024)),
    "print_changed_only": True,
    "display": "diagram",
    "pairwise_dist_chunk_size": int(
    os.environ.get("SKLEARN_PAIRWISE_DIST_CHUNK_SIZE", 256)
        ),
    "enable_cython_pairwise_dist": True,
    }

_DEFAULT_TAGS = {
    "non_deterministic": False,
    "requires_positive_X": False,
    "requires_positive_y": False,
    "X_types": ["2darray"],
    "poor_score": False,
    "no_validation": False,
    "multioutput": False,
    "allow_nan": False,
    "stateless": False,
    "multilabel": False,
    "_skip_test": False,
    "_xfail_checks": False,
    "multioutput_only": False,
    "binary_only": False,
    "requires_fit": True,
    "preserves_dtype": [numpy.float64],
    "requires_y": False,
    "pairwise": False,
    }
class ParameterGrid:
    def __init__(self, param_grid):
        if isinstance(param_grid, Mapping):
            param_grid = [param_grid]

        self.param_grid = param_grid

    def __len__(self):
        from functools import partial, reduce
        import operator

        product = partial(reduce, operator.mul)
        return sum(product(len(v) for v in p.values()) if p else 1 for p in self.param_grid)

    def __iter__(self):
        for p in self.param_grid:
            items = sorted(p.items())
            if not items:
                yield {}
            else:
                keys, values = zip(*items)
                for v in product(*values):
                    params = dict(zip(keys, v))
                    yield params

    def __getitem__(self, ind):
        '''
        Definition to get an item from the parameter grid
        It outputs an array where the keys are the parameters
        and the values are one specific value from that parameter\'s grid
        '''
        for sub_grid in self.param_grid:
            keys, values_lists = zip(*sorted(sub_grid.items())[::-1])
            sizes = [len(v_list) for v_list in values_lists]
            total = numpy.product(sizes)
            if ind >= total:
                ind -= total
            else:
                out = {}
                for key, v_list, n in zip(keys, values_lists, sizes):
                    ind, offset = divmod(ind, n) ## returns ind/n -- divmod(5,5) = (1,0); divmod(7,5) = (1,2)
                    out[key] = v_list[offset]
                return out

        raise IndexError("ParameterGrid index out of range")

class ParameterSampler:
    def __init__(self, param_distributions, n_iter, random_state = None):
        if isinstance(param_distributions, Mapping):
            param_distributions = [param_distributions]

        self.n_iter              = n_iter
        self.random_state        = random_state
        self.param_distributions = param_distributions

    def _is_all_lists(self):
        return all(all(not hasattr(v, "rvs") for v in dist.values()) for dist in self.param_distributions)

    def __iter__(self):
        from sklearn.utils.random import sample_without_replacement

        rng = check_random_state(self.random_state)

        if self._is_all_lists():
            param_grid = ParameterGrid(self.param_distributions)
            grid_size = len(param_grid) ## total combinations of parameter configurations
            n_iter = self.n_iter if grid_size > self.n_iter else grid_size ## number of combinations to be ran in this iteration

            ## selects n_iter parameter combinations out of grid_size
            for i in sample_without_replacement(grid_size, n_iter, random_state=rng):
                yield param_grid[i]

        else:
            printerror("Not all the parameter distributions are lists. Need to implement this part.")

    def __len__(self):
        from functools import partial, reduce
        import operator
        
        if self._is_all_lists():
            product = partial(reduce, operator.mul)
            grid_size = sum(product(len(v) for v in p.values()) if p else 1 for p in self.param_distributions)

            return min(self.n_iter, grid_size)
        else:
            printinfo("not is all lists")
            return self.n_iter

def check_random_state(seed):
    if seed is None or seed is nump.random:
        return numpy.random.mtrand._rand ## Mersene Twister random number

    printerror("Seed not None not implemented")

_threadlocal = threading.local()

def _get_threadlocal_config():
    if not hasattr(_threadlocal, "global_config"):
        _threadlocal.global_config = _global_config.copy()

    return _threadlocal.global_config

def get_config():
    return _get_threadlocal_config().copy()

def set_config(assume_finite               = None,
               working_memory              = None,
               print_changed_only          = None,
               display                     = None,
               pairwise_dist_chunk_size    = None,
               enable_cython_pairwise_dist = None):
    local_config = _get_threadlocal_config()

    if assume_finite is not None:
        local_config["assume_finite"] = assume_finite
    if working_memory is not None:
        local_config["working_memory"] = working_memory
    if print_changed_only is not None:
        local_config["print_changed_only"] = print_changed_only
    if display is not None:
        local_config["display"] = display
    if pairwise_dist_chunk_size is not None:
        local_config["pairwise_dist_chunk_size"] = pairwise_dist_chunk_size
    if enable_cython_pairwise_dist is not None:
        local_config["enable_cython_pairwise_dist"] = enable_cython_pairwise_dist


@contextmanager
def config_context(assume_finite               = None,
                   working_memory              = None,
                   print_changed_only          = None,
                   display                     = None,
                   pairwise_dist_chunk_size    = None,
                   enable_cython_pairwise_dist = None):
    old_config = get_config()
    set_config(assume_finite               = assume_finite,
               working_memory              = working_memory,
               print_changed_only          = print_changed_only,
               display                     = display,
               pairwise_dist_chunk_size    = pairwise_dist_chunk_size,
               enable_cython_pairwise_dist = enable_cython_pairwise_dist)

    try:
        yield
    finally:
        set_config(**old_config)

def delayed(function):
    '''
    Decorator used to capture the arguments of a function
    '''
    @functools.wraps(function)
    def delayed_function(*args, **kwargs):
        return _FuncWrapper(function), args, kwargs
    return delayed_function

class _FuncWrapper:
    def __init__(self, function):
        self.function = function
        self.config   = get_config()
        functools.update_wrapper(self, self.function)

    def __call__(self, *args, **kwargs):
        with config_context(**self.config):
            return self.function(*args, **kwargs)

def clone(estimator, *, safe=True):
    import copy
    
    estimator_type = type(estimator)
    if estimator_type in (list, tuple, set, frozenset):
        return estimator_type([clone(e, safe=safe) for e in estimator])
    elif not hasattr(estimator, "get_params") or isinstance(estimator, type):
        if not safe: ## for None and str objects
            return copy.deepcopy(estimator)
        else:
            printerror("This part of clone() needs to be implemented")

    klass = estimator.__class__
    new_object_params = estimator.get_params(deep=False)
    for name, param in new_object_params.items():
        new_object_params[name] = clone(param, safe=False)
    new_object = klass(**new_object_params)
    params_set = new_object.get_params(deep=False)

    # quick sanity check of the parameters of the clone
    for name in new_object_params:
        param1 = new_object_params[name]
        param2 = params_set[name]
        if param1 is not param2:
            printerror("Cannot clone object {}, as the constructor either does not set or modifies parameter {}".format(estimator, name))
            
    return new_object

def _determine_key_type(key, accept_slice = True):
    dtype_to_str = {int: "int", str: "str", bool: "bool", numpy.bool_: "bool"}
    array_dtype_to_str = {"i": "int",
                          "u": "int",
                          "b": "bool",
                          "O": "str",
                          "U": "str",
                          "S": "str"}

    if isinstance(key, tuple(dtype_to_str.keys())):
        return dtype_to_str[type(key)]
    if hasattr(key, "dtype"):
        return array_dtype_to_str[key.dtype.kind]

    printerror("Determining key type needs to be further implemented")

def _array_indexing(array, key, key_dtyppe, axis):
    from scipy.sparse import issparse

    if issparse(array) and key_dtype == "bool":
        key = numpy.asarray(key)
    if isinstance(key, tuple):
        key = list(key)

    return array[key] if axis == 0 else array[:,key]

def _pandas_indexing(X, key, key_dtype, axis):
    if hasattr(key, "shape"):
        key = numpy.asarray(key)
        key = key if key.flags.writeable else key.copy()
    elif isinstance(key, tuple):
        key = list(key)

    if key_dtype == "int" and not (isinstance(key, slice) or numpy.isscalar(key)):
        return X.take(key, axis = axis)
    else:
        indexer = X.iloc if key_dtype == "int" else X.loc
        return indexer[:,key] if axis else indexer[key]

def _safe_indexing(X, indices, axis = 0):
    indices_dtype = _determine_key_type(indices)

    if hasattr(X, "iloc"):
        return _pandas_indexing(X, indices, indices_dtype, axis = axis)
    elif hasattr(X, "shape"):
        return _array_indexing(X, indices, indices_dtype, axis = axis)

    printerror("Error with safe indexing")

def _safe_tags(estimator, key=None):
    if hasattr(estimator, "_get_tags"):
        tags_provider = "_get_tags()"
        tags = estimator._get_tags()
    elif hasattr(estimator, "_more_tags"):
        tags_provider = "_more_tags()"
        tags = {**_DEFAULT_TAGS, **estimator._more_tags()}
    else:
        tags_provider = "_DEFAULT_TAGS"
        tags = _DEFAULT_TAGS

    if key is not None:
        if key not in tags:
            printerror(f"The key {key} is not defined in {tags_provider} for the class {estimator.__class__.__name__}.")
        return tags[key]
    return tags


def _safe_split(estimator, X, y, indices):
    if _safe_tags(estimator, key = "pairwise"):
        if not hasattr(X, "shape"):
            printerror("Precomputed kernels or affinity matrices have to be passed as arrays or sparse matrices")
        if X.Shape[0] != X.shape[1]:
            printerror("X should be a square kernel matrix")
        X_subset = X[numpy.ix_(indices, indices)]
    else:
        X_subset = _safe_indexing(X, indices)

    y_subset = _safe_indexing(y, indices)

    return X_subset, y_subset

from contextlib import suppress
def _score(estimator, X_test, y_test, scorer, error_score="raise"):
    import numbers
    if isinstance(scorer, dict):
        # will cache method calls if needed. scorer() returns a dict
        scorer = _MultimetricScorer(**scorer)
        
    try:
        if y_test is None:
            scores = scorer(estimator, X_test)
        else:
            scores = scorer(estimator, X_test, y_test)
    except Exception:
        raise

    if isinstance(scores, dict):
        for name, score in scores.items():
            if hasattr(score, "item"):
                with suppress(ValueError):
                    score = score.item()
            if not isinstance(score, numbers.Number):
                raise ValueError("")
            scores[name] = score
    else:  # scalar
        if hasattr(scores, "item"):
            with suppress(ValueError):
                scores = scores.item()
        if not isinstance(scores, numbers.Number):
            raise ValueError("")
    return scores

def _passthrough_scorer(estimator, *args, **kwargs):
    return estimator.score(*args, **kwargs)

def _aggregate_score_dicts(scores):
    import numbers
    
    return {
        key: numpy.asarray([score[key] for score in scores])
        if isinstance(scores[0][key], numbers.Number)
        else [score[key] for score in scores]
        for key in scores[0]
    }

def _fit_and_score(estimator,
                   X_train,
                   y_train,
                   X_test,
                   y_test,
                   scorer,
                   train_indices,
                   test_indices,
                   verbose,
                   parameters,
                   fit_params,
                   return_train_score=False,
                   return_parameters=False,
                   return_n_test_samples=False,
                   return_times=False,
                   return_estimator=False,
                   split_progress=None,
                   candidate_progress=None,
                   error_score=numpy.nan,):
    fit_params = fit_params or {}

    if parameters is not None:
        cloned_parameters = {}
        for k,v in parameters.items():
            cloned_parameters[k] = clone(v, safe = False)

        estimator = estimator.set_params(**cloned_parameters)

    X_train, y_train = _safe_split(estimator, X_train, y_train, train_indices)
    X_test , y_test  = _safe_split(estimator, X_test , y_test , test_indices)

    result = {}
    start_time = time.time()
    try:
        estimator.fit(X_train, y_train, **fit_params)
    except Exception:
        raise
    else:
        result["fit_error"] = None

        fit_time = time.time() - start_time
        test_scores = _score(estimator, X_test, y_test, scorer)
        score_time = time.time() - start_time - fit_time
        if return_train_score:
            train_scores = _score(estimator, X_train, y_train, scorer)

    result["test_scores"] = test_scores
    if return_train_score:
        result["train_scores"] = train_scores
    if return_n_test_samples:
        result["n_test_samples"] = X_test.shape[0]
    if return_times:
        result["fit_time"] = fit_time
        result["score_time"] = score_time
    if return_parameters:
        result["parameters"] = parameters
    if return_estimator:
        result["estimator"] = estimator

    return result

def _normalize_score_results(scores, scaler_score_key="score"):
    """Creates a scoring dictionary based on the type of `scores`"""
    if isinstance(scores[0], dict):
        # multimetric scoring
        return _aggregate_score_dicts(scores)
    # scaler
    return {scaler_score_key: scores}

def _top_k(results, k, itr):
    # Return the best candidates of a given iteration
    iteration,mean_test_score,params = (numpy.asarray(a) for a in (results["iter"],results["mean_test_score"],results["params"]))
    iter_indices   = numpy.flatnonzero(iteration == itr)
    sorted_indices = numpy.argsort(mean_test_score[iter_indices])
    
    return numpy.array(params[iter_indices][sorted_indices[-k:]])

class ParameterSearch:
    def __init__(self, estimator, param_distributions, n_jobs = 3, cv_splits = 2, random_state = None):
        self.estimator           = estimator
        self.param_distributions = param_distributions
        self.n_jobs              = n_jobs
        self.random_state        = random_state
        self.cv_splits           = cv_splits

        self.refit = False
        
    def _check_input_parameters(self, X, y):
        magic_factor = 4
        self.min_resources = self.cv_splits * magic_factor

        n_classes = numpy.unique(y).shape[0]
        self.min_resources *= n_classes

        self.max_resources = X.shape[0]
        
    def _generate_candidate_params(self):
        n_candidates_first_iter = self.max_resources // self.min_resources

        return ParameterSampler(self.param_distributions,
                                n_candidates_first_iter,
                                random_state = self.random_state)

    def _run_search(self, evaluate_candidates):
        from math import ceil, floor, log

        candidate_params = self._generate_candidate_params()
        self.factor      = 3
        
        n_required_iterations = 1 + floor(log(len(candidate_params), self.factor))
        last_iteration        = n_required_iterations - 1
        min_resources         = max(self.min_resources, self.max_resources // self.factor**last_iteration)

        n_possible_iterations = 1 + floor(log(self.max_resources // min_resources, self.factor))
        n_iterations          = min(n_possible_iterations, n_required_iterations)

        self.n_resources_  = []
        self.n_candidates_ = []

        printinfo("n iterations: {}".format(n_iterations))

        for itr in range(n_iterations):
            power = itr
            n_resources = int(self.factor**power * self.min_resources)
            n_resources = min(n_resources, self.max_resources)
            self.n_resources_.append(n_resources)

            n_candidates = len(candidate_params)
            self.n_candidates_.append(n_candidates)

            printinfo("number of resources: {}".format(n_resources))

            more_results = {"iter"       : [itr] * n_candidates,
                            "n_resources": [n_resources] * n_candidates}

            results = evaluate_candidates(candidate_params, more_results = more_results, n_resources = n_resources)

            n_candidates_to_keep = ceil(n_candidates / self.factor)
            candidate_params     = _top_k(results, n_candidates_to_keep, itr)

        self.n_remaining_candidates_ = len(candidate_params)
        self.n_required_iterations_  = n_required_iterations
        self.n_possible_iterations_  = n_possible_iterations
        self.n_iterations_           = n_iterations

    def split(self, X_train, y_train, X_test, y_test, n_resources):
        if numpy.unique(y_train).shape[0] != numpy.unique(y_test).shape[0]:
            printerror("Different number of classes in training and test samples")
        self.n_classes = numpy.unique(y_train)

        train_indices, test_indices = numpy.arange(X_train.shape[0]), numpy.arange(X_test.shape[0])
        
        printinfo("n_resources = {}".format(n_resources))
        train_idx = train_indices[:n_resources]
        test_idx  = test_indices[:n_resources]

        return train_idx, test_idx

    def _format_results(self, candidate_params, n_splits, out, more_results=None, return_train_score = False):
        from numpy.ma import MaskedArray
        from collections import defaultdict
        from scipy.stats import rankdata
        
        n_candidates = len(candidate_params)
        out = _aggregate_score_dicts(out)

        results = dict(more_results or {})
        for key, val in results.items():
            # each value is a list (as per evaluate_candidate's convention)
            # we convert it to an array for consistency with the other keys
            results[key] = numpy.asarray(val)

        def _store(key_name, array, weights=None, splits=False, rank=False):
            """A small helper to store the scores/times to the cv_results_"""
            # When iterated first by splits, then by parameters
            # We want `array` to have `n_candidates` rows and `n_splits` cols.
            array = numpy.array(array, dtype=numpy.float64).reshape(n_candidates, n_splits)
            if splits:
                for split_idx in range(n_splits):
                    # Uses closure to alter the results
                    results["split%d_%s" % (split_idx, key_name)] = array[:, split_idx]

            array_means = numpy.average(array, axis=1, weights=weights)
            results["mean_%s" % key_name] = array_means

            if key_name.startswith(("train_", "test_")) and numpy.any(~numpy.isfinite(array_means)):
                printwarning(f"One or more of the {key_name.split('_')[0]} scores are non-finite: {array_means}")

            # Weighted std is not directly available in numpy
            array_stds = numpy.sqrt(numpy.average((array - array_means[:, numpy.newaxis]) ** 2, axis=1, weights=weights))
            results["std_%s" % key_name] = array_stds

            if rank:
                results["rank_%s" % key_name] = numpy.asarray(rankdata(-array_means, method="min"), dtype=numpy.int32)

        _store("fit_time", out["fit_time"])
        _store("score_time", out["score_time"])
        # Use one MaskedArray and mask all the places where the param is not
        # applicable for that candidate. Use defaultdict as each candidate may
        # not contain all the params
        param_results = defaultdict(functools.partial(MaskedArray,
                                                      numpy.empty(n_candidates,),
                                                      mask=True,
                                                      dtype=object,))
        for cand_idx, params in enumerate(candidate_params):
            for name, value in params.items():
                # An all masked empty array gets created for the key
                # `"param_%s" % name` at the first occurrence of `name`.
                # Setting the value at an index also unmasks that index
                param_results["param_%s" % name][cand_idx] = value

        results.update(param_results)
        # Store a list of param dicts at the key 'params'
        results["params"] = candidate_params

        test_scores_dict = _normalize_score_results(out["test_scores"])
        if return_train_score:
            train_scores_dict = _normalize_score_results(out["train_scores"])

        for scorer_name in test_scores_dict:
            # Computed the (weighted) mean and std for test scores alone
            _store("test_%s" % scorer_name,
                   test_scores_dict[scorer_name],
                   splits=True,
                   rank=True,
                   weights=None)
            if return_train_score:
                _store("train_%s" % scorer_name,
                       train_scores_dict[scorer_name],
                       splits=True,)

        return results

    @staticmethod
    def _select_best_index(refit, refit_metric, results):
        last_iter = numpy.max(results["iter"])
        last_iter_indices = numpy.flatnonzero(results["iter"] == last_iter)
        best_idx = numpy.argmax(results["mean_test_score"][last_iter_indices])

        return last_iter_indices[best_idx]

    
    def fit(self, X_train, y_train, X_test, y_test):
        from joblib      import Parallel
        from collections import defaultdict

        self._check_input_parameters(X = pandas.concat([X_train, X_test], ignore_index = True),
                                     y = pandas.concat([y_train, y_test], ignore_index = True))

        fit_and_score_kwargs = dict(scorer                = _passthrough_scorer,
                                    return_train_score    = False,
                                    return_n_test_samples = True,
                                    return_times          = True,
                                    return_parameters     = False,
                                    error_score           = "raise",
                                    verbose               = 1,
                                )


        parallel = Parallel(n_jobs = self.n_jobs)
        #parallel = Parallel(n_jobs = 1)
        results = {}
        with parallel:
            all_candidate_params = []
            all_out              = []
            all_more_results     = defaultdict(list)

            def evaluate_candidates(candidate_params, n_resources, more_results = None):
                from itertools import product
                
                candidate_params = list(candidate_params)
                n_candidates     = len(candidate_params)
                printinfo("Fitting {0} folds for each of {1} candidates.".format(self.cv_splits, n_candidates))

                train, test = self.split(X_train, y_train, X_test, y_test, n_resources)
                out = parallel(delayed(_fit_and_score)(clone(self.estimator),
                                                       X_train        = X_train,
                                                       y_train        = y_train,
                                                       X_test         = X_test,
                                                       y_test         = y_test,
                                                       train_indices  = train,
                                                       test_indices   = test,
                                                       parameters     = parameters,
                                                       fit_params     = None,
                                                       **fit_and_score_kwargs)
                               for cand_idx, parameters in enumerate(candidate_params))

                all_candidate_params.extend(candidate_params)
                all_out.extend(out)

                if more_results is not None:
                    for key, value in more_results.items():
                        all_more_results[key].extend(value)

                nonlocal results
                results = self._format_results(
                    all_candidate_params, self.cv_splits, all_out, all_more_results, fit_and_score_kwargs["return_train_score"]
                )

                return results


            

            self._run_search(evaluate_candidates)

            first_test_score = all_out[0]["test_scores"]
            self.multimetric_ = isinstance(first_test_score, dict)

        if self.refit or not self.multimetric_:
            refit_metric = "score"
            self.best_index_ = self._select_best_index(self.refit, refit_metric, results)
            if not callable(self.refit):
                # With a non-custom callable, we can select the best score
                # based on the best index
                self.best_score_ = results[f"mean_test_{refit_metric}"][self.best_index_]
            self.best_params_ = results["params"][self.best_index_]

        self.best_estimator_ = clone(clone(self.estimator).set_params(**self.best_params_))

        if self.refit:
            # we clone again after setting params in case some
            # of the params are estimators as well.
            printdebug("Refitting")
            refit_start_time = time.time()
            self.best_estimator_.fit(X_train, y_train, **fit_params)
            refit_end_time   = time.time()
            self.refit_time_ = refit_end_time - refit_start_time
            
            if hasattr(self.best_estimator_, "feature_names_in_"):
                self.feature_names_in_ = self.best_estimator_.feature_names_in_

        # Store the only scorer not as a dict for single metric evaluation
        #self.scorer_ = scorers
        
        self.cv_results_ = results
        self.n_splits_ = self.cv_splits
            
        return self

