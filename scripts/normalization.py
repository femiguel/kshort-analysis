import os

from scripts         import printinfo, printdebug, printwarning, printerror
from utils.ana_utils import years, sMB, lumis, BR, trigger_categories

from numpy import sqrt

def alpha():
    br, sbr = BR["K0S2pi2"]
    Npipi   = get_Npipi()

    corr_multiple_cands  = 0.99
    pid_corr, s_pid_corr = pid_correction()

    eff_ratio, s_eff_ratio, eff_ratio_trig = get_eff_ratio(pid_corr, corr_multiple_cands)

    RTrack, s_RTrack = get_R_track()

    alpha      = (br/Npipi)*(eff_ratio)*RTrack
    alpha_trig = {tcat: (br/Npipi)*(eff_ratio_trig[tcat]) for tcat in trigger_categories}
    
    shlt   = hlt_systematic()
    printdebug("s_hlt = {}".format(shlt))
    s_year = 0.01

    ## uncertainty of sMB -> 0
    salpha = sqrt((sbr/br)**2 + (s_pid_corr/pid_corr)**2 + (s_eff_ratio/eff_ratio)**2 + (s_RTrack/RTrack)**2 + s_year**2 + shlt**2)
    salpha_trig = {}
    for tcat in trigger_categories:
        salpha_trig[tcat] = sqrt(salpha**2 + l0_systematic(tcat)**2)*alpha_trig[tcat]
    
    salpha *= alpha

    printinfo("alpha = {0} +/- {1}".format(alpha, salpha))
    printinfo("alpha TIS = {0} +/- {1}".format(alpha_trig["TIS"], salpha_trig["TIS"]))
    printinfo("alpha xTOS = {0} +/- {1}".format(alpha_trig["xTOS"], salpha_trig["xTOS"]))

    return (alpha, salpha), (alpha_trig, salpha_trig)

def get_eff_ratio(pid_corr, corr_multiple_cands):
    '''
    Get the ratio of efficiencies for the alpha parameter, eff(KS->pipi)/eff(KS->4mu)

    :pid_corr:            float, PID correction
    :corr_multiple_cands: float, correction applied to take into account multiple candidates
    '''
    variables_pipi = ["Generation", "Reconstruction", "Stripping"]
    variables_mu4  = lambda tcat: variables_pipi+["Trigger"+tcat, "Classifier"+tcat]

    ## Uncorrected MC
    effpipi, _  = get_total_eff(channel = "K0S2pi2", variables = variables_pipi)
    effmu4_trig = {tcat: get_total_eff(channel = "K0S2mu4", variables = variables_mu4(tcat))[0]*pid_corr*corr_multiple_cands for tcat in trigger_categories}
    effmu4      = sum(effmu4_trig.values())

    printdebug("Regular efficiency KS->pipi = {}".format(effpipi))
    printdebug("Regular efficiency KS->4mu = {}".format(effmu4))

    ## Corrected MC
    effpipi_corr, _  = get_total_eff(channel = "K0S2pi2", variables = variables_pipi, corr = True)
    effmu4_trig_corr = {tcat: get_total_eff(channel = "K0S2mu4", variables = variables_mu4(tcat), corr = True)[0]*pid_corr*corr_multiple_cands for tcat in trigger_categories}
    effmu4_corr      = sum(effmu4_trig_corr.values())

    printdebug("Corrected efficiency KS->pipi = {}".format(effpipi_corr))
    printdebug("Corrected efficiency KS->4mu = {}".format(effmu4_corr))

    ## Ratios
    eff_ratio   = effpipi_corr/effmu4_corr
    s_eff_ratio = 0.5*(effpipi_corr/effmu4_corr - effpipi/effmu4)
    eff_ratio_trig = {tcat: effpipi/effmu4_trig[tcat] for tcat in list(effmu4_trig.keys())}

    printinfo("Ratio of efficiencies: {0} +/- {1}".format(eff_ratio, s_eff_ratio))

    return eff_ratio, s_eff_ratio, eff_ratio_trig
    
def extrapolation(eff_mumu, s_eff_mumu, r):
    from sympy import Symbol
    x = Symbol("x", real=True)
    
    eff1 = x + r*(1-x)*x
    eff2 = x + (1-x)*x + (1-x)**2*x + (1-x)**3*x

    s_eff1 = eff1.diff(x)
    s_eff2 = eff2.diff(x)

    return (eff1.subs(x, eff_mumu), s_eff1.subs(x, eff_mumu)*s_eff_mumu), (eff2.subs(x, eff_mumu), s_eff2.subs(x, eff_mumu)*s_eff_mumu)

def l0_systematic(tcat):
    #s_eff_mumu =  0.21
    #r = 0.5

    #s_eff1 = (2-2*eff_mumu)*s_eff_mumu
    #s_eff2 = (1 + r*(1-2*eff_mumu))*s_eff_mumu

    return 0.1 if tcat == "TIS" else 0.21

def hlt_systematic():
    eff_mumu   = 0.556 ## from KS->mumu ANA note
    s_eff_mumu = 0.074
    r = 0.5

    (eff1, s_eff1), (eff3, s_eff3) = extrapolation(eff_mumu, s_eff_mumu, r=1)
    (eff2, s_eff2), _              = extrapolation(eff_mumu, s_eff_mumu, r=r)

    printdebug("ratio 1 = {}".format(s_eff1/eff1))
    printdebug("ratio 2 = {}".format(s_eff2/eff2))
    printdebug("ratio 3 = {}".format(s_eff3/eff3))

    return float(max(s_eff1/eff1, s_eff2/eff2, s_eff3/eff3))

def get_Npipi():
    '''
    Get the total number of events in the normalization channel weighted by the sMB per year.
    '''
    from scripts.files      import prepare_files
    from scripts.files_info import info
    from scripts.selections import cuts
    from utils.rootutils    import get_entries

    channel = "K0S2pi2"
    sel = cuts["extracuts_norm"]

    Npipi, Npipi_total = {}, 0
    for year in years:
        ifilename = prepare_files(data_type = "Data",
                                  year      = year,
                                  channel   = channel)
        itreename = info["Trees"][channel]["Data"][year][channel+"_NoPTCuts"]
        Npipi[year] = get_entries(ifilename = ifilename,
                                  itreename = itreename,
                                  sel       = sel)
        Npipi_total += Npipi[year]/sMB[year]

    printinfo("N(KS->pipi) = {}".format(Npipi_total))

    return Npipi_total

def get_R_track():
    R_track, s_R_track = 1.002, 0.0124

    return R_track, s_R_track

def get_total_eff(channel, variables, corr = False):
    '''
    Compute the total efficiency for a given channel and a given set of variables.
    '''
    from scripts.efficiencies import compute_total_eff_error, compute_eff

    p_array, sp_array = [],[]
    for name in variables:
        p_, sp_ = compute_eff(name = name, channel = channel, correct = corr)
        p_array += [p_]; sp_array += [sp_]

    p,sp = compute_total_eff_error(p_array,sp_array)

    return p, sp

def pid_correction():
    from pid.main import get_info, get_pidvar_from_pidcut

    pidcut = ["IsMuon==1", "DLLmu>0"]
    pidvar = get_pidvar_from_pidcut(pidcut)

    info = get_info()
    corr = {year+mag: info[year][mag][pidvar]["PIDCalibEff"]/info[year][mag][pidvar]["MCEff"] for year in years for mag in ("Up", "Down")}

    R_pid   = sum([corr[year+mag]*lumis[year]/2 for year in years for mag in ["Up", "Down"]])/sum(list(lumis.values()))
    s_R_pid = max(list(corr.values())) - min(list(corr.values()))

    printinfo("R_pid = {0} +/- {1}".format(R_pid, s_R_pid))

    return R_pid, s_R_pid

if __name__ == "__main__":
    from scripts import setup_debug

    setup_debug()

    alpha()
