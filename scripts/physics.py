def armenteros_vars(p1, p2):
    def vsqrt(expr):
        from math import sqrt
        
        if expr < 0: return expr
        return sqrt(expr)

    p = p1 + p2
    pu = p.unit()
    pl1 = p1*pu ## longitudinal component
    pl2 = p2*pu

    ap_alpha = (pl1 - pl2)/(pl1 + pl2)
    ap_pt    = vsqrt(abs(p1)**2 - pl1**2)

    return ap_alpha, ap_pt

def read_pdg():
    '''
    Access an instance of the PDG
    '''
    from ROOT import TDatabasePDG

    return TDatabasePDG.Instance()
