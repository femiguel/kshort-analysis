'''
Basic module to run the analysis.
It contains basic information like the location of the tuples or the path to the current project.
It also sets up basic logging functions like printinfo or printerror.
Additionally, it sets up the LHCb style for the generated plots.
'''
import os
import logging

__logger_level__ = logging.INFO

def configure_logger(loglevel):
    '''
    Configure the logger instances.
    '''
    from scripts.logger import LogFormatter

    f = LogFormatter()
    h = logging.StreamHandler()
    h.setFormatter(f)
    h.setLevel(loglevel)
    logging.getLogger().addHandler(h)
    logging.getLogger().setLevel(loglevel)

    return h

h = configure_logger(__logger_level__)

def setup_debug():
    '''
    Allow for debug messages to be printed by logging
    '''
    logging.getLogger().removeHandler(h)
    configure_logger(logging.DEBUG)

def get_host():
    '''
    Retrieve the name of the host.
    '''
    import socket
    host = socket.gethostname()

    if "mastercr1" not in host and "nodo" not in host and "lxplus" not in host:
        printerror("Unidentified host {}".format(host))

    return "lxplus" if "lxplus" in host else "nodo" if "nodo" in host else "master"

def get_project_path():
    '''
    Retrieve the path to the project in this host.
    '''
    cwd = os.path.dirname(os.path.abspath(__file__))

    return os.path.dirname(cwd)

def get_original_tuples_path():
    '''
    Retrieve the path to the location of the n-tuples used in the analysis
    '''
    host = get_host()
    if host == "master" or host == "nodo":
        return "/scratch49/miguel.fernandez/KShortTuples_New"

    return "/eos/lhcb/wg/RD/K0S2mu4/"

def get_tuples_path():
    '''
    Retrieve the path to the location of the n-tuples used in the analysis
    '''
    host = get_host()
    if host == "master" or host == "nodo":
        return "/scratch49/miguel.fernandez/KShortTuples_New"

    return "/eos/user/f/femiguel/KShortTuples"

def printinfo(msg):
    logging.info(msg)

def printdebug(msg):
    logging.debug(msg)
    
def printwarning(msg):
    logging.warning(msg)

def printerror(msg):
    import sys
    
    logging.error(msg)
    sys.exit(1)

try:
    from ROOT import gROOT
    gROOT.ProcessLine(".L {}/lhcbStyle.C".format(os.path.dirname(os.path.realpath(__file__))))
except ModuleNotFoundError:
    printwarning("ROOT not found, cannot load lhcbStyle.C")
