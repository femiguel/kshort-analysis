import os
import collections

from scripts         import printinfo, printwarning, printdebug, printerror
from utils.ana_utils import trigger_categories, years

def setup_mcfiles(classifier):
    '''
    Apply the given classifier to the entire MC files

    :classifier: str, name of the classifier to apply
    '''
    from utils.mva_utils import get_filenames, MVAInfo
    from utils.pyutils   import file_exists
    from utils.rootutils import copytree, merge_files, has_tree
    from scripts.runTMVA import applyClassifier
    from scripts.files   import prepare_files
    from scripts.files_info import info

    data_type, channel = "MC", "K0S2mu4"
    out = {}

    for year in years:
        out[year] = {}
        mcfilename = prepare_files(data_type, year, channel)
        ofilename  = get_filenames("FullMC"+year).format(**locals())
        out[year]["File"]  = ofilename
        out[year]["Trees"] = {}
        for triggercat in trigger_categories:
            mctreename = info["Trees"][channel][data_type][year][channel]+"_"+triggercat
            otreename  = "DecayTree_"+triggercat
            out[year]["Trees"][triggercat] = otreename

            if file_exists(ofilename) and has_tree(ofilename, mctreename):
                printdebug("File {} already exists with {} in it".format(ofilename, triggercat))
                continue

            copytree(ifilename = mcfilename,
                     itreename = mctreename,
                     ofilename = ofilename,
                     otreename = otreename,
                     sel       = "")
            
            applyClassifier(ifilename  = mcfilename,
                            itreename  = mctreename,
                            variables  = MVAInfo.vars_training,
                            spectators = MVAInfo.spectators_training,
                            classifier = classifier[triggercat],
                            nfolds     = MVAInfo.nfolds,
                            triggercat = triggercat)

    mergedfilename = get_filenames("FullMC").format(**locals())

    return out, mergedfilename

def setup_signal_parameters(classifier, classifier_eff, savefilename = None):
    from utils.rootutils  import copytree, has_tree, merge_files
    from utils.file_utils import get_ofilename
    from utils.pyutils    import file_exists, save_data_file
    from utils.mva_utils  import get_selection, getBDTsel, MVAInfo
    
    mcfiles, _mergedfilename = setup_mcfiles(classifier)
    name = "Signal"
    mode = "Apply"
    ofilenames, otreenames = [], {}
    for year in years:
        ifilename = mcfiles[year]["File"]
        ofilename = get_ofilename("BDTCut", ifilename)
        ofilenames += [ofilename]

        for triggercat in trigger_categories:
            itreename = otreename = mcfiles[year]["Trees"][triggercat]
            otreenames[triggercat] = otreename

            if file_exists(ofilename) and has_tree(ofilename, otreename):
                printdebug("File {} already exists with {} in it".format(ofilename,triggercat))
                continue
            
            sel = " || ".join(["({0} && {1})".format(get_selection(name = name, mode = mode, iteration = iteration), 
                                                     getBDTsel(year, triggercat, parity, classifier[triggercat], classifier_eff[triggercat])) \
                               for iteration,parity in enumerate(MVAInfo.parity_names)])

            copytree(ifilename = ifilename,
                     itreename = itreename,
                     ofilename = ofilename,
                     otreename = otreename,
                     sel       = sel)

    mergedfilename = get_ofilename("BDTCut", _mergedfilename)
    if not file_exists(mergedfilename):
        merge_files(ofilename = mergedfilename,
                    ifiles    = ofilenames)
    else:
        printdebug("File {} already exists")

    if len(otreenames) != len(trigger_categories): printerror("Something went wrong, different numbers of treenames and trigger categories")
    res = fit_signal(ifilenames = {triggercat: mergedfilename for triggercat in trigger_categories},
                     itreenames = otreenames)

    pars_from_fit = list(res.floatParsFinal()) + list(res.constPars())
    pars = {}
    for triggercat in trigger_categories:
        pars[triggercat] = {}
        for pvar in pars_from_fit:
            varname = pvar.GetName().split(triggercat+"_Ipatia_")
            try:
                pars[triggercat][varname[1]] = [pvar.getVal()]
            except IndexError:
                continue

    if ofilename is not None:
        save_data_file(savefilename, pars)
    
    return pars


def fit_signal(ifilenames, itreenames):
    '''
    Fit signal TTree to a RooIpatia object
    '''
    from utils.fit_utils import get_mass, create_dataset, mass_values, create_var,FitConditions, PlotConditions, MultiFitResult, SimFitResult
    from utils.ana_utils import __blind_region__
    from scripts.fit_models import SignalModel, Category
    from ROOT import RooFit, RooCategory
    
    mass = get_mass("K0S")
    DefRange = "FullRange"

    category = RooCategory("category", "category")
    categories = []
    for triggercat in trigger_categories:
        data = create_dataset(ifilename = ifilenames[triggercat],
                              itreename = itreenames[triggercat],
                              variables = [mass.var])

        sig_mean = create_var("mean", mass_values("K0S"), *__blind_region__)
        model = SignalModel.draw_ipatia(var    = mass.var,
                                        mean   = sig_mean,
                                        params = mc_ipa_params(),
                                        suffix = "")
        categories += [Category(triggercat, model, data)]

    main_category = Category.make_simultaneous(name = "whole",
                                               category = category,
                                               categories = categories,
                                               mass       = mass.var)

    res = main_category.model.pdf.fitTo(main_category.data, RooFit.Range(DefRange), RooFit.NormRange(DefRange), RooFit.Minos(1), RooFit.Save(True))

    return res

def fit_main(ifilenames, itreenames, alphas, ipa_params, ofilename, unblind = False, show = True):
    from utils.fit_utils    import (get_mass, create_dataset, candidates_in_region,
                                    FitConditions, PlotConditions, SimFitResult, create_var, setup_fit_constraints)
    from ROOT               import (RooCategory, RooFit, kGreen, kRed, kBlue, kDashed,
                                    RooFormulaVar, RooArgList, RooArgSet)
    from scripts.fit_models import Category, BkgModels, create_main_model

    mass = get_mass("K0S")
    
    nsig, summaryConstraints, BR, _ = setup_fit_constraints(alphas, unblind)

    category = RooCategory("category", "category")
    categories = []
    for triggercat in trigger_categories:
        data  = create_dataset(ifilename = ifilenames[triggercat],
                               itreename = itreenames[triggercat],
                               variables = [mass.var],
                               cuts      = None)

        model = create_main_model(data, mass, {"signal": ipa_params[triggercat], "bkg": {"indx": [-0.05, 0.05]}}, triggercat, nsig[triggercat])
        categories += [Category(triggercat, model, data)]

    main_category = Category.make_simultaneous(name       = "whole",
                                               category   = category,
                                               categories = categories,
                                               mass       = mass.var)

    DefRange = "LeftSideband,RightSideband" if not unblind else "FullRange"
    res = main_category.model.pdf.fitTo(main_category.data, RooFit.Range(DefRange), RooFit.NormRange(DefRange), RooFit.Minos(1), RooFit.Save(True), RooFit.ExternalConstraints(RooArgSet(summaryConstraints)))

    for cat in categories:
        bkg_pdf = None
        for m in cat.model.models:
            if m.name() == cat.format_name(BkgModels.__model_name__):
                bkg_pdf = m.pdf
                break

        if bkg_pdf is None:
            printerror("Unable to find background PDF")

        n = candidates_in_region(bkg_pdf, main_category.data, mass, range = 'Signal', norm_range = "FullRange")

        printinfo("Number of candidates in signal region: {} (category = {})".format(n, cat.name()))

    components = {"bkg"   : [RooFit.LineColor(kRed)  , RooFit.LineStyle(kDashed), RooFit.NormRange(DefRange)],
                  "Ipatia": [RooFit.LineColor(kGreen), RooFit.LineStyle(kDashed), RooFit.NormRange(DefRange)]}

    plot_conds = PlotConditions(components, show=show, plot_pulls = True, nbins = 30)
    if unblind:
        fit_conds = FitConditions(mass, "FullRange")
    else:
        fit_conds = FitConditions(mass, DefRange)
    
    _, x, y = SimFitResult(categories, main_category, res).plot_and_write(ofilename, fit_conds, plot_conds, BR, summaryConstraints)

    return x, y
