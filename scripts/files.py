'''
Set up the files used in the analysis. The original files, after the n-tuple creation, are stored in a directory called OriginalTuples, ideally with read-only permissions for everyone so that they are not changed.

When running this module, they are moved to a new directory, {tuples_path}/{data_type}/{channel}, where {tuples_path} is the given path to where the tuples are stored (see scripts/__init__.py), {data_type} can be Data or MC, and channel can be K0S2mu4 or K0S2pi2. When moved there, only a select number of branches are included, to minimize the space in disk. The VeloMatterVeto variable is added, and the MC is truth-matched. Then, several steps are performed, which include MC corrections (if necessary), cuts on the track variables, and additional selections (trigger, blinding, etc.).
'''
import os

from scripts            import printinfo, printwarning, printerror, printdebug, get_tuples_path
from scripts.files_info import check_params
from utils.rootutils    import copytree
from utils.ana_utils    import years, trigger_categories

class Step(object):
    def __init__(self, name, func, ifilename = "", ofilename = "", force = False, kwargs = {}):
        '''
        Instatiate a step for processing files.

        :name:      str, name of the step
        :func:      function, method that will run the step
        :ifilename: str, path to the input filename
        :ofilename: str, path to the output filename
        :force:     bool, True if the step is to be run even if the output filename (ofilename) already exists
        :kwargs:    dict, additional keyword arguments needed by the given function
        '''
        self.name      = name
        self.func      = func
        self.ifilename = ifilename
        self.ofilename = ofilename
        self.force     = force
        self.kwargs    = kwargs

def setup_trigger(ifilename, itreename, ofilename, otreename, odir, cuts):
    '''
    Auxiliary function to set up the trigger cut step

    :ifilename: str, path to the input filename
    :itreename: str, name of the input TTree
    :ofilename: str, path to the output filename
    :otreename: str, name of the output TTree
    :odir:      str, name of the TDirectory where the TTree will be stored
    :cuts:      str or Selection object, cuts to apply to the input TTree
    '''
    for tcat in list(cuts.keys()):
        copytree(ifilename = ifilename,
                 itreename = itreename,
                 ofilename = ofilename,
                 otreename = otreename,
                 odir      = odir,
                 sel       = cuts[tcat],
                 suffix    = "_"+tcat)

@check_params
def prepare_files(data_type, year, channel):
    '''
    Main function to prepare the files used in the analysis.
    For a given data type, it outputs the path to the ROOT file with the last step applied.
    The wrapper check_params() checks that the three input parameters are correct
    
    :data_type: str, can be "MC" or "Data"
    :year:      str or int, can be 2016, 2017 or 2018
    :channel:   str, can be "K0S2mu4" or "K0S2pi2"
    '''
    from scripts.selections import cuts
    from utils.pyutils      import file_exists, get_filename_from_path, get_path_from_filename, flatten_array
    from utils.file_utils   import setup_tuples, get_ofilename

    ifilename, itreenames = setup_tuples(data_type, year, channel)

    steps = []

    ## Monte Carlo corrections
    if data_type == "MC":
        from scripts.mc_correct import correct
        steps += [Step(name   = "AcceptReject",
                       func   = correct,
                       kwargs = {"correction": "AcceptReject"}),
                  Step(name   = "HorizontalCorrection",
                       func   = correct,
                       kwargs = {"correction": "HorizontalCorrection",
                                 "year"      : year,
                                 "channel"   : channel})]                  

    ## Armenteros cut
    if channel == "K0S2pi2":
        from scripts.mc_correct import addArmenteros, cutArmenteros
        steps += [Step(name   = "addArmenteros",
                       func   = addArmenteros,
                       kwargs = {"data_type": data_type}),
                  Step(name   = "cutArmenteros",
                       func   = cutArmenteros,
                       kwargs = {"data_type": data_type})]

    ## Trigger selections
    if channel == "K0S2mu4":
        steps += [Step(name      = "Trigger",
                       func      = setup_trigger,
                       kwargs    = {"cuts": {tcat: cuts["trigsel"+tcat.lower()] for tcat in trigger_categories}})]

    ## Track cuts
    steps += [Step(name   = "TrackCuts",
                   func   = copytree,
                   kwargs = {"sel": cuts["trackcuts{0}{1}".format("sig" if channel == "K0S2mu4" else "norm",
                                                                  "MC"*(data_type == "MC"))]})]

    ## Blinding
    if data_type == "Data" and channel == "K0S2mu4":
        steps += [Step(name = "Blind",
                       func = copytree,
                       kwargs = {"sel": cuts["blind"]})]

    ## Run steps
    for k,step in enumerate(steps):
        step.ifilename = ifilename if k == 0 else steps[k-1].ofilename
        step.ofilename = get_ofilename(step.name, step.ifilename)
        if not file_exists(step.ofilename) or step.force:
            for itreename in itreenames:
                printinfo("Running {0} step on {1}".format(step.name, itreename))
                step.func(ifilename = step.ifilename,
                          itreename = itreename,
                          ofilename = step.ofilename,
                          otreename = get_filename_from_path(itreename),
                          odir      = get_path_from_filename(itreename),
                          **step.kwargs)

            printdebug("Step {} concluded".format(step.name))

            ## After running the trigger step, the input treename changes and splits into two
            if step.name == "Trigger":
                itreenames = flatten_array([[t.replace("Tree", "Tree_{}".format(tcat)) for tcat in trigger_categories] if "/DecayTree" in t else t for t in itreenames])
                printdebug("New input treenames: {}".format(itreenames))

        else:
            printdebug("ROOT file {} already exists. Please check if it should be recreated.".format(step.ofilename))

    printdebug("All steps done")

    return step.ofilename

def add_arguments(parser):
    parser.add_argument("--data-type", type = str, choices = ["MC", "Data"], required = True,
                        help = "Data type to run")
    parser.add_argument("--channel", type = str, choices = ["K0S2mu4", "K0S2pi2"], required = True,
                        help = "Channel to run")
    parser.add_argument("--year", type = str, choices = years, required = True,
                        help = "Year to run")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

    
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description = __doc__)
    add_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    prepare_files(data_type = args.data_type,
                  year      = args.year,
                  channel   = args.channel)
