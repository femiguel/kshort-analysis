'''
Module to compute the efficiencies of this analysis.
'''
import os
import functools

from ROOT               import TFile
from scripts.selections import cuts
from scripts            import get_tuples_path, printinfo, printwarning, printerror, printdebug
from scripts.files_info import info, get_filename
from utils.rootutils    import get_entries
from utils.pyutils      import file_exists
from numpy              import sqrt

#eff_gen   = 0.13716
#s_eff_gen = 0.00035

def check_eff_params(channel, year):
    '''
    Checks the parameters "channel" and "year" are correct.

    :year:    str or int, can be 2016, 2017 or 2018
    :channel: str, can be "K0S2mu4" or "K0S2pi2"
    '''
    from utils.ana_utils import years

    if channel not in ["K0S2mu4", "K0S2pi2"]:
        printerror("Computation of efficiencies for channel '{}' has not been implemented yet".format(channel))
    if str(year) not in years:
        printerror("Unknown year '{}'".format(year))

    return channel, str(year)

def eff(a, n, sa = 0, sb = 0):
    '''
    Auxiliary function to calculate an efficiency and its error.

    :a:  int, number of events that pass
    :n:  int, total number of events
    :sa: float, error of the "a" parameter
    :sb: float, error of the "b" parameter, computed as b = n-a
    '''
    p = a/n
    if not (sa or sb):
        return p, sqrt(p*(1-p)/n)
    b = n-a
    if not sb: sb = sqrt(b)
    if not sa: sa = sqrt(a)
    else:
        return p, 1/n**2*sqrt(b**2*sa**2 + a**2*sb**2)

def main_eff(func):
    '''
    Main function to decorate methods that calculate different types of efficiencies.
    '''
    @functools.wraps(func)
    def wrapper(channel, year, correct, triggercat):
        channel, year = check_eff_params(channel, year)
        
        ch = "sig" if channel == "K0S2mu4" else "norm"
        try:
            itreenames = info["Trees"][channel]["MC"][year]
        except KeyError:
            printerror("Key error in info = {}".format(info))

        ifilename = get_mcfilename(channel, year, "_PTMCfilter_Corr"*int(correct))
        ifile     = TFile.Open(ifilename)

        if triggercat is None:
            args = (ifile, itreenames, channel, ch)
        else:
            args = (ifile, itreenames, triggercat, channel, ch)

        t1, t2, sel1, sel2 = func(*args)

        p, sp = eff(a = get_entries(itree = t1, sel = sel1),
                    n = get_entries(itree = t2, sel = sel2))

        printdebug("{0} efficiency for {1} and year {2}: {3} +/- {4}".format(func.__name__, channel, year, p, sp))

        if isinstance(ifile, tuple):
            for ifile_ in ifile: ifile_.Close()
        else:
            ifile.Close()

        return p, sp

    return wrapper

def get_mcfilename(channel, year, ext = ""):
    '''
    Retrieves the filename used for a given channel, year, and extension (which covers for possible MC corrections).

    :year:    str or int, can be 2016, 2017 or 2018
    :channel: str, can be "K0S2mu4" or "K0S2pi2"
    :ext:     str, optional, extension that the ROOT file path may have
    '''
    path_to_file = os.path.join(get_tuples_path(), "MC", channel, "MC_{0}_{1}{2}.root".format(channel, year, ext))

    if not file_exists(path_to_file):
        if channel == "K0S2mu4":
            printerror("File {} does not exist".format(path_to_file))

        from utils.rootutils import copytree
        from utils.pyutils   import get_filename_from_path, get_path_from_filename, make_dirs
        
        ifilepath, itreenames = get_filename(data_type = "MC", year = year, channel = channel)
        ifilename = get_filename_from_path(ifilepath)

        odir = os.path.join(get_tuples_path(), "MC", channel, "Efficiencies")
        odir = make_dirs(odir)
        
        ofilename = os.path.join(odir, ifilename)

        branches = get_branches(channel, year)
        mctruthbranches = ["K0S_TRUEPT"]
        
        for itreename in itreenames:
            if itreename == "Ks2PiPiForRnSLine/DecayTree":
                printdebug("Skipping Ks2PiPiForRnSLine/DecayTree")
                continue
            copytree(ofilename = ofilename,
                     ifilename = ifilepath,
                     itreename = itreename,
                     otreename = get_filename_from_path(itreename),
                     option    = "update",
                     odir      = get_path_from_filename(itreename),
                     sel       = cuts["mctruth{}".format("sig" if channel == "K0S2mu4" else "norm")] if "MCDecayTree" not in itreename else "",
                     branches  = branches if "MCDecayTree" not in itreename else mctruthbranches,
                     mc_prepared = True)

        path_to_file = ofilename
        printdebug("{0} now created".format(path_to_file))


    return path_to_file

def Generation(channel, year, *args, **kwargs):
    '''
    Output the generation efficiency for a given year and channel
    '''
    if channel == "K0S2pi2": return 1,0

    return 0.13716, 0.00035

@main_eff
def Reconstruction(ifile, itreenames, channel, ch):
    '''
    Compute the reconstruction efficiency.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    tmc   = ifile.Get(itreenames["MCTruth"])
    treco = ifile.Get(itreenames["Reco"])

    selmc   = ""
    selreco = cuts["mctruth"+ch]

    return treco, tmc, selreco, selmc

@main_eff
def StrippingNoID(ifile, itreenames, channel, ch):
    '''
    Compute the stripping efficiency from the reconstruction tuples (not including mu ID variables)

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    treco  = ifile.Get(itreenames["Reco"])

    selreco  = cuts["mctruth"+ch]
    selstrip = cuts["mctruth"+ch] + cuts["stripping"+ch]

    return treco, treco, selstrip, selreco

@main_eff
def Stripping(ifile, itreenames, channel, ch):
    '''
    Compute the full stripping efficiency.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''

    treco = ifile.Get(itreenames["Reco"])
    tstrip = ifile.Get(itreenames[channel])

    selreco = selstrip = cuts["mctruth"+ch]

    return tstrip, treco, selstrip, selreco

@main_eff
def L0(ifile, itreenames, triggercat, channel, ch):
    '''
    Compute the efficiency of the level-0 trigger cuts.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :triggercat: str, can be "TIS" or "xTOS"
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    tstrip = ifile.Get(itreenames[channel])

    selstrip = cuts["mctruth"+ch]
    sell0    = cuts["mctruth"+ch]+cuts["l0sel"+triggercat.lower()]

    return tstrip, tstrip, sell0, selstrip

@main_eff
def HLT1(ifile, itreenames, triggercat, channel, ch):
    '''
    Compute the efficiency of the high level trigger 1 cuts.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :triggercat: str, can be "TIS" or "xTOS"
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    tstrip = ifile.Get(itreenames[channel])

    sell0   = cuts["mctruth"+ch]+cuts["l0sel"+triggercat.lower()]
    selhlt1 = cuts["mctruth"+ch]+cuts["l0sel"+triggercat.lower()]+cuts["hlt1sel"]

    return tstrip, tstrip, selhlt1, sell0

@main_eff
def HLT2(ifile, itreenames, triggercat, channel, ch):
    '''
    Compute the efficiency of the high level trigger 2 cuts.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :triggercat: str, can be "TIS" or "xTOS"
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    tstrip = ifile.Get(itreenames[channel])

    selhlt1 = cuts["mctruth"+ch]+cuts["l0sel"+triggercat.lower()]+cuts["hlt1sel"]
    selhlt2 = cuts["mctruth"+ch]+cuts["l0sel"+triggercat.lower()]+cuts["hlt1sel"]+cuts["hlt2sel"]

    return tstrip, tstrip, selhlt2, selhlt1

@main_eff
def Trigger(ifile, itreenames, triggercat, channel, ch):
    tstrip = ifile.Get(itreenames[channel])

    selstrip = cuts["mctruth"+ch]
    seltrig  = cuts["mctruth"+ch]+cuts["trigsel"+triggercat.lower()]

    return tstrip, tstrip, seltrig, selstrip

def Classifier(triggercat, *args, **kwargs):
    from utils.mva_utils import MVAInfo
    
    return MVAInfo.optimal_efficiency[triggercat], 0

@main_eff
def Tracks(ifile, itreenames, channel, ch):
    '''
    Compute the track cuts efficiency.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    tstrip = ifile.Get(itreenames[channel])

    selstrip = cuts["mctruth"+ch]
    seltrack = cuts["mctruth"+ch]+cuts["trackcuts"+ch+"MC"]

    return tstrip, tstrip, seltrack, selstrip

@main_eff
def IsMuon(ifile, itreenames, channel, ch):
    '''
    Compute the IsMuon efficiency.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    treco = ifile.Get(itreenames["Reco"])

    selinacc  = cuts["mctruth"+ch]+cuts["stripping"+ch]+cuts["inAcc"]
    selismuon = cuts["mctruth"+ch]+cuts["stripping"+ch]+cuts["inAcc"]+cuts["isMuon"]

    return treco, treco, selismuon, selinacc

@main_eff
def InAcc(ifile, itreenames, channel, ch):
    '''
    Compute the InAcc efficiency.

    :ifile:      str, path to the input file
    :itreenames: dict, for reference, look for the dict "info" in files_info.py
    :channel:    str, can be "K0S2mu4" or "K0S2pi2"
    :ch:         str, can be "sig" or "norm" depending on the value of channel
    '''
    treco = ifile.Get(itreenames["Reco"])

    selstrip = cuts["mctruth"+ch]+cuts["stripping"+ch]
    selinacc = cuts["mctruth"+ch]+cuts["stripping"+ch]+cuts["inAcc"]

    return treco, treco, selinacc, selstrip
    
def StrippingCuts(channel, year, correct):
    '''
    Compute the efficiency of each of the stripping cuts

    :channel: str, can be "K0S2mu4" or "K0S2pi2"
    :year:    str or int, can be 2016, 2017 or 2018
    :correct: bool, True if corrected MC is to be used.
    '''
    from utils import selutils
    import re
    
    ifilename = get_mcfilename(channel, year, "_PTMCfilter_Corr"*(int(correct)))
    ifile     = TFile.Open(ifilename)

    itreenames = info["Trees"][channel]["MC"][year]
    itree = ifile.Get(itreenames["Reco"])

    ch = "sig" if channel == "K0S2mu4" else "norm"
    line = "K0s2MuMuMuMuLine" if channel == "K0S2mu4" else "Ks2PiPiForRnSNoPtCutsLine"

    treco  = ifile.Get(itreenames["Reco"])

    sel = cuts["mctruth"+ch]

    _, strip_cuts = selutils.stripping(line        = line,
                                       return_cuts = True)

    for stripcut in strip_cuts:
        stripcut = re.sub(r"\s+","",stripcut)
        newsel = sel + selutils.Selection(stripcut)
        
        p, sp  = eff(a = get_entries(itree = itree, sel = newsel),
                     n = get_entries(itree = itree, sel = sel))

        printinfo("Efficiency for {0} is {1} +/- {2}".format(stripcut, p, sp))

        sel = newsel

def compute_eff(name, channel, correct):
    '''
    Compute the average of a given efficiency of all three years, weighted by the luminosity.

    :name:    str, name of the efficiency (must match the name of a function defined in this module)
    :channel: str, ncan be "K0S2mu4" or "K0S2pi2"
    :correct: bool, True if corrected MC is to be used.
    '''
    from utils.ana_utils import years, lumis
    
    if "TIS" in name or "TOS" in name:
        tcat = "TIS" if "TIS" in name else "xTOS"
        name = name.split(tcat)[0]
    else:
        tcat = None

    func = globals()[name]
    p, sp = [], []
    total_lumi = 0
    for year in years:
        p_, sp_ = func(channel = channel, year = year, correct = correct, triggercat = tcat)
        p += [p_*lumis[str(year)]]; sp += [sp_*lumis[str(year)]]
        total_lumi += lumis[str(year)]

    p_avg, sp_avg = sum(p)/total_lumi, sum(sp)/total_lumi
    printdebug("Average {0} efficiency is {1} +/- {2}".format(name, p_avg, sp_avg))

    return p_avg, sp_avg

def compute_total_eff(p_array):
    '''
    Compute the product of a given set of efficiencies.

    :p_array: array of float, given list of efficiencies
    '''
    p = 1
    for p_ in p_array: p*=p_

    return p

def compute_total_eff_error(p_array, sp_array):
    '''
    Compute the product efficiency and its error for a given list of efficiencies.

    :p_array:  array of float, given list of efficiencies
    :sp_array: array of float, given list of the erorrs of efficiencies
    '''
    aux = 0
    p = compute_total_eff(p_array)
    for k in range(len(p_array)):
        aux += (p*sp_array[k]/p_array[k])**2

    return p, sqrt(aux)

def add_arguments(parser):
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    #StrippingCuts(channel = "K0S2pi2", year = "2016", correct = False)
    #path_to_file = get_mcfilename(channel = "K0S2pi2", year = "2017", ext = "_PTMCfilter_Corr")
    #branches(channel = "K0S2mu4",
    #         year    = "2016")

    '''
    p_array, sp_array = [eff_gen], [s_eff_gen]

    #for name in ("Reconstruction", "StrippingNoID"):
    for name in ("Reconstruction", "Stripping", "Tracks"):
        p_, sp_ = compute_eff(name = name, channel = "K0S2pi2", correct = True)
        p_array += [p_]; sp_array += [sp_]

    p,sp = compute_total_eff_error(p_array,sp_array)

    printinfo("Total efficiency: {0} +/- {1}".format(p, sp))
    '''
    L0(channel="K0S2mu4", year = "2016", correct = False, triggercat = "TIS")
