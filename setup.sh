## Get the current path
path=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
## Add $path/scripts to PYTHONPATH
export PYTHONPATH=$path:$PYTHONPATH
echo "Added $path to PYTHONPATH"
