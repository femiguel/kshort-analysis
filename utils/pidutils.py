import os
import pandas
import numpy

from scripts import printinfo, printwarning, printerror, printdebug
from tqdm    import tqdm

def _apply_sel(df, sel):
    '''
    Apply a given selection to a given dataframe

    :df:  pandas DataFrame, given dataframe to cut
    :sel: str, selection to apply
    '''
    df = df.query(sel)

    return df

def get_branch_names(bin_vars, cuts, pidcuts):
    '''
    Gets the branch names we need for this dataset.
    These are the variables that will be part of the binning, as well as the variables we need to cut at,
    both for preliminary cuts to align both the calibration and reference samples, and the PID cuts we are to study.

    :bin_vars: array, names of the binning variables. In this analysis we use ("P", "ETA", "nSPDhits")
    :cuts:     array, cuts to align both the calibration and reference samples
    :pidcuts:  array, PID cuts to study
    '''
    from   pid.aliases import aliases
    import re

    def get_cut_varnames(cuts_array):
        varnames = {}
        for cut in cuts_array:
            cut    = re.sub(r"\s+", "", cut) ## remove all spaces from the cuts
            cut_var = re.split(r"<|>|==|!=", cut)[0]

            varnames[cut_var] = aliases[cut_var]

        return varnames

    bin_var_names = {"sWeight": "probe_sWeight"}

    for br in bin_vars:
        bin_var_names[br] = aliases[br]

    cut_var_names = get_cut_varnames(cuts)
    pidcut_var_names = get_cut_varnames(pidcuts)

    return bin_var_names, cut_var_names, pidcut_var_names

def get_custom_filename(mode, year, mag, particle, bin_var_names, cuts, pid_cuts, nbins_array):
    '''
    Format the name of the pickle file where the bin edges will be stored.

    :mode:          str, purpose of the filename -- ("bins", "hists")
    :year:          str or int, year being studied -- (2016, 2017, 2018)
    :mag:           str, magnet polarity -- ("Up", "Down")
    :particle:      str, name of the particle to be studied
    :bin_var_names: array of str, names of the binning variables
    :cuts:          array of str, user cuts meant to align the calibration and reference samples
    :pid_cuts:      array of str, PID cuts to be studied
    :nbins_array:   array of 3 ints, it contains the number of bins each of the binning variables will have
    '''
    from scripts import get_project_path
    from utils.pyutils import make_dirs
    odirname = "{0}_{1}{4}_{2}{5}_{3}{6}".format(mode, *nbins_array, *bin_var_names)
    odir     = os.path.join(get_project_path(), "pid", odirname)
    
    odir = make_dirs(odir)

    return os.path.join(odir, "custom_{0}_{1}_Mag{2}_{3}_{4}_{5}.pickle".format(mode,
                                                                                year,
                                                                                mag.capitalize(),
                                                                                particle.replace("-", ""),
                                                                                "&".join(cuts),
                                                                                "&".join(pid_cuts)))

def get_samples(sample, mag, particle, max_files):
    '''
    Get the calibration sample from the PID Calib library (samples.json)
    This is a dict with all the ROOT files available for calibration
    for each year, magnet polarity, and particle.

    :sample:    str, name of the sample. For this analysis we use "Turbo", so sample will be Turbo+year
    :mag:       str, magnet polarity ("Up", "Down")
    :particle:  str, particle to calibrate. In this analysis we use low-PT muons, so this should be "Mu_nopt"
    :max_fiels: int, maximum number of files to get from the catalog
    '''
    import json
    from   scripts import get_project_path
    
    samples_file = os.path.join(get_project_path(), "pid/samples.json")
    with open(samples_file, "r") as f:
        samples_dict = json.load(f)

    magnet      = "Mag"+mag.capitalize()
    sample_name = "-".join([sample, magnet, particle])

    sample_dict = samples_dict[sample_name]
    if "link" in sample_dict:
        raise NotImplementedError("Link in sample, go back to the code to implement")

    calibration_sample = sample_dict.copy()
    if max_files:
        calibration_sample["files"] = calibration_sample["files"][:max_files]

    return calibration_sample

def get_tree_paths(particle):
    '''
    Get the name of the TTrees from the PIDCalib catalog
    for a given particle to calibrate.

    :particle: str, name of the particle to calibrate
    '''
    from pid.samples import tuple_names

    tree_paths = []
    for tuple_name in tuple_names[particle]:
        tree_paths += [tuple_name+"/DecayTree"]

    return tree_paths

def load_pidcalib_sample_as_dataframe(ifilename, itreenames, branches, cuts):
    '''
    Load a given sample from PIDCalib and apply the given cuts

    :ifilename:  str, path to the ROOT file from PIDCalib
    :itreenames: array of str, names of the TTrees to load
    :branches:   dict, where keys are the names of the variables in our notation,
                 and the values are the names of the variables in PIDCalib notation
    :cuts:       array of str, all the cuts to apply to the dataframe
    '''
    from utils.rootutils import get_rootfile_as_dataframe

    df = get_rootfile_as_dataframe(ifilename, itreenames, branches.values())

    inverse_branch_dict = {val: key for key,val in branches.items()}
    df = df.rename(columns = inverse_branch_dict)

    for cut in cuts:
        df = _apply_sel(df, cut)

    return df

def mybranches():
    '''
    The keys are the variables in PIDCalib notation. The values are the variables in our own ntuples notation.
    Additional variables may be added.
    '''
    branches_dict = {
        "DLLmu"                 : "PIDmu",
        "IsMuon"                : "isMuon",
        "InMuonAcc"             : "InMuonAcc",
        "TRCHI2NDOF"            : "TRACK_CHI2NDOF_Corr",
        "MC15TuneV1_ProbNNghost": "ProbNNghost_Corr",
        "TRACK_GHOSTPROB"       : "TRACK_GhostProb_Corr",
        "PT"                    : "PT",
        "P"                     : "P",
        "ETA"                   : "ETA",
        "nSPDhits"              : "nSPDHits"
    }

    for key,br in branches_dict.items():
        if key != "nSPDhits":
            branches_dict[key] = ["mu{0}_{1}".format(k,br) for k in range(1,5)]

    return branches_dict
