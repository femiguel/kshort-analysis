import os
import sys
import scripts

from logging import info    as printinfo
from logging import error   as printerror
from logging import warning as printwarning

class UniFunc:
    NMIN = 10
    NBINSMAX = 500
    def __init__(self, var, nbinsmax = NBINSMAX):
        xlist = list(map(float, var))
        xlist.sort()
        n = len(xlist)
        nbins0 = int((1.*n)/self.NMIN)
        self.size = min(nbins0, nbinsmax)
        self.xaxis = self.size*[0.]
        self.yaxis = self.size*[0.]

        supmin = .5/len(var)
        cte    = (1.-2*supmin)/(self.size - 1)
        for i in range(self.size):
            self.yaxis[i] = supmin + i*cte
            jevt = int(i*n*cte)
            jevt = min(jevt, n-1)
            self.xaxis[i] = xlist[jevt]

    def value(self, x, xlist, ylist):
        def bin(x, xlist):
            x = float(x)
            if x <= xlist[0]: return 0, 0
            if x >= xlist[-1]: return self.size-1, self.size-1
            for i in range(self.size):
                if x < xlist[i]:
                    return max(0, i-1), min(self.size-1, i)

        x = float(x)
        ww = bin(x, xlist)
        if not "__len__" in dir(ww):
            print("Crazy, ", x, xlist[0], xlist[-1])

        i,j = ww
        x0 = xlist[i]
        y0 = ylist[i]
        dx = xlist[j] - x0
        dy = ylist[j] - y0
        dydx = 0.
        if i!=j: dydx = dy/dx
        y = y0 + dydx*(x-x0)

        return y

    def __call__(self, x):
        return self.value(x, self.xaxis, self.yaxis)

    def inverse(self, y):
        return self.value(y, self.yaxis, self.xaxis)

class Vector(list):
    def __init__(self, x, y, z):
        list.__init__(self, [x,y,z])
    def __abs__(self):
        from math import sqrt
        
        return sqrt(sum([self[k]**2 for k in range(len(self))]))
    def __add__(self, v2):
        return Vector(*[self[k] + v2[k] for k in range(3)])

    def __sub__(self, v2):
        return Vector(*[self[k] - v2[k] for k in range(3)])

    def __mul__(self, alpha):
        if isinstance(alpha, Vector):
            return sum(self[k]*alpha[k] for k in range(3))
        
        return Vector(*[alpha*self[k] for k in range(3)])

    def __rmul__(self, alpha):
        return Vector(*[alpha*self[k] for k in range(3)])

    def __truediv__(self, alpha):
        if isinstance(alpha, Vector):
            raise Exception("Two vectors cannot be divided")

        return self*(1./alpha)

    def unit(self):
        return self*(1./abs(self))
    
def check_lists_equal(list1, list2):
    '''
    Checks if two given lists are equal

    :list1: array, first array
    :list2: array, second array
    '''
    if len(list1) == len(list2):
        for el in list1:
            if el not in list2: return False
        return True
    return False

def load_data_file(fname):
    '''
    Load the content of a JSON, yaml or pickle file

    :fname: path to the data file
    '''
    ext_pos = fname.rfind(".")
    ext     = fname[ext_pos+1:]
    if ext == "pickle":
        from pickle import load
        mode = "rb"
    elif ext == "json":
        from json import load
        mode = "r"
    elif ext == "yaml":
        from yaml import safe_load as load
        mode = "r"
    else:
        printerror("Cannot open data file. Unknown extension '{}'".format(ext))
        sys.exit(1)

    with open(fname, mode) as outf:
        out = load(outf)
        
    return out

def delete_files(*args):
    '''
    Delete a list of files

    :args: array, list of files to delete
    '''
    for f in args:
        if os.path.exists(f): os.remove(f)

def file_exists(path_to_file):
    '''
    Check whether a file or path exists or not

    :path_to_file: str, given path or path to the file
    '''
    return os.path.exists(path_to_file)

def flatten_array(list_of_lists):
    '''
    Turn a list of lists into a one-dimensional array

    :list_of_lists: array to be turned into 1-d
    '''
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return flatten_array(list_of_lists[0]) + flatten_array(list_of_lists[1:])
    return list_of_lists[:1] + flatten_array(list_of_lists[1:])

def get_filename_from_path(ipath, separator = "/"):
    '''
    Get the name of the file from a given path.
    Example: /path/to/file.ext -> returns file.ext

    :ipath:     str, input path to the file
    :separator: str, character that divides the directories (in some cases can be ".")
    '''
    pos = ipath.rfind(separator)

    return ipath[pos+1:]

def get_path_from_filename(ipath, separator = "/"):
    '''
    Opposite to get_filename_from_path()
    Example: /path/to/file.ext -> returns /path/to

    :ipath:     str, input path to the file
    :separator: str, character that divides the directories (in some cases can be ".")
    '''
    pos = ipath.rfind(separator)

    return ipath[:pos]

def get_key(_array, _value):
    '''
    Retrieves the key for a given value in an array

    :_array: array, given array to find the key in
    :_value: value to look for
    '''
    returnvalues = []
    for k,v in _array.items():
        if v == _value:
            returnvalues += [k]

    return returnvalues if len(returnvalues) > 1 else returnvalues[0]

def integrate(xarray, yarray):
    '''
    Compute the numerical integral of a set of x- and y-coordinates

    :xarray: array, x-coordinates
    :yarray: array, y-coordinates
    '''
    from numpy import abs
    
    if len(xarray) != len(yarray):
        raise ValueError("x and y arrays must have the same length")

    area = 0
    for k in range(len(xarray)-1):
        x1, y1 = xarray[k],   yarray[k]
        x2, y2 = xarray[k+1], yarray[k+1]
        area  += abs(x2-x1)*(y1+y2)*1./2
    return area

def make_dirs(path):
    '''
    Create the directories in the given path if it does not exisst

    :path: str, given path
    '''
    if not file_exists(path):
        os.makedirs(path)

    return path

def save_data_file(fname, obj):
    '''
    Save a given object into a JSON, yaml or pickle file

    :fname: str, path to the file
    :obj:   Object, object to be saved
    '''
    ext_pos = fname.rfind(".")
    ext     = fname[ext_pos+1:]
    if ext == "pickle":
        from pickle import dump
        mode = "wb"
    elif ext == "json":
        from json import dump
        mode = "w"
    elif ext == "yaml":
        from yaml import dump
        mode = "w"
    else:
        printerror("Cannot open data file. Unknown extension '{}'".format(ext))

    with open(fname, mode) as outf:
        dump(obj, outf)
