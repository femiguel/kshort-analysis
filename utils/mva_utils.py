'''
Useful methods to use for the MVA training
'''
from scripts         import printinfo, printwarning, printerror, printdebug, get_project_path
from utils.rootutils import input_tree
from ROOT            import TGraph, TFile, TCanvas, TList, TTree
from array           import array
from scipy           import random as rnd

import os
import numpy
import functools

def check_mode_name(func):
    '''
    Decorates a function to check if the mode and name parameters are correct.
    '''
    @functools.wraps(func)
    def wrapper(mode, name, **kwargs):
        '''
        Internal wrapper
        '''
        if mode.lower() not in ("train", "test", "fom", "apply"):
            printerror("Given mode '{}' is not correct. It must be 'Train', 'Test', 'FOM', or 'Apply'".format(mode))
        if name.lower() not in ("signal", "bkg"):
            printerror("Given parameter 'name', '{}', is not correct. It must be 'Signal' or 'Bkg'".format(name))

        return func(mode, name, **kwargs)

    return wrapper

class MVAInfo:
    vars_training = ["K0S_IP_OWNPV", "mu_minIP_OWNPV", "K0S_FDT_OWNPV", "K0S_DOCA", "VeloMatterVeto", "logTheta"]

    add_muon_vars = lambda var: ["mu{0}_{1}".format(k+1, var) for k in range(4)]
    
    spectators_training  = ["K0S_M"]
    spectators_training += add_muon_vars("PIDmu")
    nfolds = 2
    parity_names = ["even", "odd"]

    if len(parity_names) != nfolds: printerror("Number of MVA folds is different from the number of parity names.")

    ## To complete after training
    optimal_classifiers = {"TIS": "BDT", "xTOS": "BDT"}
    optimal_efficiency  = {"TIS": 0.8  , "xTOS": 0.8}
    
    def get_parity(iteration):
        return "even" if iteration == 0 else "odd"

class CLsCalculator:
    def __init__(self, b, Ntoys = 5000):
        self.b = b
        self.s = 1
        self.toysB = rnd.poisson(self.b, Ntoys)
        self.Ntoys = Ntoys
        self.RunToys()
        
    def RunToys(self):
        self.toysS = rnd.poisson(self.b + self.s,self.Ntoys)
        self.tsB   = list(map(self.ts, self.toysB))
        self.tsS   = list(map(self.ts, self.toysS))
        self.tsB.sort()
        self.tsS.sort()

    def ts(self,Nobs):
        poissonLL = lambda mu,n: n*numpy.log(mu)-mu
        
        chi2b  = -2*poissonLL(self.b, Nobs)
        chi2sb = -2*poissonLL(self.s+self.b, Nobs)

        return chi2b - chi2sb

    def cls(self, Nobs):
        from bisect import bisect

        ts0  = self.ts(Nobs)
        clsb = bisect(self.tsS,ts0)*1./len(self.tsS)
        clb  = bisect(self.tsB,ts0)*1./len(self.tsB)
        return clsb*1./clb
    
    def lexp90(self):
        #if not self.b : return 2.3# return 2*sqrt(self.b)
        for i in range(100):
            self.s = 1 + 0.1*i
            self.RunToys()
            cls_list = list(map(self.cls, self.toysB))
            cls_list.sort()
            cls = cls_list[int(round(0.5*self.Ntoys))]
            if cls < 0.1: break
        return self.s

def add_column_to_tree(ifilename, itreename, variable_name, **kwargs):
    '''
    Adds a TBranch to a given tree with a given variable name.
    This name must be among the ones that are considered here

    :ifilename: str, path to the ROOT file
    :itreename: str, name of the TTree
    :variable_name: str, name of the variable
    '''
    from array import array

    f = TFile.Open(ifilename, "update")
    t = f.Get(itreename)

    slash = itreename.rfind("/")
    odir  = f if slash == -1 else f.Get(itreename.split("/")[0])

    t.SetBranchStatus("*", 0)
    branch_arrays = {}
    if variable_name == "K0S_FDT_OWNPV":
        variables = ["K0S_ENDVERTEX_X", "K0S_ENDVERTEX_Y"]
        func      = transverse_fd

    elif variable_name == "mu_minIP_OWNPV":
        variables = ["mu{}_IP_OWNPV".format(k) for k in range(1,5)]
        func      = min_IP_muons

    elif variable_name == "logTheta":
        variables = ["mu{}_P{}".format(muonid+1, p_comp) for muonid in range(4) for p_comp in ["X", "Y", "Z"]]
        func      = minLogTheta

    elif variable_name == "VeloMatterVeto":
        from scripts.matter_veto import velo

        tool      = velo.VeloMaterial(os.path.join(get_project_path(), "scripts/matter_veto/CL12.1_pars.root"))
        mother    = kwargs["mother"] if "mother" in kwargs else "K0S"
        variables = ["{}_ENDVERTEX_{}{}".format(mother, coor, _type) for _type in ("", "ERR") for coor in ("X", "Y", "Z")]
        func      = tool.distance

    elif "g" in kwargs and "applyClassifier" in kwargs:
        variables = kwargs["variables"]
        func      = kwargs["g"].calculate

    elif variable_name == "AP_pt" or variable_name == "AP_alpha":
        from utils.ana_utils import get_daughters_names

        daughters = get_daughters_names(channel = "K0S2pi2", data_type = kwargs["data_type"])
        variables = ["{0}_P{1}".format(d, p) for d in daughters for p in ("X", "Y", "Z")]
        func      = armenteros_pt if variable_name == "AP_pt" else armenteros_alpha        

    elif variable_name == "WeightValues":
        if "ptname" not in kwargs: printerror("To compute the weight values you must provide the 'ptname' parameter.")
        variables = [kwargs["ptname"]]
        func      = PTw8
        
    elif variable_name[-5:] == "_Corr":
        from utils.rootutils import copy_column
        
        odir.cd()
        t.SetBranchStatus("*", 1)
        t.Write()
        f.Close()
        copy_column(ifilename, itreename, originalName = variable_name.split("_Corr")[0], finalName = variable_name)

        return None
    else:
        raise ValueError("The variable {} is not among the options we are considering. Please include it in the code".format(variable_name))

    for var in variables:
        t.SetBranchStatus(var, 1)
        branch_arrays[var] = array("d", [0])
        t.SetBranchAddress(var, branch_arrays[var])
        
    var_array = array("d", [0])
    branch = t.Branch(variable_name, var_array, variable_name+"/D")
    for evt in range(t.GetEntries()):
        t.GetEntry(evt)
        var_array[0] = func(*[list(branch_arrays.values())[_][0] for _ in range(len(list(branch_arrays.values())))])

        branch.Fill()

    odir.cd()
    t.SetBranchStatus("*", 1)
    if not hasattr(t, variable_name):
        printerror("Something went wrong. Variable {} not in output tree".format(variable_name))
    t.Write()
    f.Close()

def armenteros(var, args):
    '''
    Auxiliary function to add a given Armenteros variable

    :var:  str, name of the variable
    :args: obj, daughter momentum variables
    '''
    from scripts.physics import armenteros_vars
    from utils.pyutils   import Vector

    v1,v2 = [Vector(*[args["d{0}_p{1}".format(k,coor)] for coor in ("x", "y", "z")]) for k in (1,2)]

    alpha, pt = armenteros_vars(v1,v2)

    return alpha if var == "alpha" else pt

def armenteros_alpha(d1_px, d1_py, d1_pz,
                     d2_px, d2_py, d2_pz):
    '''
    Run the computation of the alpha Armenteros variable.

    :d1_px, d1_py, d1_pz: float, momentum components of the first daughter.
    :d2_px, d2_py, d2_pz: float, momentum components of the second daughter.
    '''
    return armenteros("alpha", locals())

def armenteros_pt(d1_px, d1_py, d1_pz,
                  d2_px, d2_py, d2_pz):
    '''
    Run the computation of the PT Armenteros variable.

    :d1_px, d1_py, d1_pz: float, momentum components of the first daughter.
    :d2_px, d2_py, d2_pz: float, momentum components of the second daughter.    
    '''
    return armenteros("pt", locals())

def transverse_fd(x_array, y_array):
    '''
    Compute the transverse flight distance variable.

    :x_array: float, x-component of the end vertex
    :y_array: float, y-component of the end vertex
    '''
    return numpy.sqrt(x_array**2 + y_array**2)

def min_IP_muons(mu1_array, mu2_array, mu3_array, mu4_array):
    '''
    Compute the minimum IP of the four muons.

    :mu_array: float, IP of each of the muons
    '''
    return min(mu1_array, mu2_array, mu3_array, mu4_array)

def minLogTheta(mu1_px, mu1_py, mu1_pz,
                mu2_px, mu2_py, mu2_pz,
                mu3_px, mu3_py, mu3_pz,
                mu4_px, mu4_py, mu4_pz):
    '''
    Compute a variable for the MVA that is the minimum of the angle between each pair of tracks.

    :mu1_px, mu1_py, mu1_pz: float, momentum components of the first muon.
    :mu2_px, mu2_py, mu2_pz: float, momentum components of the second muon.    
    :mu3_px, mu3_py, mu3_pz: float, momentum components of the third muon.
    :mu4_px, mu4_py, mu4_pz: float, momentum components of the fourth muon.    
    '''
    theta = lambda muon1, muon2: numpy.log(1-(sum([muon1[k]*muon2[k] for k in range(3)]))*1./(numpy.sqrt(sum([muon1[k]**2 for k in range(3)]))*numpy.sqrt(sum([muon2[k]**2 for k in range(3)]))))

    mu1 = [mu1_px, mu1_py, mu1_pz]
    mu2 = [mu2_px, mu2_py, mu2_pz]
    mu3 = [mu3_px, mu3_py, mu3_pz]
    mu4 = [mu4_px, mu4_py, mu4_pz]

    return min(theta(mu1, mu2), theta(mu1, mu3), theta(mu1, mu4),
               theta(mu2, mu3), theta(mu2, mu4), theta(mu3, mu4))    

def PTw8(pt):
    if pt > 119.4 and pt < 1584.5:
        return PTw8Pol(pt)
    
    return PTw8Log(pt)

def PTw8Log(pt):
    return 2.69688e+00 + 1.32875e-01*numpy.log( 3.89190e-09 * pt)

def PTw8Pol(pt):
    return (  0.721886  + 0.000249358*pt  + 6.00346e-07  * pt**2  -5.78713e-10  * pt**3 + 1.2387e-13 * pt**4)

def get_paths(key):
    '''
    Returns the path where the MVA files are stored for a given key.
    If necessary, it creates the intermediate paths.

    :key: str, key to retrieve the files for.
    '''
    from utils.pyutils import make_dirs
    from scripts       import get_tuples_path

    _join = lambda path: os.path.join(get_tuples_path(), path)

    paths = {"MVA"        : _join("MVA")}
    paths.update({"Train"      : _join("MVA/Train"),
                  "Test"       : _join("MVA/Test"),
                  "FOM"        : _join("MVA/FOM"),
                  "Apply"      : _join("MVA/Apply"),
                  "TMVA_Output": _join("MVA/TMVA_Output"),
                  "Sklearn_Output": _join("MVA/Sklearn_Output"),
                  "ExpectedLimit" : _join("MVA/ExpectedLimit"),
                  "Prospects"     : _join("MVA/ExpectedLimit/Prospects")})

    for path in list(paths.values()):
        make_dirs(path)

    return paths[key]

def get_filenames(key):
    '''
    Get the MVA file name for a given key.

    :key: str, key to retrieve the file name for.
    '''
    from utils.ana_utils import years

    filenames = {}
    for mode in ["Train", "Test", "FOM", "Apply"]:
        filenames[mode] = os.path.join(get_paths(mode), "{mode}_{name}_{triggercat}.root")
        for year in years:
            filenames[mode+year] = os.path.join(get_paths(mode), "{mode}_{name}_{year}_{triggercat}.root")
            filenames[mode+year+"Full"] = os.path.join(get_paths(mode), "full_{name}_{year}_{triggercat}.root")
        filenames[mode+"Full"] = os.path.join(get_paths(mode), "full_{name}_{triggercat}.root")

    filenames["TMVA_Output"] = os.path.join(get_paths("TMVA_Output"), "TMVA_output_{iteration}_{triggercat}.root")
    filenames["TMVA_MergedTest"] = os.path.join(get_paths("TMVA_Output"), "MergedTest_{triggercat}.root")
    filenames["Sklearn_Output"] = os.path.join(get_paths("Sklearn_Output"), "Sklearn_output_{iteration}_{triggercat}.pickle")
    filenames["Sklearn_MergedTest"] = os.path.join(get_paths("Sklearn_Output"), "MergedTest_{triggercat}.pickle")

    filenames["TMVA_ROC"] = os.path.join(get_paths("TMVA_Output"), "roc_curve_{triggercat}.png")
    filenames["ClassifierDistribution"] = os.path.join(get_paths("TMVA_Output"), "{classifier}_{parity}_dist_{triggercat}.png")
    filenames["TMVA_FoM"] = os.path.join(get_paths("TMVA_Output"), "{FoM}_{classifier}_{parity}_{triggercat}.png")

    for year in years:
        filenames["FullMC"+year] = os.path.join(get_paths("ExpectedLimit"), "MC_{channel}_{year}_WithClassifier.root")
    filenames["FullMC"] = os.path.join(get_paths("ExpectedLimit"), "MC_{channel}_WithClassifier.root")

    #filenames["SignalFitParameters"] = os.path.join(get_project_path(), "scripts/Ipatia_Fit_Parameters.yaml")

    filenames["FakeUnblind"] = os.path.join(get_paths("ExpectedLimit"), "bkg_FakeUnblind_{triggercat}.root")
    filenames["Prospects"] = os.path.join(get_paths("Prospects"), "bkg_FakeUnblind_Lumi_{lumi}.root")

    return filenames[key]

def has_classifier(func):
    '''
    Decorator of a function to determine if it has a given classifier.
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        from utils.rootutils import has_branch
        from utils.rootutils import add_variables_into_tree
        
        bdt_branches = [kwargs["classifier"]+"_"+kwargs["triggercat"]+"_"+iter for iter in MVAInfo.parity_names]
        
        if has_branch(ifilename = kwargs["ifilename"], itreename = kwargs["itreename"], branches = bdt_branches):
            printinfo("This tree already has {} applied. Jumping out.".format(kwargs["classifier"]))
            return None

        add_variables_into_tree(kwargs["ifilename"], kwargs["itreename"], kwargs["variables"]+kwargs["spectators"])
        
        return func(**kwargs)

    return wrapper

@check_mode_name
def create_tuples(mode, name, triggercat, nfolds, variables, spectators):
    '''
    Create the MVA tuples.

    :mode:       str, ("Train", "Test", "FOM", "Apply")
    :name:       str, ("signal", "bkg")
    :triggercat: str, ("TIS", "xTOS")
    :nfolds:     int, number of folds of the MVA
    :variables:  array of str, names of the variables used in the training
    :spectators: array of str, names of the variables that are not used in the training, but kept in the MVA tuples
    '''
    from utils.pyutils   import make_dirs, delete_files
    from utils.rootutils import copytree, merge_files, add_variables_into_tree
    from utils.ana_utils import years
    from scripts.files   import prepare_files
    
    flist = []
    for year in years:
        ofilename = get_filenames(mode+year).format(**locals())
        delete_files(*[ofilename])

        ifilename = prepare_files(data_type = "Data" if name == "Bkg" else "MC",
                                  year      = year,
                                  channel   = "K0S2mu4")
        itreename = "K0s2MuMuMuMuLine/DecayTree_{}".format(triggercat)

        for i in range(nfolds):
            sel = get_selection(name      = name,
                                mode      = mode,
                                iteration = i)
            copytree(ifilename = ifilename,
                     itreename = itreename,
                     ofilename = ofilename,
                     otreename = "DecayTree",
                     suffix    = "_Iter{}".format(i),
                     sel       = sel)

            add_variables_into_tree(ifilename = ofilename,
                                    itreename = "DecayTree_Iter{}".format(i),
                                    variables = variables+spectators)

        flist += [ofilename]

    merge_files(ofilename = get_filenames(mode).format(**locals()), ifiles = flist)

@check_mode_name
def get_selection(mode, name, iteration):
    from scripts.selections import cuts

    selpid = str(cuts["pidmu"])
    if mode == "Apply":
        return "(eventNumber%2=={0})&&{1}".format(iteration, selpid)

    basicsel = "(eventNumber%4=={})"

    if mode == "Train" or (mode == "Test" and name == "Bkg"):
        basicsel = basicsel.format(int(not iteration))
    else:
        basicsel = basicsel.format(3 if iteration == 0 else 2)

    if mode == "FOM" and name == "Bkg":
        return basicsel

    return "({0})&&{1}({2})".format(basicsel, "!" if mode == "Train" and name == "Bkg" else "",  selpid)

def setup_mvatuples(func):
    '''
    Decorator to set up MVA tuples.
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        '''
        Internal wrapper
        '''
        from scripts import get_tuples_path
        
        variables   = MVAInfo.vars_training
        spectators  = MVAInfo.spectators_training
        nfolds      = MVAInfo.nfolds
        
        if "create_tuples" in kwargs and kwargs["create_tuples"]:
            for name in ["Signal", "Bkg"]:
                for mode in ["Train", "Test", "FOM", "Apply"]:
                    create_tuples(mode       = mode,
                                  name       = name,
                                  triggercat = kwargs["triggercat"],
                                  nfolds     = nfolds,
                                  variables  = variables,
                                  spectators = spectators)

        return func(variables   = variables,
                    spectators  = spectators,
                    nfolds      = nfolds,
                    **kwargs)

    return wrapper

@input_tree
def compute_ROC(itree, classifiers, output_path = "./", ofilename = "roc_curve.png", **kwargs):
    '''
    Compute the arrays to draw the ROC Curve

    :itree:       TTree, input tree that has been tested in TMVA training
    :classifiers: array, list of classifiers that will be included in the Curve
    :output_path: str, path where the output PNG file will be stored
    :ofilename:   str, name of the output PNG file
    '''
    from utils.pyutils import integrate, get_key
    from utils.plot_utils import PlotOptions, drawGraph
    
    signal_arrays, bkg_arrays, auc = {}, {}, {}
    thres = numpy.linspace(-1, 1, 500)
    printinfo("Drawing ROC Curve")
    for c in classifiers:
        printdebug("Creating signal and bkg arrays for {} classifier".format(c))
        s, b = [numpy.array([0]*len(thres), "float") for k in range(2)]

        for k in range(len(thres)):
            s[k] = itree.GetEntries("{0}>{1}&&classID==0".format(c, thres[k]))*1./itree.GetEntries("classID==0")
            b[k] = 1. - itree.GetEntries("{0}>{1}&&classID==1".format(c, thres[k]))*1./itree.GetEntries("classID==1")
        
        auc[c] = integrate(xarray = s, yarray = b)
        printdebug("{0} Area Under Curve: {1}".format(c, auc[c]))

        if auc[c]>0.99:
            signal_arrays[c], bkg_arrays[c] = s,b

    best_auc = max([auc[c] for c in classifiers])
    best_c   = get_key(_array = auc, _value = best_auc)
    printinfo("The best classifier is {0}, with AUC = {1}".format(best_c, best_auc))

    from ROOT import kBlue, kMagenta, kCyan, kGreen, kOrange, kRed, kYellow, kPink, kViolet, kAzure
    colors = [kBlue, kMagenta, kCyan, kGreen, kOrange, kRed, kYellow, kPink, kViolet, kAzure]

    zoom_array = [[0,1], [0.99,1]]

    plotOpts = {}
    for k,c in enumerate(classifiers):
        if c in signal_arrays:
            plotOpts[c] = PlotOptions(linecolor       = colors[k],
                                      linewidth       = 4,
                                      xtitle          = "Signal Acceptance",
                                      ytitle          = "Background Rejection",
                                      zoom            = zoom_array,
                                      legendSize      = (0.3, 0.3),
                                      legendTextSize  = 0.05)

    printinfo("Plotting the curves")
    drawGraph(x             = signal_arrays,
              y             = bkg_arrays,
              plotOpts      = plotOpts,
              ofilename     = os.path.join(output_path, ofilename),
              includeLegend = True)

    return best_c

def compute_FOM(FOM, bdt_varname, triggercat, iteration, classifier, ofilename):
    '''
    Compute a given Figure of Merit.

    :FOM:         str, name of the Figure of Merit to compute.
    :bdt_varname: str, name of the classifier variable in the TTree
    :triggercat:  str, ("TIS", "xTOS")
    :iteration:   int, number of the iteration
    :classifier:  str, name of the classifier applied
    :ofilename:   str, path to the output file name
    '''
    from scripts.selections import cuts
    from utils.rootutils    import get_entries

    if FOM != "CLs":
        printerror("Figure of merit {} has not been implemented".format(FOM))

    mode = "FOM"
    #parity = "even" if iteration == 0 else "odd"
    parity = get_parity(iteration)
    farray, itrees = [], {}
    printdebug("Opening TFiles for FOM")
    
    for name in ["Signal", "Bkg"]:
        printdebug("Opening {} file".format(name))
        f = TFile.Open(get_filenames(mode).format(**locals()))
        t = f.Get("DecayTree_Iter{}".format(iteration))

        farray += [f]; itrees[name] = t
    
    def _compute_eff_parity(parity_cut):
        from scripts.files_info import info
        from scripts.files      import prepare_files
        from scripts            import get_tuples_path
        from utils.ana_utils    import years
        import tempfile

        printdebug("Computing parity efficiency")

        data_type = "Data"; channel = "K0S2mu4"
        f_array = []; t_array = []; tlist = TList()
        
        for year in years:
            fname = prepare_files(data_type, year, channel)
            tname = info["Trees"][channel][data_type][year][channel]+"_"+triggercat
            f = TFile.Open(fname)
            t = f.Get(tname)
            f_array += [f]; t_array += [t]; tlist.Add(t)

        with tempfile.TemporaryDirectory() as tmpdir:
            tmpfile = os.path.join(tmpdir, "eraseme.root")

            dummyf = TFile.Open(tmpfile, "recreate")
            outt   = TTree.MergeTrees(tlist)

            eff = get_entries(itree = outt, sel = parity_cut)/get_entries(itree = outt)

            dummyf.Close()

        for f in f_array: f.Close()

        return eff

    Npoints = 30
    g = TGraph()
    paritycut = get_selection(name = name,
                              mode = "FOM",
                              iteration = iteration)
    cut = "&&".join([paritycut, bdt_varname+">{cutval}"])

    eff_parity = _compute_eff_parity(paritycut)
    eff_pid    = get_entries(itree = itrees["Bkg"],
                             sel   = str(cuts["pidmu"])+"&&"+paritycut)
    eff_pid   /= get_entries(itree = itrees["Bkg"],
                             sel   = paritycut)

    printdebug("Parity efficiency computed to be {}".format(eff_parity))

    SF  = (510 - 490)/((490-450) + (600-510)) ## scale factor
    SF *= eff_pid/eff_parity

    from tqdm import tqdm
    for i in tqdm(range(Npoints+1), leave = True, desc = "Running toys"):
        cutval = i/Npoints - 0.3
        Nb = get_entries(itree = itrees["Bkg"], sel = cut.format(**locals()))
        Nb_exp = SF*Nb

        eff = get_entries(itree = itrees["Signal"],
                          sel = cut.format(**locals()))/get_entries(itree = itrees["Signal"], sel = paritycut)
        if not eff: break
        if Nb_exp > 0:
            caca = CLsCalculator(Nb_exp)
            Ns90 = caca.lexp90()
        else:
            Ns90 = 2.3
        BR90exp = Ns90/eff ## the expected limit is the median of the limits

        g.SetPoint(i, eff*100, BR90exp)

    g.GetYaxis().SetRangeUser(0, 10)
    g.GetXaxis().SetTitle("#varepsilon (%)")
    g.GetXaxis().SetRangeUser(60, 100)
    g.GetYaxis().SetTitle("Expected limit (arbitrary units)")

    c = TCanvas()
    g.Draw("AL*")
    c.Update()

    c.SaveAs(ofilename)

    input("Waiting for approval. Press ENTER to continue.")

    for f in farray:
        f.Close()

def getBDTsel(year, triggercat, parity, classifier, classifier_eff):
    '''
    Compute the classifier selection for a given classifier efficiency.

    :year:           str or int, (2016, 2017, 2018)
    :triggercat:     str, ("TIS", "xTOS")
    :parity:         str, ("even", "odd")
    :classifier:     str, name of the classifier to cut at
    :classifier_eff: float, efficiency of the MVA at which we want to cut
    '''
    from utils.file_utils import get_ofilename
    from utils.rootutils  import get_rootfile_as_dataframe
    from utils.selutils   import Selection
    from utils.pyutils    import UniFunc
    from scripts.runTMVA  import TMVAInfo

    printdebug("Getting the selection corresponding to the {} efficiency {}".format(classifier, classifier_eff))

    mode = "Apply"
    name = "Signal"
    iteration = 0 if parity == "even" else 1

    ifilename = get_filenames(mode+year+"Full").format(**locals())
    ifilename = get_ofilename("Classifier", ifilename)

    itreename = "DecayTree"

    branches  = [TMVAInfo.ClassifierBranches.format(**locals())]
    sel    = get_selection(name = name, mode = mode, iteration = iteration).replace("&&", "&").replace("||", "|")

    printdebug("Getting {} as dataframe with selection {}".format(ifilename, sel))
    df = get_rootfile_as_dataframe(ifilename  = ifilename,
                                   itreenames = [itreename],
                                   branches   = branches,
                                   cut        = sel)

    var          = df[TMVAInfo.ClassifierBranches.format(**locals())]
    var_uniform  = UniFunc(var)
    
    cut = var_uniform.inverse(1 - classifier_eff)
    out = "{0}>{1}".format(TMVAInfo.ClassifierBranches.format(**locals()), cut)

    printdebug("The cut is {}".format(out))
    
    return out
