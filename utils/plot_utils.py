'''
Useful methods for plotting in ROOT.
'''
import os

from collections import namedtuple
from numpy       import linspace
from scripts     import printinfo, printerror, printwarning, printdebug
from ROOT        import TFile, TH1F, TCanvas, TGraph, TLegend, TGaxis, kTRUE, TPaveText

_PlotOptions = namedtuple("_PlotOptions", ["linecolor", "linestyle", "linewidth", "fillcolor", "fillstyle", "xtitle", "ytitle", "zoom", "legendSize", "legendTextSize", "legendFillStyle", "legendName", "labelTextSize", "labelFillColor", "nbins"])
class PlotOptions(_PlotOptions):
    def __new__(cls, linecolor = None, linestyle = None, linewidth = None, fillcolor = None, fillstyle = None, xtitle = "", ytitle = "", zoom = None, legendSize = None, legendTextSize = None, legendFillStyle = None, legendName = None, labelTextSize = None, labelFillColor = None, nbins = None):
        return super(PlotOptions, cls).__new__(cls, linecolor, linestyle, linewidth, fillcolor, fillstyle, xtitle, ytitle, zoom, legendSize, legendTextSize, legendFillStyle, legendName, labelTextSize, labelFillColor, nbins)

def drawGraph(x, y, plotOpts, ofilename = "", includeLegend = False):
    '''
    Generic function to draw a TGraph with two given arrays
    and a given set of options

    :x:             array, x coordinates of the points to be drawn
    :y:             array, y coordinates of the points to be drawn
    :plotOpts:      PlotOptions object, contains the options for plotting
    :ofilename:     str, name of the output file
    :includeLegend: bool, whether or not to include a Legend in the TGraph
    '''
    printdebug("Legend options: {}".format(plotOpts[list(x.keys())[0]].legendSize))
    canvas = TCanvas("canvas", "canvas", 1920, 1080)
    legend = TLegend(*plotOpts[list(x.keys())[0]].legendSize) if includeLegend is True and plotOpts[list(x.keys())[0]].legendSize is not None else TLegend() if includeLegend is True and plotOpts[list(x.keys())[0]].legendSize is None else None
    graphs = []
    n = len(x[list(x.keys())[0]])
    for i,key in enumerate(list(x.keys())):
        graph_name = "{}_graph".format(key)
        gr = TGraph(len(x[key]), x[key], y[key])
        gr.SetName(graph_name)
        if plotOpts[key].linecolor is not None:
            gr.SetLineColor(plotOpts[key].linecolor)
        if plotOpts[key].linewidth is not None:
            gr.SetLineWidth(plotOpts[key].linewidth)
        if i == 0:
            gr.Draw()
            gr.GetXaxis().SetTitle(plotOpts[key].xtitle)
            gr.GetYaxis().SetTitle(plotOpts[key].ytitle)
            if plotOpts[key].zoom is not None:
                gr.GetXaxis().SetRangeUser(*plotOpts[key].zoom[0])
                gr.GetYaxis().SetRangeUser(*plotOpts[key].zoom[1])
        else:
            gr.Draw("same")
        graphs.append(gr)
        if legend is not None:
            legend.AddEntry(graph_name, key, "l")
    canvas.Update()
    if legend is not None:
        if plotOpts[list(x.keys())[0]].legendTextSize is not None: legend.SetTextSize(plotOpts[list(x.keys())[0]].legendTextSize)
        legend.Draw()
    if ofilename != "": canvas.SaveAs(ofilename)
    input("Waiting for approval. Press ENTER.")

def setup_histframe(h_array, xtitle = None, logy = False, force_ymin = None):
    '''
    Method that sets up the histogram frame where one or multiple histograms will be plotted.
    This is useful so that the height of the frame is not dictated by the first plotted histogram,
    but rather, the tallest one. The same goes for the x-axis.

    :h_array:    array of TH1 objects, histograms that will be drawn in the frame
    :xtitle:     str, name of the x-axis
    :logy:       bool, True if the y-axis is to have logarithmic scale
    :force_ymin: float or None, if not None, minimum value for the y-axis. This is useful when logy is set to True and there are empty bins
    '''
    xmin = min(map(lambda h: h.GetXaxis().GetXmin(), h_array))
    xmax = max(map(lambda h: h.GetXaxis().GetXmax(), h_array))

    ymin = min(map(lambda h: h.GetMinimum(), h_array))
    ymax = max(map(lambda h: h.GetMaximum(), h_array))
    ymax += 0.1*(ymax - ymin) if not logy else (ymax-ymin)

    printdebug("ymin = {}; ymax = {}".format(ymin, ymax))

    hframe = h_array[0].__class__("h", "h", 1, xmin, xmax)
    if xtitle:
        hframe.GetXaxis().SetTitle(xtitle)
    if force_ymin is not None:
        ymin = force_ymin
    else:
        ymin = max(ymin, 2e-5)
    hframe.GetYaxis().SetRangeUser(ymin, ymax)

    hframe.Draw()
    
    return hframe

def normalize( h, sumw2 = True , normalize_to = None):
    '''
    Normalize the given histogram

    :h:            TH1 obj, Histogram to normalize
    :sumw2:        bool, True if apply the sum of weights rule for the errors
    :normalize_to: float or None, if not None, it will be a value to which the histograms will be normalized.
    '''
    if sumw2:
        h.Sumw2()

    if normalize_to is None:
        h.Scale(1./h.GetSumOfWeights())
    else:
        printdebug("Normalizing to {}".format(normalize_to))
        h.Scale(normalize_to)

def draw_hist(ifiles, itrees, branches, plotOpts, ofilename = "", _drawNorm = False, normalize_to = None, sel = None, includeLegend = False, xtitle = None, logx = False, logy = False, force_ymin = None, lhcb_label = False, lhcb_label_preliminary = False, units = "", options = "hist"):
    '''
    Main method to call when drawing a histogram.

    :ifiles:        dict, where the keys are labels for each of the input files and the values are the paths to those files. If two TTrees are to be included from the same TFile, the keys must be different but the values can be the same
    :itrees:        dict, where the keys are the same as in ifiles and the values are the names of the TTrees to open from the corresponding input file
    :branches:      dict, where the keys are the same as in ifiles and the values are dicts, where the keys are the names of the branches plotted for each input file and the values are the ranges from that branch
    :plotOpts:      dict, where the keys are the same as in ifiles and the values are PlotOptions objects
    :ofilename:     str, path to the output file name where the histogram will be stored
    :_drawNorm:     bool, True if the histogram is to be normalized
    :normalize_to:  float or None, if not None, it will be a value to which the histograms will be normalized.
    :sel:           dict, where the keys are the same as in ifiles and the values are strings or Selection objects, containing selections for each of the input files
    :includeLegend: bool, True if a TLegend object is to be included
    :xtitle:        str, name of the x-axis
    :logx:          bool, True if the x-axis is to have logarithmic scale
    :logy:          bool, True if the y-axis is to have logarithmic scale
    :force_ymin:    float or None, if not None, minimum value for the y-axis. This is useful when logy is set to True and there are empty bins
    :lhcb_label:    bool, True if the LHCb label is to be added to the plot
    :lhcb_label_preliminary: bool, True if the text "preliminary" is to be added to the LHCb label
    :units:         str, units of the variable in the x-axis
    '''
    f_array, t_array, h_array = [], [], []

    if sel is None: sel = {key: "" for key in ifiles}
    for k,v in sel.items(): sel[k] = str(v) ## in case the values are Selection objects
    if normalize_to is None: normalize_to = {key: None for key in ifiles}

    canvas = TCanvas("canvas", "canvas", 1920, 1080)
    legend = TLegend(*plotOpts[list(ifiles.keys())[0]].legendSize) if includeLegend is True and plotOpts[list(ifiles.keys())[0]].legendSize is not None else TLegend() if includeLegend is True and plotOpts[list(ifiles.keys())[0]].legendSize is None else None
    if logx:
        canvas.SetLogx()
    if logy:
        canvas.SetLogy()
    for j, key in enumerate(list(ifiles.keys())):
        f = TFile.Open(ifiles[key])
        t = f.Get(itrees[key])
        try:
            printdebug("Loaded TTree {0} from TFile {1} ({2} events)".format(t.GetName(), ifiles[key], t.GetEntries()))
        except ReferenceError:
            printerror("Failed opening TFile {}".format(ifiles[key]))
            continue
        f_array += [f]; t_array += [t]

        h = TH1F("h"+str(key), "h"+str(key), plotOpts[key].nbins, *branches[key][list(branches[key].keys())[0]])
        for br in list(branches[key].keys()):
            t.Project("h"+str(key), br, sel[key])
        if plotOpts[key].linecolor:
            h.SetLineColor(plotOpts[key].linecolor)
        if plotOpts[key].linewidth:
            h.SetLineWidth(plotOpts[key].linewidth)
        if plotOpts[key].linestyle:
            h.SetLineStyle(plotOpts[key].linestyle)
        if plotOpts[key].fillcolor:
            h.SetFillColor(plotOpts[key].fillcolor)
        if plotOpts[key].fillstyle:
            h.SetFillStyle(plotOpts[key].fillstyle)
        
        if _drawNorm: normalize(h, normalize_to = normalize_to[key])
        if legend is not None:
            legend.AddEntry(h, plotOpts[key].legendName, "l")

        h_array += [h]

    hframe = setup_histframe(h_array, xtitle+" ("+units+")" if xtitle is not None and units != "" else xtitle, logy, force_ymin)
    for h in h_array:
        h.Draw("same "+options)
    canvas.Update()
    if legend is not None:
        if plotOpts[list(ifiles.keys())[0]].legendTextSize:
            legend.SetTextSize(plotOpts[list(ifiles.keys())[0]].legendTextSize)
        if plotOpts[list(ifiles.keys())[0]].legendFillStyle:
            legend.SetFillStyle(plotOpts[list(ifiles.keys())[0]].legendFillStyle)

        legend.Draw()

    if lhcb_label:
        from utils.ana_utils import lumis
        pavesize      = plotOpts[list(ifiles.keys())[0]].labelTextSize or (0, 1, 0.5, 0.8)
        pavefillcolor = plotOpts[list(ifiles.keys())[0]].labelFillColor or 0
        total_lumi    = round(sum(list(lumis.values())), 1)
        
        pave = TPaveText(*pavesize, "NDC""NB")
        pave.SetFillColor(pavefillcolor)
        pave.AddText("LHCb"+" Preliminary"*lhcb_label_preliminary)
        pave.AddText(str(total_lumi)+" fb^{-1}")
        pave.Draw("same")

    canvas.Update()
        
    if ofilename != "": canvas.SaveAs(ofilename)

    input("Waiting for Approval. Press ENTER to continue.")

    for f in f_array:
        f.Close()
