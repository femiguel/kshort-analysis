'''
Useful methods to use when working with ROOT.
'''
import os
import functools
import ROOT

from scripts        import printinfo, printerror, printwarning, printdebug
from utils.selutils import format_sel
#from ROOT           import TFile, TDirectoryFile, TTree, TList

## keys are types in ROOT, values are types in array
type_correspondence = {"B": "b",
                       "b": "B",
                       "S": "h",
                       "s": "H",
                       "I": "i",
                       "i": "I",
                       "F": "f",
                       "f": "f",
                       "D": "d",
                       "d": "d",
                       "L": "q",
                       "l": "Q",
                       "O": "I"}

def input_tree(func):
    '''
    Decorate a function to input a TTree object if only the input filename is given.
    Another option is to already input a TTree, in which case it will proceed as normal.
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        '''
        Internal wrapper
        '''
        if "itree" in kwargs:
            return func(**kwargs)
        if "ifilename" not in kwargs or "itreename" not in kwargs:
            printerror("You must input either a TTree or a filename and treename")

        ifile = ROOT.TFile.Open(kwargs["ifilename"])
        itree = ifile.Get(kwargs["itreename"])

        if not itree:
            printerror("TTree {0} does not exist in {1}".format(kwargs["itreename"], kwargs["ifilename"]))
        if isinstance(itree, ROOT.TDirectoryFile):
            printerror("Input parameter 'itreename' ({}) is a TDirectory, not a TTree".format(itree.GetName()))

        to_return = func(itree = itree, **kwargs)

        ifile.Close()

        return to_return

    return wrapper

def input_trees(func):
    '''
    Decorate a funciton to input multiple TTree objects if only the input filename is given.
    Another option is to already input a list of TTree objects, in which case it will proceed as normal.
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        '''
        Internal wrapper
        '''
        if "itrees" in kwargs:
            return func(**kwargs)

        ## Sanity checks
        if ("ifilename" not in kwargs or "itreenames" not in kwargs) and ("ifilenames" not in kwargs or "itreename" not in kwargs) and ("ifilenames" not in kwargs or "itreenames" not in kwargs):
            printerror("You must input either a list of TTrees, a filename and a list of tree names, a list of filenames and a treename, or a list of filenames and a list of treenames.")
        if "ifilenames" in kwargs and not isinstance(kwargs["ifilenames"], list):
            printerror("The input 'ifilenames' parameter must be a list")
        if "itreenames" in kwargs and not isinstance(kwargs["itreenames"], list):
            printerror("The input 'itreenames' parameter must be a list")
        if "ifilenames" in kwargs and "itreenames" in kwargs:
            if len(kwargs["ifilenames"]) != len(kwargs["itreenames"]):
                printerror("The input lists of filenames and treenames must have the same length")

        ## Open TFiles
        if "ifilename" in kwargs:
            ifiles = [ROOT.TFile.Open(kwargs["ifilename"]) for _ in range(len(kwargs["itreenames"]))]
        else:
            ifiles = [ROOT.TFile.Open(ifilename) for ifilename in kwargs["ifilenames"]]

        ## Open TTrees
        itreenames = kwargs["itreenames"] if "itreenames" in kwargs else [kwargs["itreename"] for _ in range(len(ifiles))]
        tlist = []
        for k,ifile in enumerate(ifiles):
            tlist += [ifile.Get(itreenames[k])]

        ## Sanity checks on TTrees
        for itree in tlist:
            if not itree:
                printerror("TTree {0} does not exist in {1}".format(kwargs["itreename"], kwargs["ifilename"]))
            if isinstance(itree, ROOT.TDirectoryFile):
                printerror("Input tree name {} is a TDirectory, not a TTree".format(itree.GetName()))
        
        ## Run function
        to_return = func(itrees = tlist, **kwargs)
        
        for ifile in ifiles:
            ifile.Close()

        return to_return

    return wrapper

def mctruth_prepare(func):
    '''
    Decorate a function where a MCDecayTree may be passed.
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        '''
        Internal wrapper
        '''
        from utils.pyutils   import get_filename_from_path, get_path_from_filename
        if "mc_prepared" in kwargs and kwargs["mc_prepared"]:
            return func(**kwargs)
        if "MCDecayTree" in kwargs["itree"].GetName() and "mc_prepared" not in kwargs:
            if "correction" in kwargs and kwargs["correction"] == "AcceptReject":
                return func(**kwargs)
            else:
                newfunc = func if func.__name__ == "copytree" else copytree
                return newfunc(itree     = kwargs["itree"],
                               ofilename = kwargs["ofilename"],
                               otreename = get_filename_from_path(kwargs["itreename"]),
                               option    = "update",
                               odir      = get_path_from_filename(kwargs["itreename"]),
                               branches  = ["K0S_TRUEPT"], ## if Accept-Reject is not the first correction, WeightValues must be added here
                               mc_prepared = True)

        return func(**kwargs)

    return wrapper

def add_variables_into_tree(ifilename, itreename, variables, **kwargs):
    '''
    Add multiple variables into a TTree

    :ifilename: str, path to the input file
    :itreename: str, name of the input TTree
    :variables: array of str, names of the variables to add
    '''
    from utils.mva_utils import add_column_to_tree
    
    f = ROOT.TFile.Open(ifilename)
    tree = f.Get(itreename)
    for var in variables:
        if not hasattr(tree, var):
            f.Close()
            printdebug("Adding variable {0} to {1} from {2}".format(var, itreename, ifilename))
            add_column_to_tree(ifilename, itreename, var, **kwargs)
            f = ROOT.TFile.Open(ifilename)
            tree = f.Get(itreename)

    f.Close()

@input_tree
def branches_by_regex(itree, regex, **kwargs):
    '''
    Returns all the branches inside a given TTree for a given ergular expression
    The input_tree wrapper allows to call this function by giving it an input filename and an input treename.

    :itree: TTree, input TTree
    :regex: str, regular expression to select branch names
    '''
    import re

    all_branches = [br.GetName() for br in list(itree.GetListOfBranches())]

    comp    = re.compile(regex)
    matches = map(comp.match, all_branches)

    return list(map(lambda m: m.string, filter(lambda m: m is not None, matches)))

@input_tree
def check_duplicates(itree, mvar, **kwargs):
    '''
    Method that checks for duplicates inside an input TTree

    :itree: TTree obj, input Tree
    :mvar:  str, name of the mass variable
    '''
    d, d2 = {}, {}
    l     = []
    for entry in itree:
        run, evt = entry.runNumber, entry.eventNumber
        if run not in d.keys(): 
            d[run], d2[run] = [], []
        if evt not in d[run]: 
            d[run].append(evt)
        else:
            d2[run].append(evt)

    for entry in itree:
        run, evt = entry.runNumber, entry.eventNumber
        if evt in d2[run]: 
            l.append([run, evt, getattr(entry, mvar)])
    l.sort()

    return d, d2, l

def copy_column(ifilename, itreename, originalName, finalName):
    '''
    Copies a column with a given name into another column with another name

    :ifilename:    str, path to the ROOT file
    :itreename:    str, name of the TTree
    :originalName: str, name of the TBranch that is already inside the TTree
    :finalName:    str, name of the TBranch that will be created
    '''
    from array import array

    printdebug("Cloning Branch {0} into {1} in TTree {2} from TFile {3}".format(originalName, finalName, itreename, ifilename))

    f = ROOT.TFile.Open(ifilename, "update")
    t = f.Get(itreename)

    slash = itreename.rfind("/")
    odir  = f if slash == -1 else f.Get(itreename.split("/")[0])

    brname, brtype = get_branch_titles(itree = t, branchnames = [originalName])[0].split("/")
    original_branch_array, final_branch_array = [array(type_correspondence[brtype], [0]) for k in range(2)]

    t.SetBranchAddress(originalName, original_branch_array)
    branch    = t.Branch(finalName, final_branch_array, finalName+"/"+brtype)
    for evt in range(t.GetEntries()):
        t.GetEntry(evt)
        final_branch_array[0] = original_branch_array[0]

        branch.Fill()
    odir.cd()
    t.Write()
    f.Close()

@format_sel
@input_tree
@mctruth_prepare
def copytree(itree, ofilename, otreename = "DecayTree", suffix = "", sel = "", option = "update", odir = None, branches = None, *args, **kwargs):
    '''
    Create a copy of the input TTree with a given selection, and store it in a given ROOT file
    The input_tree wrapper allows to call this function by giving it an input filename and an input treename.

    :ofilename: str, path to the output file
    :itree:     TTree, input TTree
    :otreename: str, name of the output TTree
    :suffix:    str, suffix to add to the otreename
    :sel:       str or Selection object
    :branches:  array of str, list of branches to include in the output Tree. If None, all will be included
    '''
    printdebug("Creating a copy of TTree {0} ({1} events) into {2} with name {3} and selection {4}; branches are {5}".format(itree.GetDirectory().GetName()+"/"+itree.GetName(), itree.GetEntries(), ofilename, otreename+suffix, sel, branches if branches else "all"))

    ofile = ROOT.TFile.Open(ofilename, option)
    if odir:
        if not ofile.Get(odir):
            ofile.mkdir(odir).cd()
        else:
            ofile.Get(odir).cd()
    if sel == "" and branches is None:
        otree = itree.CloneTree(-1, "fast")
    else:
        if branches:
            itree.SetBranchStatus('*', 0)
            for b in branches:
                itree.SetBranchStatus(b, 1)
        if sel != "":
            otree = itree.CopyTree(sel)
        else:
            otree = itree.CloneTree()
    printdebug("Output TTree has {} events".format(otree.GetEntries()))

    if otreename != "":
        otree.SetName(otreename+suffix)
        otree.SetTitle(otreename+suffix)
    printdebug("Writing output TTree")
    otree.Write()
    printdebug("Closing output file")
    ofile.Close()

@input_tree
def get_branch_titles(itree, branchnames, **kwargs):
    '''
    Returns the GetTitle() result from all given branches.
    The input_tree wrapper allows to call this function by giving it an input filename and an input treename.

    :itree:       TTree, input TTree
    :branchnames: array of str or "*", names of all the branches
    '''
    brtitles = []
    listbranches = itree.GetListOfBranches()

    for br in listbranches:
        if branchnames == "*":
            brtitles += [br.GetTitle()]
            continue
        if br.GetName() in branchnames:
            brtitles += [br.GetTitle()]

    return brtitles

@format_sel
@input_tree
def get_entries(itree, sel = "", **kwargs):
    '''
    Return the number of entries in side a TTree
    The input_tree wrapper allows to call this function by giving it an input filename and an input treename.

    :itree: TTree, input Tree
    sel:    str or Selection object, given selection
    '''
    return itree.GetEntries(sel)

def get_rootfile_as_dataframe(ifilename, itreenames, branches, cut = None):
    '''
    Load ROOT file as a Pandas dataframe

    :ifilename:  str, path to the ROOT file
    :itreenames: array of str, names of the TTrees to load
    :branches:   array of str, names of the branches to load from the TTrees
    '''
    import uproot
    import pandas
    
    dfs = []
    for itreename in itreenames:
        _df = uproot.open("{0}:{1}".format(ifilename, itreename))
        dfs.append(_df.arrays(branches, library = "pd", cut = cut))

    return pandas.concat(dfs, ignore_index = True)

@input_tree
def has_branch(itree, branches, **kwargs):
    '''
    Returns True if a given TTree has the given branch or branches.
    The input_tree wrapper allows to call this function by giving it an input filename and an input treename.

    :itree:       TTree, input TTree
    :branchnames: array of str or str, names of all the branches
    '''
    if isinstance(branches, str): branches = [branches]

    for br in branches:
        if not hasattr(itree, br):
            return False

    return True

def has_tree(ifilename, itreename):
    '''
    Returns True if a given TFile has a TTree object with the given name

    :ifilename: str, path to the input file
    :itreename: str, name of the given TTree
    '''
    ifile = ROOT.TFile.Open(ifilename)
    itree = ifile.Get(itreename)

    out = False if not itree else True

    ifile.Close()

    return out

def merge_files(ofilename, ifiles):
    '''
    Merge the given files into a single file

    :ofilename: str, path to the output file
    :ifiles:    array of str, paths of the input files
    '''
    import subprocess
    
    printinfo("Merging {0} files into {1}".format(len(ifiles), ofilename))

    p = subprocess.Popen('hadd -O -ff {} {}'.format(ofilename, ' '.join(ifiles)).split(),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    if p.wait() != 0:
        printerror("Problem detected while merging files")

@input_trees
def merge_trees(itrees, ofilename, otreename, **kwargs):
    '''
    Merge a given set of input TTrees into a ROOT file

    :itrees:    array of TTree objects, input Trees to be merged
    :ofilename: str, path to the output ROOT file
    :otreename: str, name of the output TTree
    '''
    tlist = ROOT.TList()
    for t in itrees:
        printinfo("Adding TTree {} with {} entries".format(t.GetName(), t.GetEntries()))
        tlist.Add(t)

    f = ROOT.TFile.Open(ofilename, "recreate")
    outt = ROOT.TTree.MergeTrees(tlist)
    printinfo("Merged all TTrees, output Tree has {} entries".format(outt.GetEntries()))
    outt.SetName(otreename)
    outt.SetTitle(otreename)
    outt.Write()
    f.Close()

