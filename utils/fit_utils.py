import math

from collections import namedtuple

from scripts import printinfo, printdebug, printerror, printwarning
from utils.rootutils import input_tree
from ROOT    import TFile, TCanvas, kBlue, RooRealVar, RooArgSet, RooDataSet

_FitConditions  = namedtuple("_FitConditions" , ["mass", "range", "weighted", "simultaneous"])
_PlotConditions = namedtuple("_PlotConditions", ["components", "ranges", "nbins", "show", "plot_total", "plot_pulls"])
MassRange = namedtuple("MassRange", ["min", "max"])
MassVar  = namedtuple("MassVar",  ["var", "ranges"])
Variable = namedtuple("Variable", ["var", "lims"])

def mc_ipa_params():
    params = {#"l"    : [0, -3, 3],
              "l": [-1.99],
              #"zeta" : [0.5, 1e-7, 2],
              "zeta" : [1e-5],
              #"beta" : [0, -1, 1],
              "beta" : [0],
              "sigma": [5, 1, 10],
              "a1"   : [2, 0, 10],
              "n1"   : [0.5, 0, 1],
              "a2"   : [2, 0, 20],
              "n2"   : [1, 0, 2]}

    return params

def diego_ipa_params(triggercat):
    params = {"TIS":{"mean" : [498.2212294904529],
                     "l"    : [-1.9853888612326855],
                     "zeta" : [1e-5],
                     "beta" : [0],
                     "sigma": [3.8831361368893678*1.05],
                     "a1"   : [7.173652274765219],
                     "n1"   : [1.0],
                     "a2"   : [20.0],
                     "n2"   : [2.0]},
              
              "xTOS":{"mean" : [498.14605224392426],
                      "l"    : [-1.9655689472828861],
                      "zeta" : [1e-5],
                      "beta" : [0],
                      "sigma": [4.002524265477817*1.05],
                      "a1"   : [3.8031560293654274],
                      "n1"   : [1.0],
                      "a2"   : [20.0],
                      "n2"   : [2.0]}}

    return params[triggercat]

class NumericFunction:
    def __init__(self, data1, data2, name = "", points = 500):
        N = len(data1)
        self.OriginVar = name
        self.database  = {}

        self.binSize = max(1, round(N/points))
        l = [[data1[i], data2[i]] for i in range(N)]
        l.sort()

        for i in range(N):
            p = l[i]
            if not i%self.binSize: self.database[p[0]] = p[1]

        self.references = list(self.database.keys())
        self.references.sort()

        self.refsize = len(self.references)
        self.lowval  = self.references[0]
        self.upval   = self.references[self.refsize - 1]

    def useOver(self, number):
        lowval, upval = self.lowval, self.upval
        if number <= lowval: return self.database[lowval]
        if number >= upval : return self.database[upval]

        for i in range(self.refsize):
            x1 = self.references[i]
            if number == x1: return self.database[x1]
            if x1 > number:
                x0 = self.references[i-1]
                y0 = self.database[x0]
                y1 = self.database[x1]
                m = (y1 - y0)/(x1 - x0)
                y = y0 + m*(number - x0)

                return y

    def __call__(self, entry):
        return self.useOver(entry)

    def derivative(self, number):
        raise NotImplementedError("Derivative function not written")

    def derivativeF(self):
        raise NotImplementedError("DerivativeF function not written")

    def Draw(self, color):
        raise NotImplementedError("Draw function not written")

def candidates_in_region(pdf, data, mass, range, norm_range):
    '''
    Calculate the expected candidates in the signal region for the given fit range.

    :pdf:
    :data:
    :mass:
    :range:
    :norm_range:
    '''
    from ROOT import RooFit
    nset = RooArgSet(mass.var)

    cutarray = []
    for ir in norm_range.split(","):
        l,r = mass.ranges[ir]
        cutarray += ["(({0} >= {1}) && ({0} < {2}))".format(mass.var.GetName(), l, r)]

    cutstring = "||".join(cutarray)
    nentries = data.sumEntries(cutstring)

    fsb_int = pdf.createIntegral(nset, RooFit.NormSet(nset), RooFit.Range(norm_range))
    fsb = fsb_int.getVal()

    fbl_int = pdf.createIntegral(nset, RooFit.NormSet(nset), RooFit.Range(range))
    fbl = fbl_int.getVal()

    return fbl*nentries/fsb

@input_tree
def create_dataset(itree, variables = None, cuts = None, name = "data", nentries = -1, **kwargs):
    '''
    Creates a RooDataSet object given a list of variables and cuts

    :ifilename:  str, name of the file containing the TTree
    :itreename:  str, name of the TTree
    :variables: array, instances of RooRealVar which correspond to the variables that are to be included in the dataset
    :cuts:      str, cuts to be applied to the dataset
    :name:      str, name of the dataset
    '''
    if cuts is None and nentries<0:
        return dataset_from_tree(itree     = itree,
                                 variables = variables,
                                 name      = name)
    else:
        import tempfile, os

        ifile = TFile.Open(ifilename)
        itree = ifile.Get(itreename)
        
        printinfo("Created TTree object with {} entries created".format(itree.GetEntries()))

        with tempfile.TemporaryDirectory() as tmpdir:
            ofilename = os.path.join(tmpdir, "temp_file.root")
            
            ofile = TFile.Open(ofilename, "new")
            if cuts is not None:
                printinfo("Copying TTree object and applying given cuts")
                dummytree = itree.CopyTree(cuts)
                printinfo("Cloning TTree to select the given number of entries")
                otree     = dummytree.CloneTree(nentries)
            else:
                printinfo("No cuts given. Cloning TTree to select the given number of entries")
                otree     = itree.CloneTree(nentries)

            otree.AutoSave()
        
            otreename = otree.GetName()

            ofile.Close()
            ifile.Close()
            
            printinfo("Creating dataset")

            return dataset_from_tree(ofilename, otreename, variables, name)

def create_plots(data, model, ofile, fit_conds, plot_conds = None, res = None, name = "canvas"):
    '''                                                                                                                                                       
    '''
    from ROOT import RooFit
    
    plot_conds = plot_conds or PlotConditions()

    plotables = []

    frame = fit_conds.mass.var.frame(
        RooFit.Name(fit_conds.mass.var.GetName()),
        RooFit.Title(fit_conds.mass.var.GetName()),
        RooFit.Range(fit_conds.range),
        )
    plotables.append(frame)

    opts = (RooFit.Binning(plot_conds.nbins), RooFit.Name("data"))

    data.plotOn(frame, *opts)

    if plot_conds.components:
        for c, cmds in plot_conds.components.items():
            model.pdf.plotOn(frame, RooFit.Components(c), RooFit.Range(fit_conds.range), *cmds)
    if plot_conds.ranges:
        for cmds in list(plot_conds.ranges.values()):
            model.pdf.plotOn(frame, *cmds)
    if plot_conds.plot_total:
        model.pdf.plotOn(frame, RooFit.LineColor(kBlue), RooFit.Range(fit_conds.range))

    if plot_conds.show:
        canvas = TCanvas(name, name, 1920, 1080)

        plotables.append(canvas)

        frame.Draw()
        canvas.Update()
        if ofile != "" and ofile is not None:
            canvas.SaveAs(ofile)

        input("Waiting for Approval. Press Enter")

    return plotables

def create_var(name, *args, **kwargs):
    '''
    Create an instance of RooRealVar for a given name and a set of args.

    :name: str, name of the variable
    '''
    return RooRealVar(name, name, *args, **kwargs)

def create_yield_param(nentries, name):
    '''
    Create the RooRealVar for a yield

    :nentries: int, total entries we are fitting
    :name:     str, name of the variable
    '''
    return create_var(name, nentries*1./2, 0, nentries)

@input_tree
def dataset_from_tree(itree, variables, name, **kwargs):
    '''
    Create a RooDataSet object from a given tree

    :filename:  str, name of the file where the TTree is stored
    :treename:  str, name of the tree
    :variables: array of RooRealVar objects, containing the variables that will be included in the dataset
    :name:      str, name of the dataset
    '''
    listvars = RooArgSet()
    for var in list(variables):
        printdebug("Adding var {}".format(var.GetName()))
        listvars.add(var)
    data = RooDataSet(name, name, itree, listvars)

    printdebug("Dataset from TTree {} created with {} events".format(itree.GetName(), data.sumEntries()))

    return data

def gaussian_constraint(par, val, error):
    '''
    Create a Gaussian constraint for a given parameter, with a given value and error
    '''
    from ROOT import RooGaussian, RooFit
    from utils.fit_utils import create_var

    name    = par.GetName()+"_const"

    return RooGaussian(name, name, par, RooFit.RooConst(val), RooFit.RooConst(error))

def generate_profiles(cat, show, BR, summaryConstraints):
    '''
    Calculate the profile from the given category

    :cat: Category, given category
    '''
    from ROOT import RooFit, TCanvas, RooArgSet
    
    #nll   = cat.model.pdf.createNLL(cat.data, RooFit.NumCPU(6), RooFit.ExternalConstraints(RooArgSet(summaryConstraints)))
    nll   = cat.model.pdf.createNLL(cat.data, RooFit.ExternalConstraints(RooArgSet(summaryConstraints)))
    printinfo("Creating profile")
    pl = nll.createProfile(RooArgSet(BR))
    printdebug("BR = {}".format(BR))
    printdebug("pl = {}".format(pl))
    plotables = []
    if show:
        f = BR.frame(RooFit.Name("profile_BR"),
                     RooFit.Title("Profile of BR"))
        pl.plotOn(f, RooFit.ShiftToZero())
        f.SetMinimum(0)
        fcanvas = TCanvas("Profile", "Profile", 1920, 1080)
        f.Draw()
        fcanvas.Update()
        input("Press ENTER to continue.")

        plotables += [f]

    _x, _y = [],[]
    for i in range(3000):
        br = i/10
        BR.setVal(br)
        _x.append(br)
        _y.append(math.exp(-pl.getVal()))

    return plotables, _x, _y

def get_mass(par):
    '''
    Create an instance of MassVar for a given particle

    :par: str, name of the particle
    '''
    from utils.ana_utils import __mass_lims__
    
    lims   = __mass_lims__[par]
    ranges = mass_ranges(par)

    mass = RooRealVar(par+"_M", "M_{#"+par+"}", *lims, "MeV/c^{2}")
    for name, values in ranges.items():
        mass.setRange(name, *values)

    return MassVar(mass, ranges)

def mass_ranges(par):
    '''
    Get the mass ranges for a given particle.

    :par: str, name of the given particle
    '''
    from utils.ana_utils import __blind_region__, __mass_lims__

    if par == "K0S":
        return {"FullRange"    : MassRange(*__mass_lims__["K0S"]),
                "LeftSideband" : MassRange(__mass_lims__["K0S"][0], __blind_region__[0]),
                "Signal"       : MassRange(*__blind_region__),
                "RightSideband": MassRange(__blind_region__[1], __mass_lims__["K0S"][1])
                }
    
    printerror("Mass ranges for particle '{}' are not implemented".format(par))

def mass_values(par):
    '''
    Read the mass value for a particle from the PDG.

    :par: str, name of the particle
    '''
    from scripts.physics import read_pdg
    from utils.ana_utils import __mc_numbers__

    pdg = read_pdg()
    
    return pdg.GetParticle(__mc_numbers__[par]).Mass()*1000

def setup_fit_constraints(alphas, unblind):
    '''
    Set up a Gaussian constraint for the fit

    :alphas:  dict, values of the alpha parameter for TIS, xTOS, and global
    :unblind: bool, True if this is unblinded data
    '''
    from ROOT            import RooArgList, RooFormulaVar, RooArgSet
    from utils.ana_utils import trigger_categories

    sigma_range = 3

    alpha_vars, alpha_const = {},{}
    for key in alphas:
        alpha_vars[key]  = create_var(key, alphas[key][0],
                                      alphas[key][0]-sigma_range*alphas[key][1],
                                      alphas[key][0]+sigma_range*alphas[key][1])
        alpha_const[key] = gaussian_constraint(alpha_vars[key], *alphas[key])
        
    BR = create_var("BR", 0, 100)
    if not unblind:
        BR.setVal(0)
    BR.setConstant(not unblind)

    nsig = {}
    for triggercat in trigger_categories:
        nsig[triggercat] = RooFormulaVar("nsig", "nsig", "BR/(alpha*alpha{})".format(triggercat),
                                         RooArgList(BR, alpha_vars["alpha"], alpha_vars["alpha"+triggercat]))

    summaryConstraints = RooArgSet(*list(alpha_const.values()))

    return nsig, summaryConstraints, BR, (alpha_vars, alpha_const)

class FitConditions(_FitConditions):
    def __new__(cls, mass, range, weighted = False, simultaneous = False):
        return super(FitConditions, cls).__new__(cls, mass, range, weighted, simultaneous)

class PlotConditions(_PlotConditions):
    def __new__(cls, components = None, ranges = None, nbins = 75, show = False, plot_total = True, plot_pulls = False):
        components = components or {}
        return super(PlotConditions, cls).__new__(cls, components, ranges, nbins, show, plot_total, plot_pulls)

class FitResult(object):
    def __init__(self):
        '''
        Dummy initialization
        '''
        super(FitResult, self).__init__()

class MultiFitResult(FitResult):
    def __init__(self, ress):
        self._ress = ress
        super(MultiFitResult, self).__init__()

    def plot_and_write(self, data, model, ofilename, fit_conds, plot_conds, name = "canvas"):
        '''
        Internal function to create and write the output plots on the given ROOT file.        
        A directory is created for each fit
        '''
        plotables = []
        for res in zip(self._ress):
            plotables += create_plots(data, model, ofilename, fit_conds, plot_conds, res, name)

        return plotables

class SimFitResult(FitResult):
    def __init__( self, cats, main_cat, res ):
        '''
        Represent a result for a simultaneous fit.
        '''
        self._cats = cats
        self._res  = res
        self.main_cat = main_cat
        super(SimFitResult, self).__init__()

    def plot_and_write( self, ofilename, fit_conds, plot_conds, BR, summaryConstraints):
        '''
        Internal function to create an write the ouput plots on the given
        Root file.
        A directory is created for each category, but the RooFitResult is
        saved together in the top directory.
        '''
        plotables = []
        if plot_conds.show:
            plotables = create_sim_plots(self._cats, ofilename, fit_conds, plot_conds)

        _, x, y = generate_profiles(self.main_cat, plot_conds.show, BR, summaryConstraints)
            
        return plotables, x, y
        
def create_sim_plots(categories, ofilename, fit_conds, plot_conds = None):
    from ROOT import RooFit, kDotted, kTRUE, kBlue
    
    plot_conds = plot_conds or PlotConditions()

    plotables, frame, pulls_frame, hpulls1, hpulls2 = [], {}, {}, {}, {}
    for k in range(len(categories)):
        key = categories[k].name()
        n = categories[k].format_name("Mass_"+fit_conds.mass.var.GetName())
        frame[key] = fit_conds.mass.var.frame(RooFit.Name(n), RooFit.Title(n))
        plotables.append(frame[key])

        opts = (RooFit.Binning(plot_conds.nbins), RooFit.Name(key+"data"))
        categories[k].data.plotOn(frame[key], *opts)

        if plot_conds.components:
            for c, cmds in plot_conds.components.items():
                n = categories[k].format_name(c)
                printinfo(n)
                categories[k].model.pdf.plotOn(frame[key], RooFit.Components(n), RooFit.Range(fit_conds.range), *cmds)

        if plot_conds.ranges:
            for cmds in list(plot_conds.ranges.values()):
                categories[k].model.pdf.plotOn(frame[key], *cmds)
        if plot_conds.plot_total:
            categories[k].model.pdf.plotOn(frame[key], RooFit.LineColor(kBlue), RooFit.Range(fit_conds.range), RooFit.NormRange("FullRange"))
        if plot_conds.plot_pulls:
            n = categories[k].format_name("Pulls_"+fit_conds.mass.var.GetName())
            printinfo(n)
            pulls_frame[key] = fit_conds.mass.var.frame(RooFit.Name(n), RooFit.Title(n))

            plotables.append(pulls_frame[key])

            hpulls1[key] = frame[key].getObject(0).makePullHist(frame[key].getObject(1), kTRUE)
            hpulls2[key] = frame[key].getObject(0).makePullHist(frame[key].getObject(2), kTRUE)

            pulls_frame[key].addPlotable(hpulls1[key], "P")
            pulls_frame[key].addPlotable(hpulls2[key], "P")

    if plot_conds.show:
        canvas = TCanvas("canvas", "canvas", 1920, 1080)
        canvas.Divide(len(categories), 1+int(plot_conds.plot_pulls))
        plotables.append(canvas)

        for k in range(len(categories)):
            key = categories[k].name()
            canvas.cd(k+1).SetPad(0.5*k, 0.2*int(plot_conds.plot_pulls), 0.5 + 0.5*k, 1) 
            frame[key].Draw("same")
            if plot_conds.plot_pulls:
                canvas.cd(k+3).SetPad(0.5*k, 0, 0.5 + 0.5*k, 0.2 )
                pulls_frame[key].Draw()

        canvas.Update()

        if ofilename != "" and ofilename is not None:
            canvas.SaveAs(ofilename)

        input("Waiting for Approval. Press ENTER")

    return plotables
