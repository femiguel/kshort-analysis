'''
Useful methods for this particular analysis
'''
import os
import functools

from scripts            import printinfo, printerror, printwarning, printdebug
from scripts.files_info import check_params

years = ["2016", "2017", "2018"]
trigger_categories = ["TIS", "xTOS"]

sMB = {"2016": 5.66509e-7,
       "2017": 5.30689e-7,
       "2018": 4.43192e-7}

lumis = {"2016": 1.63,
         "2017": 1.47,
         "2018": 2.02}

BR = {"K0S2pi2": (0.6920, 0.0005)}

__mc_numbers__ = {"K0S":  310,
                  "pi+":  211,
                  "pi-": -211,
                  "pi0":  111,
                  "mu+":  -13,
                  "mu-":   13,
                  "e+" :  -11,
                  "e-" :   11,
                  }

__blind_region__ = (490, 510)
__mass_lims__    = {"K0S": (450, 600)}

def get_ana_filenames(key):
    from scripts import get_project_path
    
    _join = lambda fname: os.path.join(get_project_path(), fname)
    info = {"PID"                : _join("pid/PID_Calibration.json"),
            "SignalFitParameters": _join("scripts/Ipatia_Fit_Parameters.json")}

    return info[key]

def get_names(func):
    '''
    Decorate a function that inputs the names of the mother and daughters for a given set of data type and channel.
    '''
    @functools.wraps(func)
    def wrapper(data_type, channel, **kwargs):
        '''
        Internal wrapper
        '''
        mother    = get_mother_name(data_type = data_type, channel = channel)
        daughters = get_daughters_names(data_type = data_type, channel = channel)

        return func(mother = mother, daughters = daughters, **kwargs)

    return wrapper

def branches_event():
    '''
    Event-related branches used in the analysis
    '''
    return ["eventNumber", "runNumber", "Polarity", "nSPDHits"]

def branches_kinematic():
    '''
    Branches that reflect the kinematic properties of the particles.
    '''
    branches_mother    = ["P", "PT"]
    branches_daughters = ["P", "PT", "ETA"]

    return branches_mother, branches_daughters

def branches_mcmatch():
    '''
    Branches used for the MC-matching in this analysis
    '''
    branches_mother    = ["TRUEID"]
    branches_daughters = ["TRUEID", "MC_MOTHER_ID", "MC_MOTHER_KEY"]

    return branches_mother, branches_daughters

def branches_mva():
    '''
    Branches needed by the MVA training in the analysis.
    In particular, they are needed for the VeloMatterVeto, minIP and FDT variables.
    '''
    branches_mother    = ["ENDVERTEX_X", "ENDVERTEX_Y", "ENDVERTEX_Z", "ENDVERTEX_XERR", "ENDVERTEX_YERR", "ENDVERTEX_ZERR"]
    branches_daughters = ["IP_OWNPV", "PX", "PY", "PZ"]

    return branches_mother, branches_daughters

def branches_pid(channel):
    '''
    Branches used for the PID efficiency computations.
    '''
    branches_mother    = []
    branches_daughters = ["InMuonAcc", "isMuon", "PIDmu"] if channel == "K0S2mu4" else []

    return branches_mother, branches_daughters

def branches_strip(channel):
    '''
    Branches used for the stripping selections.
    '''
    branches_mother    = ["M", "ENDVERTEX_Z", "OWNPV_Z", "DIRA_OWNPV", "TAU", "IP_OWNPV", "IPCHI2_OWNPV", "ENDVERTEX_CHI2", "ENDVERTEX_NDOF", "DOCA"] if channel == "K0S2mu4" else ["M", "DIRA_OWNPV", "FD_OWNPV", "P", "IP_OWNPV", "DOCA", "ADMASS"]
    branches_daughters = ["TRACK_CHI2NDOF", "TRACK_GhostProb"] if channel == "K0S2mu4" else ["IPCHI2_OWNPV", "PT"]

    return branches_mother, branches_daughters

def branches_track():
    branches_mother = []
    branches_daughters = ["ProbNNghost", "TRACK_GhostProb", "TRACK_CHI2NDOF"]

    return branches_mother, branches_daughters

def branches_trigger():
    '''
    Branches used for the trigger selection.
    '''
    branches_mother = []
    for triggercat in ["TIS", "TOS"]:
        branches_mother += [l+"Decision_"+triggercat for l in l0lines(triggercat)]
    branches_mother += [l+"Decision_TOS" for l in hlt1lines() + hlt2lines()]

    return branches_mother, []

@check_params
def get_branches(data_type, year, channel):
    '''
    Returns all the branches used in the analysis, so that the n-tuples do not use as much disk space
    The wrapper check_params() checks that the three input parameters are correct
    
    :data_type: str, can be "MC" or "Data"
    :year:      str or int, can be 2016, 2017 or 2018
    :channel:   str, can be "K0S2mu4" or "K0S2pi2"
    '''
    mcmother     , mcdaughters      = branches_mcmatch()
    stripmother  , stripdaughters   = branches_strip(channel)
    pidmother    , piddaughters     = branches_pid(channel)
    mvamother    , mvadaughters     = branches_mva()
    triggermother, triggerdaughters = branches_trigger()
    trackmother  , trackdaughters   = branches_track()
    kinemother   , kinedaughters    = branches_kinematic()

    branches_mother    = stripmother + pidmother + mvamother + triggermother + trackmother + kinemother
    branches_daughters = stripdaughters + piddaughters + mvadaughters + triggerdaughters + trackdaughters + kinedaughters

    if data_type == "MC":
        branches_mother    += mcmother
        branches_daughters += mcdaughters
    
    branches = setup_branches(data_type = data_type,
                                        channel   = channel,
                                        branches_mother    = branches_mother,
                                        branches_daughters = branches_daughters)

    branches += branches_event()

    return branches

def get_daughters_names(channel, data_type):
    '''
    Get the name of the daughter particles in the n-tuples
    '''
    pi_name = "d" if data_type == "Data" else "pi"

    return ["mu{}".format(k) for k in range(1,5)] if channel == "K0S2mu4" else [pi_name+str(k) for k in range(1,3)]

def get_mother_name(data_type, channel):
    '''
    Get the name of the daughter particles in the n-tuples
    '''
    return "K0S" if data_type == "MC" or (data_type == "Data" and channel == "K0S2mu4") else "mother"

def hlt1lines():
    '''
    Trigger lines at the HLT1 level used in this analsis
    '''
    return ["Hlt1DiMuonNoL0", "Hlt1DiMuonLowMass"]

def hlt2lines():
    '''
    Trigger lines at the HLT2 level used in this analysis
    '''
    return ["Hlt2DiMuonSoft"]
    
def l0lines(tis):
    '''
    Trigger lines at the level 0 used in this analysis

    :tis: str, trigger category
    '''
    lines = {"TIS": ['L0Electron', 'L0Photon', 'L0Hadron', 'L0Muon', 'L0DiMuon'],
             "TOS": ['L0Muon', 'L0DiMuon', 'L0Muon,lowMult']}
    try:
        return lines[tis]
    except KeyError:
        printerror("'tis' parameter can be 'TIS' or 'TOS'")

@get_names
def setup_branches(mother, daughters, branches_mother, branches_daughters, **kwargs):
    '''
    For a given set of branchnames for mother and daughters, add the corresponding names of the particles,
    and insert all names into a singular array with no duplicates

    :mother:             str, name of the mother
    :daughters:          array of str, names of the daughters,
    :branches_mother:    array of str, names of the branches for the mother
    :branches_daughters: array of str, names of the branches for the daughters
    '''
    branches = []
    for br in branches_mother:
        branches += [mother+"_"+br]
    for br in branches_daughters:
        for d in daughters:
            branches += [d+"_"+br]

    branches = list(set(branches))

    return branches
