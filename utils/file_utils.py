'''
Useful methods to use when handling files in this analysis.
'''
import os

from scripts.files_info import check_params
from scripts            import get_tuples_path, printinfo, printerror, printwarning, printdebug

@check_params
def get_ifilename(data_type, year, channel):
    '''
    Get the basic input ROOT file names, without any selecitons or corrections.

    :data_type: str, ("MC", "Data")
    :year:      str or int, (2016, 2017, 2018)
    :channel:   str, ("K0S2mu4", "K0S2pi2")
    '''
    from utils.pyutils import make_dirs

    odir = os.path.join(get_tuples_path(), "{data_type}/{channel}".format(**locals()))
    odir = make_dirs(odir)

    return os.path.join(odir, "{data_type}_{channel}_{year}.root".format(**locals()))

def get_ofilename(corr, ifilename):
    '''
    Get the name of the output file when applying a given correction

    :corr:      str, name of the correction
    :ifilename: str, path to the input file that is being corrected
    '''
    ofilename = lambda ext: ifilename.replace(".root", ext+".root")
    if corr == "AcceptReject":
        return ofilename("_PTMCfilter")
    if corr == "HorizontalCorrection":
        return ofilename("_Corr")
    if corr == "addArmenteros":
        return ofilename("_wAP")
    if corr == "cutArmenteros":
        return ofilename("_sel")
    if "Trigger" in corr:
        return ofilename("_WithTrigger")
    if corr == "TrackCuts":
        return ofilename("_WithTrackCuts")
    if corr == "Blind":
        return ofilename("_BLIND")
    if corr == "Classifier":
        return ofilename("_WithClassifier")
    if corr == "Fit":
        return ofilename("_ForFit")
    if corr == "BDTCut":
        return ofilename("_WithBDTCut")

    raise NotImplementedError("Output file name for correction {} has not been implemented yet.".format(corr))

@check_params
def setup_tuples(data_type, year, channel):
    '''
    Copy the ROOT file for the given data type, year and channel from the original place
    to the appropriate destination (specified by the get_tuples_path() method) and add
    the VeloMatterVeto variable to all of its TTrees. If the input data is MC,
    matching selection is also applied.

    :data_type: str, ("MC", "Data")
    :year:      str or int, (2016, 2017, 2018)
    :channel:   str, ("K0S2mu4", "K0S2pi2")
    '''
    from scripts.files_info import get_filename, info
    from utils.pyutils      import make_dirs, get_filename_from_path, file_exists, get_path_from_filename
    from utils.rootutils    import add_variables_into_tree, copytree
    from utils.ana_utils    import get_branches, get_mother_name
    from scripts.selections import cuts
    
    ifilepath, itreenames = get_filename(data_type, year, channel)

    ofilename = get_ifilename(data_type, year, channel)

    branches = get_branches(data_type, year, channel)
    mother   = get_mother_name(data_type, channel)
    
    if not file_exists(ofilename):
        for itreename in itreenames:
            copytree(ofilename = ofilename,
                     ifilename = ifilepath,
                     itreename = itreename,
                     otreename = get_filename_from_path(itreename),
                     option    = "update",
                     odir      = get_path_from_filename(itreename),
                     sel       = cuts["mctruth{}".format("sig" if channel=="K0S2mu4" else "norm")] if data_type == "MC" else "",
                     branches  = branches)

            variables_to_add = []
            ptname = mother+"_{}PT".format("TRUE"*("MCDecayTree" in itreename))
            if data_type == "MC":
                variables_to_add += ["WeightValues"]
            if itreename == info["Trees"][channel][data_type][year][channel+"_NoPTCuts"*(channel=="K0S2pi2")]:
                variables_to_add += ["VeloMatterVeto"]
                
            if variables_to_add != []:
                add_variables_into_tree(ifilename = ofilename,
                                        itreename = itreename,
                                        variables = variables_to_add,
                                        mother    = mother,
                                        ptname    = ptname)
    else:
        printdebug("ROOT file {} already existed. Please check if it should be recreated.".format(ofilename))

    return ofilename, itreenames
