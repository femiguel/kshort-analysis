'''
Functions and classes that set up the tools needed for the selections used in this analysis.
'''
import functools

class Selection(object):
    __replacements__ = [("&&", " and "),
                        ("&" , " and "),
                        ("||", " or "),
                        ("|" , " or "),
                        ("!" , " not ")]
    def __init__(self, cut):
        '''
        :cut: str, selection cut
        '''
        self._cut = cut

    def __add__(self, other):
        return Selection("({0})&&({1})".format(self, other))

    def __radd__(self, other):
        return self+other

    def __neg__(self):
        return Selection("!({0})".format(self))

    def __or__(self, other):
        return Selection("({0})||({1})".format(self, other))

    def __sub__(self, other):
        return self + -other

    def __str__(self):
        return self._cut

    @property
    def cut(self):
        return str(self)

    @property
    def pyexpr(self):
        '''
        Return the python version of the expression (especially used for pandas)
        '''
        e = str(self)
        for c,p in self.__replacements__:
            e = e.replace(c,p)

        return e

def additional_daughters_cuts(daughters, cut):
    '''
    Create a selection for the daughters

    :daughters: array of str, names of the daughters
    :cut:       str, cut to apply
    '''
    return Selection("&&".join(["({0}_{1})".format(d,cut) for d in daughters]))

def define_triggersel(triggerlines, mother, tis, additional_req = None):
    '''
    Create trigger selection from input array of trigger lines

    :triggerlines:   array, requested trigger lines
    :tis:            str, ("TIS", "TOS")
    :additional_req: str, custom selection that may be added
    '''
    sel = Selection("||".join(map(lambda s: "({0}_{1}Decision_{2})".format(mother, s, tis), triggerlines)))
    if additional_req:
        if not isinstance(additional_req, Selection):
            additional_req = Selection(additional_req)
        sel += additional_req
    
    return sel

def l0sel(tis, mother = "K0S"):
    '''
    Set up the L0 trigger selection for a given trigger category and a mother.

    :tis:    str, trigger category, can be either "TIS" or "TOS"
    :mother: str, name of the mother
    '''
    from utils.ana_utils import l0lines
    
    if tis == "TOS":
        sel_not_tis = -l0sel("TIS", mother = mother)

    return define_triggersel(triggerlines = l0lines(tis),
                             mother       = mother,
                             tis          = tis,
                             additional_req = None if tis == "TIS" else sel_not_tis)

def hlt1sel(mother = "K0S"):
    '''
    Set up the HLT1 trigger selection.

    :mother: str, name of the mother
    '''
    from utils.ana_utils import hlt1lines
    
    return define_triggersel(triggerlines = hlt1lines(),
                             mother       = mother,
                             tis          = "TOS",
                             additional_req = None)
def hlt2sel(mother = "K0S"):
    '''
    Set up the HLT2 trigger selection.

    :mother: str, name of the mother
    '''
    from utils.ana_utils import hlt2lines
    
    return define_triggersel(triggerlines = hlt2lines(),
                             mother       = mother,
                             tis          = "TOS",
                             additional_req = None)

def mother_match(mother):
    '''
    Set up the MC-matching TRUEID selection for the mother.

    :mother: str, name of the mother
    '''
    return Selection("abs({0}_TRUEID)=={1}".format(mother, trueids(mother)))

def daughters_match(daughters):
    '''
    Set up the MC-matching TRUEID selection for the daughters.

    :daughters: array of str, names of the daughters
    '''
    import re
    
    dname = lambda d: re.sub("[0-9]", "", d)

    return Selection("&&".join(["abs({0}_TRUEID)=={1}".format(d,trueids(dname(d))) for d in daughters]))

def mother_id_match(mother, daughters):
    '''
    Set up the MC-matching selection that ensures all daughters share the same MOTHER ID.

    :mother:    str, name of the mother
    :daughters: array of str, names of the daughters
    '''
    import re

    dname = lambda d: re.sub("[0-9]", "", d)

    return Selection("&&".join(["{0}_MC_MOTHER_ID=={1}".format(d, trueids(mother)) for d in daughters]))

def mother_key_match(mother, daughters):
    '''
    Set up the MC-matching selection that ensures all daughters share the same MOTHER KEY.

    :mother:    str, name of the mother
    :daughters: array of str, names of the daughters    
    '''
    import re
    
    dname = lambda d: re.sub("[0-9]", "", d)

    return Selection("&&".join(["{0}_MC_MOTHER_KEY=={1}_MC_MOTHER_KEY".format(daughters[k], daughters[k+1]) for k in range(len(daughters)-1)]))

def mcMatch(mother = "K0S", daughters = ["mu1", "mu2", "mu3", "mu4"]):
    '''
    Set up the full MC-matching selection.

    :mother:    str, name of the mother
    :daughters: array of str, names of the daughters
    '''
    return mother_match(mother)+\
           daughters_match(daughters)+\
           mother_id_match(mother, daughters)+\
           mother_key_match(mother, daughters)

def norm_cuts(data_type):
    '''
    Additional cuts for the normalization channel.

    :data_type: str, ("MC", "Data")
    '''
    from utils.ana_utils import get_mother_name

    mother = get_mother_name(data_type, channel = "K0S2pi2")
    radius = Selection("sqrt({mother}_ENDVERTEX_X^2+{mother}_ENDVERTEX_Y^2)>5".format(mother=mother))
    VMV    = Selection("VeloMatterVeto>1")

    return radius + VMV

def stripping(line, return_cuts = False):
    '''
    Set up the stripping selection for a given stripping line.

    :line:        str, name of the stripping line
    :return_cuts: bool, whether or not return the array with the cuts
    '''
    if line == "K0s2MuMuMuMuLine":
        strip_cuts = ["(K0S_M                                    < 600.0)",
                      "(abs(K0S_ENDVERTEX_Z-K0S_OWNPV_Z)         > 0)",
                      "(K0S_DIRA_OWNPV                           > 0.9999)",
                      "(K0S_ENDVERTEX_Z                          < 650.0)",
                      "(K0S_TAU                                  > 0.00459280157946)",
                      "(K0S_IP_OWNPV                             < 1.0)",
                      "(K0S_IPCHI2_OWNPV                         < 100)",
                      "(K0S_ENDVERTEX_CHI2*1./K0S_ENDVERTEX_NDOF < 50)",
                      "(K0S_DOCA                                 < 2.0)"]
        strip_cuts += ["(mu{}_TRACK_CHI2NDOF  < 3)".format(k) for k in range(1,5)]
        strip_cuts += ["(mu{}_TRACK_GhostProb < 0.4)".format(k) for k in range(1,5)]

    elif line == "Ks2PiPiForRnSLine" or line == "Ks2PiPiForRnSNoPtCutsLine":
        strip_cuts = ["(K0S_M                    > 400.0)",
                      "(K0S_M                    < 600.0)",
                      "(K0S_DIRA_OWNPV           > 0)",
                      "(K0S_FD_OWNPV*K0S_M/K0S_P > 1.610411922)",
                      "(K0S_IP_OWNPV             < 0.4)",
                      "(K0S_DOCA                 < 0.1)",
                      "(K0S_ADMASS               < 100.0)",]
        strip_cuts += ["(pi{}_IPCHI2_OWNPV > 100)".format(k) for k in range(1,3)]
        if line == "Ks2PiPiForRnSLine":
            strip_cuts += ["(pi{}_PT           > 250)".format(k) for k in range(1,3)] ## cut from StdLoosePions 
    else:
        printerror("Stripping line '{}' has not been included in the selections code yet.".format(line))

    import re

    out = Selection("&&".join([re.sub(r"\s+","",c) for c in strip_cuts]))

    if return_cuts:
        out = (out,strip_cuts)
    
    return out
    

def track_cuts(daughters, isMC):
    '''
    Set up the track-related cuts.

    :daughters: array of str, names of the daughters.
    :isMC:      bool, True if the input data type is MC
    '''
    cuts = ["TRACK_CHI2NDOF{}<2.5",
            "ProbNNghost{}<0.7",
            "TRACK_GhostProb{}<0.25"]

    return Selection("&&".join(["{0}_{1}".format(d, cuts[0].format("_Corr"*isMC)) for d in daughters]))+\
           Selection("&&".join(["{0}_{1}".format(d, cuts[1].format("_Corr"*isMC)) for d in daughters]))+\
           Selection("&&".join(["{0}_{1}".format(d, cuts[2].format("_Corr"*isMC)) for d in daughters]))

def triggersel(tis):
    '''
    Set up the entire trigger selection for a given trigger category.

    :tis: str, trigger category
    '''
    return l0sel(tis)+hlt1sel()+hlt2sel()

def trueids(particle):
    '''
    Source: http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf

    :particle: str, name of the particle to get the ID of
    '''
    trueid = {"K0S": 310,
              "mu" : 13,
              "pi" : 211,
              "e"  : 11,
              }
    return trueid[particle]

def format_sel(func):
    '''
    Decorator for a function that has the "sel" parameter to parse it the actual string from the Selection object
    '''
    @functools.wraps(func)
    def wrapper(**kwargs):
        '''
        Internal wrapper
        '''
        if "sel" in kwargs:
            sel = kwargs["sel"]
            if isinstance(sel, Selection):
                sel = str(sel)

            kwargs["sel"] = sel

        return func(**kwargs)

    return wrapper
