import os, argparse

#os.environ["PYTHONPATH"] = os.path.join(os.environ["PYTHONPATH"], os.path.join(os.getcwd(), "scripts"))
if os.getcwd() not in os.environ["PYTHONPATH"]:
    os.environ["PYTHONPATH"] = os.path.join(os.environ["PYTHONPATH"], os.getcwd())

def main():
    parser     = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help = "Options available")
    
    parser_runTMVA = subparsers.add_parser("runTMVA", help = "Run TMVA and train different classifiers to compute a ROC Curve")
    parser_runTMVA.set_defaults(option = "runTMVA")
    from scripts.runTMVA import add_arguments
    add_arguments(parser_runTMVA)

    parser_fitSidebands = subparsers.add_parser("fitSidebands", help = "Fit the sidebands to a given channel")
    parser_fitSidebands.set_defaults(option = "fitSidebands")
    parser_fitSidebands.add_argument("--channel", type = str, default = "K0S2mu4", choices = ["K0S2mu4", "K0S2e4", "K0S2e2mu2", "K0S2emu3"], help = "Channel to fit")
    parser_fitSidebands.add_argument("--year", type = str, default = "2018", choices = ["2016", "2017", "2018"], help = "Year of the data that is to be fit")
    parser_fitSidebands.add_argument("--trigger", type = str, default = "TIS", choices = ["TIS", "xTOS"], help = "Trigger category to use")
    parser_fitSidebands.add_argument("--return_yield", action = "store_true", default = False, help = "Whether or not the function should return the yield in the signal region")
    parser_fitSidebands.add_argument("--showPlot", action = "store_true", default = False, help = "Whether or not to show the Canvas when the fit is done")
    parser_fitSidebands.add_argument("--saveCanvas", action = "store_true", default = False, help = "Whether or not to save the Canvas when the fit is done")
    parser_fitSidebands.add_argument("--ofilename", default = None, help = "Name of the PNG file that will store the canvas. If saveCanvas is True and this is not provided, the name will be fitSidebands_CHANNEL_YEAR.png")
    parser_fitSidebands.add_argument("--cuts", action = "store_true", default = False, help = "Implement additional cuts on the data, specified on cuts['sidebands']")


    parser_fitNorm = subparsers.add_parser("fitNorm", help = "Fit the normalization channel")
    parser_fitNorm.set_defaults(option = "fitNorm")
    parser_fitNorm.add_argument("--year", default = "2018", choices = ["2016", "2017", "2018"], help = "Year to be fit")
    parser_fitNorm.add_argument("--fix_params_from_MC", action = "store_true", default = True, help = "True if we want to fix parameters from MC fits")

    args = parser.parse_args()
    
    if args.option == "runTMVA":
        from scripts.runTMVA import callTMVA
        callTMVA(args)
        '''
        from scripts.files   import __files__, __treenames__
        from scripts.runTMVA import mainTMVA
        from scripts.selections import cuts
        key = "{}_{}".format(args.channel, args.year)
        additional_sel = cuts["addselMC"] if args.correctedMC else None
        mainTMVA(ifiles           = {name: __files__["{}_{}{}".format(data_type, key, "_Corr"*args.correctedMC*(data_type == "MC"))]
                                     for name, data_type in [("signal", "MC"), ("bkg", "Data")]},
                 itrees           = {name: __treenames__["{}_{}".format(args.channel, args.triggercat)]
                                     for key in ["signal", "bkg"]},
                 _triggercat      = args.triggercat,
                 _create_tuples   = args.makeTuples,
                 _trainTMVA       = args.trainTMVA,
                 folding_method   = args.foldingMethod,
                 _zoom_ROC        = args.zoom_ROC,
                 additional_sel   = additional_sel,
                 _drawROC         = args.drawROC,
                 _applyClassifier = args.applyClassifier,
                 _FOM             = args.computeFOM)
        '''

    elif args.option == "fitSidebands":
        from scripts.fits import fit_sidebands
        from scripts.selections import cuts
        fit_sidebands(channel      = args.channel,
                      year         = args.year,
                      triggercat   = args.trigger,
                      return_yield = args.return_yield,
                      _show        = args.showPlot,
                      save_canvas  = args.saveCanvas,
                      ofilename    = args.ofilename,
                      sel          = cuts["sidebands"] if args.cuts else None,
                      _makePlot    = True)
    elif args.option == "fitNorm":
        from scripts.fits import fit_norm
        fit_norm(year               = args.year,
                 fix_params_from_mc = args.fix_params_from_MC)

if __name__ == "__main__":
    main()
