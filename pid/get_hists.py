import os
import pandas
import numpy

from scripts         import printinfo, printwarning, printerror, printdebug
from utils.ana_utils import years
from tqdm            import tqdm

def add_hists(all_hists):
    '''
    Add a list of histgrams in dictionaries. The output histograms will have the same dictionary structure as each of them.

    :all_hists: array of dict, where the dict contains the histograms to add.
    '''
    total_hists = all_hists[0]
    for hist_dict in all_hists[1:]:
        for name in total_hists:
            total_hists[name] += hist_dict[name]

    return total_hists

def binomial_error_sweights(num_pass, num_total, err_pass_sq, err_total_sq):
    '''
    Compute the uncertainty of toy studies that use sWeights.
    Formula taken from (13) in https://cds.cern.ch/record/2202412/files/LHCb-PUB-2016-021.pdf

    :num_pass:     int, events that passed
    :num_total:    int, total number of experiments
    :err_pass_sq:  float, error of the events that passed, squared (i.e., variance)
    :err_total_sq: float, error of the total events, squared (i.e., variance)
    '''
    prob = num_pass/num_total
    prob_sq = prob**2
    num_total_sq = num_total**2
    
    return numpy.sqrt(abs(((1 - 2 * prob) * err_pass_sq + err_total_sq * prob_sq) / num_total_sq))

def make_hist_narray(df, bin_var_names, bins):
    '''
    Algorithm to compute the efficiency histograms for the custom binning. It sums the sWeights of the events
    that pass each of the subsequent cuts of the three binning variables. The error is computed squaring the sWeights and summing over them.

    :df:            pandas DataFrame, contains the calibration sample
    :bin_var_names: array of str, names of the binning variables
    :bins:          array of float, bin edges
    '''
    var1, var2, var3 = bin_var_names
    myarray = numpy.zeros((len(bins[var1])-1,
                           len(bins[var2][0])-1,
                           len(bins[var3][0][0])-1))
    myarray_err = myarray.copy()
        
    for j in range(len(bins[var1])-1):
        for k in range(len(bins[var2][j])-1):
            for l in range(len(bins[var3][j][k])-1):
                sweights = df.query("({0}>={3} and {0}<{4}) and ({1}>={5} and {1}<{6}) and ({2}>={7} and {2}<{8})"\
                                    .format(*bin_var_names,
                                            bins[var1][j],              bins[var1][j+1],
                                            bins[var2][j][k],         bins[var2][j][k+1],
                                            bins[var3][j][k][l], bins[var3][j][k][l+1]))["sWeight"]

                myarray[j][k][l] += sweights.sum()
                sweights_array = sweights.to_numpy()
                myarray_err[j][k][l] += numpy.square(sweights_array).sum()

    return myarray, myarray_err

def compute_hists(year, mag, particle, max_files, bin_var_names, cuts, pid_cuts, nbins_array, hists_filename, force_compute_bins = False):
    '''
    Main function to compute the calibration histograms for a given particle, year, magnet polarity and max number of files.

    :year:                str or int, ("2016", "2017", "2018")
    :mag:                 str, ("Up", "Down")
    :particle:            str, name of the particle to calibrate
    :max_files:           int or None, maximum number of files to process. If None or -1, all will be processed
    :bin_var_names:       array of str, names of the binning variables
    :cuts:                array of str, cuts to apply to the samples so that both the calibration and reference samples are aligned
    :pid_cuts:            array of str, PID cuts to study
    :nbins_array:         array of int, with the number of bins for each of the variables
    :histsfilename:       str, path to the output pickle file where the bins will be stored
    :force_compute_hists: bool, True if we want to compute the histograms again even if they have already been computed
    '''
    from utils.pidutils  import (get_samples     , get_tree_paths,
                                 get_branch_names, load_pidcalib_sample_as_dataframe)
    from utils.pyutils   import save_data_file
    from pid.get_binning import setup_binning
    
    sample = "Turbo"+str(year)[-2:]

    calib_sample = get_samples(sample    = sample,
                               mag       = mag,
                               particle  = particle,
                               max_files = max_files)

    tree_names = get_tree_paths(particle = particle)

    bin_vars, cut_vars, pidcut_vars = get_branch_names(bin_var_names, cuts, pid_cuts)
    branches = {**bin_vars, **cut_vars, **pidcut_vars}

    mybins = setup_binning(year               = year,
                           mag                = mag,
                           particle           = particle,
                           bin_var_names      = bin_var_names,
                           cuts               = cuts,
                           pid_cuts           = pid_cuts,
                           nbins_array        = nbins_array,
                           max_files          = max_files,
                           force_compute_bins = force_compute_bins)
    
    all_hists = {}
    printinfo("Getting histograms")
    for k,path in enumerate(tqdm(calib_sample["files"], leave = True, desc = "Processing files")):
        df = load_pidcalib_sample_as_dataframe(path, tree_names, branches, cuts)

        hists = {}
        hists["total"], hists["total_err"] = make_hist_narray(df, bin_var_names, mybins)

        df_pidcut = df.query(" and ".join(pid_cuts))
        hists["passing"], hists["passing_err"] = make_hist_narray(df_pidcut, bin_var_names, mybins)
        all_hists[path] = hists

    hists = add_hists(list(all_hists.values()))

    hists["eff"] = hists["passing"].copy()
    hists["eff"] = hists["passing"]/hists["total"]
    hists["eff_err"] = numpy.square(binomial_error_sweights(hists["passing"],
                                                              hists["total"],
                                                              hists["passing_err"],
                                                              hists["total_err"]))

    save_data_file(hists_filename, hists)

    printinfo("Saved histograms in {}".format(hists_filename))

    return hists

def setup_hists(year, mag, particle, bin_var_names,
                cuts, pid_cuts, nbins_array, max_files,
                force_compute_bins = False, force_compute_hists = False):
    '''
    Set up the histograms for a given year, magnet polarity, and set of preliminary cuts and PID cuts,
    as well as a specific number of bins for each of the binning variables.

    :year:                str or int, (2016, 2017, 2018)
    :mag:                 str, ("Up", "Down")
    :particle:            str, name of the particle to be studied
    :bin_var_names:       array of str, names of the binning variables
    :cuts:                array of str, preliminary cuts to align the calibration and reference samples
    :pid_cuts:            array of str, PID cut(s) to study
    :nbins_array:         array of 3 ints, it contains the number of bins each of the binning variables will have
    :force_compute_bins:  bool, True if we want to compute the bins again even if they have already been computed
    :force_compute_hists: bool, True if we want to compute the histograms again even if they have already been computed
    '''
    from utils.pidutils import get_custom_filename
    from utils.pyutils  import load_data_file, file_exists
    
    if pid_cuts == ["DLLmu>0"] and "IsMuon==1" not in cuts:
        printwarning("PID cut to be studied is 'DLLmu>0' and 'IsMuon==1' is not in the user-given cuts. Adding it. If you wish for this not to be the case, go back to the code and fix it.")
        cuts += ["IsMuon==1"]

    hists_filename = get_custom_filename(mode          = "hists",
                                         year          = year,
                                         mag           = mag,
                                         particle      = particle,
                                         bin_var_names = bin_var_names,
                                         cuts          = cuts,
                                         pid_cuts      = pid_cuts,
                                         nbins_array   = nbins_array)

    printdebug("Output hists filename set to {}. File does {}exist".format(hists_filename, "not "*(not file_exists(hists_filename))))
    if not file_exists(hists_filename) or force_compute_hists:
        return compute_hists(year               = year,
                             mag                = mag,
                             particle           = particle,
                             max_files          = max_files,
                             bin_var_names      = bin_var_names,
                             cuts               = cuts,
                             pid_cuts           = pid_cuts,
                             nbins_array        = nbins_array,
                             hists_filename     = hists_filename,
                             force_compute_bins = force_compute_bins)
    else:
        return load_data_file(hists_filename)

def add_arguments(parser):
    parser.add_argument("--year", "-y", type=str, default = "2016",
                        choices = years, help = "Year to run.")
    parser.add_argument("--mag", "-m", type = str, default = "Up",
                        choices = ["Up", "Down"], help = "Magnet polarity to run.")
    parser.add_argument("--particle", "-p", type=str, default = "Mu_nopt",
                        help = "Particle to calibrate")
    parser.add_argument("--bin-vars", "-bv", default = "['P', 'ETA', 'nSPDhits']",
                        help = "Array containing the names of the binning variables")
    parser.add_argument("--cuts", "-c", type = str,
                        default = '["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1"]',
                        help = "Array containing the cuts to align the reference and calibration samples.")
    parser.add_argument("--pid-cuts", type = str, default = '["IsMuon==1"]',
                        help = "PID cuts to study. It inputs an array")
    parser.add_argument("--nbins", type = str, default = '[3, 4, 4]',
                        help = "Array containing the number of bins for the binning variables (in the same order they are given)")
    parser.add_argument("--max-files", "-max", type = int, default = None,
                        help = "Maximum number of files to process when computing the binning")
    parser.add_argument("--force-compute-bins", "-fb", action = "store_true", default = False,
                        help = "Compute the bins even if they have already been computed for this setup.")
    parser.add_argument("--force-compute-hists", "-fh", action = "store_true", default = False,
                        help = "Compute the histograms even if they have already been computed for this setup.")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse, ast
    parser = argparse.ArgumentParser(description = "Compute the PIDCalib histograms for a given combination of year,"\
                                     "magnet polarity, user cuts, PID cuts and number of bins for the binning variables")

    add_arguments(parser)

    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    setup_hists(year                = args.year,
                mag                 = args.mag,
                particle            = args.particle,
                bin_var_names       = ast.literal_eval(args.bin_vars),
                cuts                = ast.literal_eval(args.cuts),
                pid_cuts            = ast.literal_eval(args.pid_cuts),
                nbins_array         = ast.literal_eval(args.nbins),
                max_files           = args.max_files,
                force_compute_bins  = args.force_compute_bins,
                force_compute_hists = args.force_compute_hists)
    
