import os
import pandas
import numpy

from scripts         import printinfo, printwarning, printerror, printdebug
from utils.ana_utils import years
from tqdm            import tqdm

def add_bin_indices(df, eff_hists, bins, bin_vars):
    '''
    Based on the efficiency histograms computed with the calibration samples,
    this method assigns a bin index to each of the event, based on the values of each of the bin variables

    :df:        pandas DataFrame, reference sample
    :eff_hists: numpy array, efficiency histograms
    :bins:      numpy array, bin edges
    :bin_vars:  array of str, names of the bin variables
    '''
    from utils.pidutils import mybranches
    
    df_new = df.copy()
    bins_flat = {key:val.flatten() for key,val in bins.items()}
    var1, var2, var3 = bin_vars
    branches_dict = mybranches()
    for j in range(1,5):
        printdebug("Assigning bins to muon {}".format(j))
        _get_branch_name = lambda name: name[0].replace("mu1", "mu{}".format(j)) if isinstance(name, list) else name
        _branches = [_get_branch_name(branches_dict[var]) for var in bin_vars]
        vals = df_new[_branches].transpose().to_numpy()
        calibbins = {var: numpy.array([]) for var in bin_vars}
        for k in range(len(vals[0])):
            bin1 = pandas.cut([vals[0][k]], bins[var1], labels = False, include_lowest = True, right = False, precision = 0)[0]
            bin2 = pandas.cut([vals[1][k]], bins[var2][bin1], labels = False, include_lowest = True, right = False, precision = 0)[0] if not numpy.isnan(bin1) else bin1
            bin3 = pandas.cut([vals[2][k]], bins[var3][bin1][bin2], labels = False, include_lowest=True,right=False,precision=0) if not numpy.isnan(bin2) else bin2

            calibbins[var1] = numpy.append(calibbins[var1], bin1)
            calibbins[var2] = numpy.append(calibbins[var2], bin2)
            calibbins[var3] = numpy.append(calibbins[var3], bin3)
            
            
        df_new["mu{0}_{1}_PIDCalibBin".format(j,var1)] = calibbins[var1]
        df_new["mu{0}_{1}_PIDCalibBin".format(j,var2)] = calibbins[var2]
        df_new["mu{0}_{1}_PIDCalibBin".format(j,var3)] = calibbins[var3]

        df_nan = df_new[df_new.isna().any(axis=1)]
        df_new.dropna(inplace=True)
        
        index_names = ["mu{0}_{1}_PIDCalibBin".format(j,var1), "mu{0}_{1}_PIDCalibBin".format(j,var2), "mu{0}_{1}_PIDCalibBin".format(j,var3)]

        ## Turns the three-dimensional bin indices into a one-dimensional bin index
        indices = numpy.ravel_multi_index(df_new[index_names].transpose().to_numpy().astype(int),
                                          (len(bins[var1])-1, len(bins[var2][0])-1, len(bins[var3][0][0])-1))
        df_new["mu{}_PIDCalibBin".format(j)] = indices
        df_new = pandas.concat([df_new, df_nan]).sort_index()
        
    return df_new

def add_efficiencies(df, efficiency_table, error_table):
    '''
    Assigns the efficiency to each of the events based on the bin index that was assigned in add_bin_indices()

    :df:               pandas DataFrame, reference sample
    :efficiency_table: numpy array, values of the efficiencies for each of the bin indices
    :error_table:      numpy array, errors of the efficiencies for each of the bin indices
    '''
    df_new = df.copy()

    df_nan = df_new[df_new.isna().any(axis=1)]  # type: ignore
    df_new.dropna(inplace=True)
    
    df_new["PIDCalibEff"] = 1
    df_new["PIDCalibRelErr2"] = 0
    for k in range(1,5):
        df_new[f"mu{k}_PIDCalibEff"] = numpy.take(efficiency_table, df_new[f"mu{k}_PIDCalibBin"])
        df_new[f"mu{k}_PIDCalibErr"] = numpy.take(error_table     , df_new[f"mu{k}_PIDCalibBin"])
        df_new["PIDCalibEff"] *= df_new["mu{}_PIDCalibEff".format(k)]
        df_new["PIDCalibRelErr2"] += (df_new["mu{}_PIDCalibErr".format(k)]/df_new["mu{}_PIDCalibEff".format(k)])**2

    df_new["PIDCalibErr"] = numpy.sqrt(df_new["PIDCalibRelErr2"])
    for k in range(1,5):
        df_new["PIDCalibErr"] *= df_new["mu{}_PIDCalibEff".format(k)]

    negative_mask = df_new.eval("|".join(f"mu{k}_PIDCalibEff<0" for k in range(1,5)))

    if any(negative_mask):
        printwarning("There are bins with negative efficiency:")
        print(negative_mask)

    df_new = pandas.concat([df_new, df_nan]).sort_index()
        
    return df_new


def binomial_error(prob, ntotal):
    '''
    Compute the error of a quantity that follows a binomial distribution.

    :prob:   float, events that passed over total number of events
    :ntotal: int, total number of events
    '''
    return numpy.sqrt(prob*(1-prob)/ntotal)

def calibrate(info, year, mag, particle, bin_var_names, cuts, pid_cuts, nbins_array, force_compute_hists = False, force_compute_bins = False):
    '''
    Main function to perform the calibration. It gets the reference sample and computes the efficiency of the given PID cuts.
    It also performs the factorization, where two of the four muons will turned off (setting !InMuonAcc) so that the sample
    is as similar as possible to the calibration sample, that only contains two muons.

    :info:                dict, contains the efficiencies of the years and polarities studied (leave it as {} if this is the first)
    :year:                str or int, year to study -- (2016, 2017, 2018)
    :mag:                 str, magnet polarity -- ("Up", "Down")
    :particle:            str, name of the particle to calibrate
    :bin_var_names:       array of str, names of the binning variables
    :cuts:                array of str, user cuts to align calibration and reference samples
    :pid_cuts:            array of str, PID cuts to study
    :nbins_array:         array of int, number of bins for each of the binning variables
    :force_compute_hists: bool, True if the histograms are to be computed again, even if they already exist
    :force_compute_bins:  boo, 
    '''
    from utils.pyutils   import flatten_array
    from pid.get_binning import setup_binning
    from pid.get_hists   import setup_hists
    from utils.pidutils  import mybranches

    branches_dict = mybranches()
    
    bins  = setup_binning(year, mag, particle, bin_var_names, cuts, pid_cuts, nbins_array, max_files = None, force_compute_bins = force_compute_bins)
    hists = setup_hists(year, mag, particle, bin_var_names, cuts, pid_cuts, nbins_array, max_files = None, force_compute_hists = force_compute_hists)
    df_ref, info, cuts_inacc = get_reference_dataframe(info          = info,
                                                       year          = year,
                                                       mag           = mag,
                                                       bin_var_names = bin_var_names,
                                                       cuts          = cuts,
                                                       pid_cuts      = pid_cuts)
    df_ref_eff = df_ref.copy()

    df_ref_eff = add_bin_indices(df_ref_eff, hists["eff"], bins, bin_vars = {k:branches_dict[k] for k in list(bins.keys())})
    df_ref_eff_InAcc = df_ref_eff.copy()
    df_ref_eff_InAcc.query(" and ".join(cuts_inacc), inplace=True)
    df_ref_eff_InAcc = add_efficiencies(df_ref_eff_InAcc, hists["eff"], hists["eff_err"])

    pidcalibeff = df_ref_eff_InAcc["PIDCalibEff"].mean()
    pidcaliberr = df_ref_eff_InAcc["PIDCalibErr"].mean()*pidcalibeff

    pidcalibeff = round(pidcalibeff, 7)
    pidcaliberr = round(pidcaliberr, 7)

    printinfo("Average value of PIDCalibEff: {0} +/- {1}".format(pidcalibeff, pidcaliberr))

    info["PIDCalibEff"] = pidcalibeff
    info["PIDCalibErr"] = pidcaliberr
    
    info = factorization(df_ref_eff, info, year, mag, hists["eff"], hists["eff_err"], pid_cuts)

    printinfo("="*70)
 
    return info

def factorization(df, info, year, mag, efficiency_table, error_table, pidcuts):
    '''
    Compute the PID cuts efficiencies setting two of the four muons to be out of muon acceptance.
    This way, we align the calibration and reference samples as much as possible, since the calibration
    uses J/psi -> mu+ mu-

    :df:               pandas DataFrame, reference sample
    :info:             dict, contains the efficiencies of the years and polarities studied (leave it as {} if this is the first)
    :year:             str or int, year to study -- (2016, 2017, 2018)
    :mag:              str, magnet polarity -- ("Up", "Down")
    :efficiency_table: numpy array, values of the efficiencies for each of the bin indices
    :error_table:      numpy array, errors of the efficiencies for each of the bin indices
    :pidcuts:          array of str, PID cuts to study
    '''
    for j in range(1,5):
        printinfo("Factorizing muon {}".format(j))
        for i in range(3):
            sel = "(mu{0}_isMuon==1 and mu{1}_InMuonAcc{3} and mu{2}_InMuonAcc{4}) or "\
                  "(mu{1}_isMuon==1 and mu{2}_InMuonAcc{3} and mu{0}_InMuonAcc{4}) or "\
                  "(mu{2}_isMuon==1 and mu{0}_InMuonAcc{3} and mu{1}_InMuonAcc{4})".format(j+1  if j<4  else j-3,
                                                                                         j+2   if j<3  else j-2,
                                                                                         j+3   if j<2  else j-1,
                                                                                          "<0" if i==0 else ">0",
                                                                                          "<0" if i<2  else ">0")
            
            eff_sel = " and ".join(["mu{0}_{1}".format(j, cut.replace("IsMuon", "isMuon").replace("DLL", "PID")) for cut in pidcuts])

            df_new = df.copy()
            df_nan = df_new[df_new.isna().any(axis=1)]
            df_new.dropna(inplace=True)

            df_new.query(sel, inplace = True)
            if df_new.shape[0] == 0:
                printwarning("Selection {} returns an empty dataframe".format(sel))
                continue

            mceff = df_new.query(eff_sel).shape[0]*1./df_new.shape[0]
            mcerr = binomial_error(mceff, df_new.shape[0])
            printinfo("MC eff of factorization {0}: {1} +/- {2}".format(i+1, round(mceff, 4), round(mcerr, 4)))
            df_new["PIDCalibEff_Factor{}".format(i+1)] = numpy.take(efficiency_table, df_new["mu{0}_PIDCalibBin".format(j)])
            df_new["PIDCalibErr_Factor{}".format(i+1)] = numpy.take(error_table     , df_new["mu{0}_PIDCalibBin".format(j)])
            pidcalibeff = round(df_new["PIDCalibEff_Factor{}".format(i+1)].mean(), 7)
            pidcaliberr = round(df_new["PIDCalibErr_Factor{}".format(i+1)].mean(), 7)

            info["PIDCalibEff_Factor{}".format(i+1)] = pidcalibeff
            info["PIDCalibErr_Factor{}".format(i+1)] = pidcaliberr

            printinfo("PIDCalib eff of factorization {0}: {1} +/- {2}".format(i+1, pidcalibeff, pidcaliberr))

        print()

    return info

def get_reference_dataframe(info, year, mag, bin_var_names, cuts, pid_cuts):
    '''
    Get the reference sample and turn it into a pandas DataFrame.
    It applies the given user cuts meant to align the calibration and reference samples,
    except for InMuonAcc and IsMuon. This is because for the factorization step, we are not interested
    in a sample with these cuts applied. It returns the IsMuon or InMuonAcc cuts to be applied for the calibration.

    :year:        str or int, (2016, 2017, 2018)
    :mag          str, ("Up", "Down")
    :bin_var_names: array of str, names of the binning variables
    :cuts:          array of str, user cuts to align the calibration and reference samples
    :pid_cuts:      array of str, PID cuts to study. They are used to compute the MC efficiency
    '''
    from utils.pyutils   import flatten_array
    from utils.pidutils  import mybranches, get_branch_names
    from utils.rootutils import get_rootfile_as_dataframe, branches_by_regex

    ifilename, itreename = setup_pid_ntuple(year)
    branches_regex = branches_by_regex(ifilename = ifilename,
                                       itreename = itreename,
                                       regex     = ".")

    df_nostrip = get_rootfile_as_dataframe(ifilename, [itreename], branches_regex)
    df_strip   = sel_stripping_polarity(df_nostrip, mag)

    bin_vars, cut_vars, pidcut_vars = get_branch_names(bin_var_names, cuts, pid_cuts)
    if "InMuonAcc" not in cut_vars:
        cut_vars.update({"InMuonAcc":"probe_InMuonAcc"})

    branches_dict = mybranches()
    branches = [branches_dict[var] for var in list({**bin_vars, **cut_vars, **pidcut_vars}.keys()) if var in branches_dict]
    branches = flatten_array(branches)

    df = df_strip[branches]

    mycuts = []; cuts_inacc = []
    for cut in cuts:
        for var in branches_dict:
            if var in cut and var != "InMuonAcc" and var != "IsMuon":
                mycuts += [cut.replace(var, newvar) for newvar in branches_dict[var]]
                break
            elif var in cut and (var == "InMuonAcc" or var == "IsMuon"):
                cuts_inacc += [cut.replace(var, newvar) for newvar in branches_dict[var]]
                break

    mypidcuts = []
    for cut in pid_cuts:
        for var in branches_dict:
            if var in cut:
                mypidcuts += [cut.replace(var, newvar) for newvar in branches_dict[var]]
                break

    df = df.query(" and ".join(mycuts))
            
    mceff = df.query(" and ".join(mypidcuts+cuts_inacc)).shape[0]/df.query(" and ".join(cuts_inacc)).shape[0]
    mcerr = binomial_error(mceff, df.query(" and ".join(cuts_inacc)).shape[0])
    mceff, mcerr = round(mceff, 4), round(mcerr, 4)
    printinfo("{0} efficiency on MC: {1} +/- {2}".format(" and ".join(pid_cuts), mceff, mcerr))

    info["MCEff"], info["MCErr"] = mceff, mcerr

    return df, info, cuts_inacc

def sel_stripping_polarity(df, mag):
    '''
    Apply stripping cuts and select a given polarity from dataframe
    '''
    from scripts.selections import cuts
    from utils.pidutils     import _apply_sel

    stripcuts = cuts["strippingsig"].pyexpr

    sel = stripcuts + " and (Polarity=={})".format(1 if mag.lower() == "up" else -1)

    return _apply_sel(df, sel)

def setup_pid_ntuple(year):
    '''
    Returns the name of the MC ROOT file with the appropriate corrections for a given year and polarity.
    '''
    from scripts.files_info import get_filename
    from utils.file_utils   import get_ofilename, get_ifilename

    _, tnames = get_filename(data_type = "MC",
                              year     = year,
                              channel  = "K0S2mu4")
    ifilename = get_ifilename(data_type = "MC",
                              year      = year,
                              channel   = "K0S2mu4")
    treename = tnames[1] ## reco tuple is the second element in the array

    ofilename  = get_ofilename(corr      = "HorizontalCorrection", 
                               ifilename = get_ofilename(corr = "AcceptReject", ifilename = ifilename))
    #path = scripts.get_tuples_path()
    #ofilename = os.path.join(path, "MC_K0S2mu4_{0}_RecoTupleNoPIDsMuons_PTMCFilter_Corr_WithMatchApplied_Mag{1}_WithStripping.root".format(year, mag))

    #if not os.path.exists(ofilename):
    #    printerror("Calibration sample {} does not exist!".format(ofilename))

    return ofilename, treename

def add_arguments(parser):
    parser.add_argument("--year", "-y", type=str, default = "2016",
                        choices = years, help = "Year to run.")
    parser.add_argument("--mag", "-m", type = str, default = "Up",
                        choices = ["Up", "Down"], help = "Magnet polarity to run.")
    parser.add_argument("--particle", "-p", type=str, default = "Mu_nopt",
                        help = "Particle to calibrate")
    parser.add_argument("--bin-vars", "-bv", default = "['P', 'ETA', 'nSPDhits']",
                        help = "Array containing the names of the binning variables")
    parser.add_argument("--cuts", "-c", type = str,
                        default = '["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1", "ETA<5.2"]',
                        help = "Array containing the cuts to align the reference and calibration samples.")
    parser.add_argument("--pid-cuts", type = str, default = '["IsMuon==1"]',
                        help = "PID cuts to study. It inputs an array")
    parser.add_argument("--nbins", type = str, default = '[3, 4, 4]',
                        help = "Array containing the number of bins for the binning variables (in the same order they are given)")
    parser.add_argument("--force-compute-bins", "-fb", action = "store_true", default = False,
                        help = "Compute the bins even if they have already been computed for this setup.")
    parser.add_argument("--force-compute-hists", "-fh", action = "store_true", default = False,
                        help = "Compute the histograms even if they have already been computed for this setup.")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse, ast, re

    parser = argparse.ArgumentParser(description = "Compute the calibration efficiency of a given particle for a given combination of year,"\
                                     "magnet polarity, user cuts, PID cuts and number of bins for the binning variables")
    add_arguments(parser)

    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()
    
    info = {}
    info[pidvar] = calibrate(info                = {},
                             year                = args.year,
                             mag                 = args.mag,
                             particle            = args.particle,
                             bin_var_names       = ast.literal_eval(args.bin_vars),
                             cuts                = ast.literal_eval(args.cuts),
                             pid_cuts            = ast.literal_eval(args.pid_cuts),
                             nbins_array         = ast.literal_eval(args.nbins),
                             force_compute_bins  = args.force_compute_bins,
                             force_compute_hists = args.force_compute_hists)

    for k,v in info[pidvar].items():
        info[pidvar][k] = [v]
    df = pandas.DataFrame.from_dict(info[pidvar])

    print(df)
