import os
import pandas
import numpy

from scripts         import printinfo, printwarning, printerror, printdebug
from utils.ana_utils import years
from tqdm            import tqdm

def compute_bins(year, mag, particle, max_files, bin_var_names, cuts, pid_cuts, nbins_array, binning_filename):
    '''
    Main function to compute the binnings for a given year, magnet polarity and max number of files.

    :year:             str or int, ("2016", "2017", "2018")
    :mag:              str, ("Up", "Down")
    :max_files:        int or None, maximum number of files to process. If None or -1, all will be processed
    :bin_var_names:    array of str, names of the binning variables
    :cuts:             array of str, cuts to apply to the samples so that both the calibration and reference samples are aligned
    :pid_cuts:         array of str, PID cuts to study
    :nbins_array:      array of int, with the number of bins for each of the variables
    :binning_filename: str, path to the output pickle file where the bins will be stored
    '''
    from utils.pidutils import (get_samples     , get_tree_paths,
                                get_branch_names, load_pidcalib_sample_as_dataframe)
    from utils.pyutils  import save_data_file

    sample = "Turbo"+str(year)[-2:]
    calib_sample = get_samples(sample    = sample,
                               mag       = mag,
                               particle  = particle,
                               max_files = max_files)

    tree_names = get_tree_paths(particle = particle)

    bin_vars, cut_vars, pidcut_vars = get_branch_names(bin_var_names, cuts, pid_cuts)
    branches = {**bin_vars, **cut_vars, **pidcut_vars}
    
    all_bins = []
    printinfo("Computing binning")
    for k,path in enumerate(tqdm(calib_sample["files"], leave = True, desc = "Processing files")):
        df = load_pidcalib_sample_as_dataframe(path, tree_names, branches, cuts+pid_cuts)

        bins = compute_custom_bins(df, particle, bin_var_names, numbins_array = nbins_array)
        all_bins.append(bins)

    ## We average the binning edges of all of the processed files
    avg_bins = compute_avg_bins(all_bins)
    save_data_file(binning_filename, avg_bins)

    printinfo("Binnings saved into {}".format(binning_filename))

    return avg_bins

def _get_p_bins(df, numbins):
    '''
    Split the P variable into the given number of bins, such that the
    edges given by the IsMuon chambers are included.
    
    :df:      pandas DataFrame, dataset used to compute the P bins
    :numbins: int, desired number of P bins
    '''
    basic_bins = [3000, 6000, 10000, df["P"].max()]
    if numbins < 3:
        printwarning("Given number of P bins is below 3. Using [3000, 6000, 10000, max(P)] by default")

    sel = lambda lim1, lim2: "P>{0} and P<{1}".format(lim1, lim2)
    bins = []
    for k in range(len(basic_bins)-1):
        nbins = int(numpy.ceil(numbins/5)) if k < 2 else numbins - 2*int(numpy.ceil(numbins/5))
        bins += split_bin(df, "P", sel(basic_bins[k], basic_bins[k+1]), nbins, basic_bins[k], basic_bins[k+1])

    return sorted(set(bins))

def split_bin(df, varname, sel, nbins, minval_bin = None, maxval_bin = None):
    '''
    Split a dataset into a given number of bins for a given variable

    :df:         pandas DataFrame, initial dataset
    :varname:    str, name of the variable to split into bins
    :sel:        str, selection to apply to the dataset
    :nbins:      int, number of bins to compute
    :minval_bin: int or None, min value of the variable on the dataset (can be customized or set to default with None)
    :maxval_bin: int or None, max value of the variable on the dataset (can be customized or set to default with None)
    '''
    from utils.pidutils import _apply_sel
    
    df_new = _apply_sel(df, sel)
    minval_bin = minval_bin or df_new[varname].min()
    maxval_bin = maxval_bin or df_new[varname].max()
    entries_per_bin = df_new["sWeight"].sum()/nbins
    step = (maxval_bin - minval_bin)*1./1000

    newbins = [minval_bin]
    for j in range(nbins):
        if j != nbins-1:
            increment = 0
            while df_new.query("{0}>{1} and {0}<{2}".format(varname, newbins[-1], newbins[-1]+increment))["sWeight"].sum() < entries_per_bin:
                increment += step
            newbins.append(round(newbins[-1]+increment, 3))
        else:
            newbins.append(maxval_bin)

    return newbins

def compute_avg_bins(all_bins):
    '''
    Average all bins from the processed files

    :all_bins: array, binning scheme
    '''
    numfiles = len(all_bins)

    avg_bins = {}
    for var in list(all_bins[0].keys()):
        avg_bins[var] = numpy.zeros(all_bins[0][var].shape)
        for k in range(numfiles):
            avg_bins[var] += all_bins[k][var]

        avg_bins[var] /= numfiles

    return avg_bins

def compute_custom_bins(df, particle, bin_var_names, numbins_array):
    '''
    Function that implements the binning computation algorithm.
    This will use a hard-coded set of P bins, and for each one, divide the sample into
    a given number of ETA bins, so that each one has roughly the same sum of sWeights.
    That will happen again with the nSPDhits variable

    :df:             pandas DataFrame, sample dataframe to use for the binning division
    :bin_var__names: array of str, names of the binning variables
    :numbins_array:  array of int, number of bins for each of the three variables
    '''

    var1, var2, var3 = bin_var_names
    if var1 == "P" and "mu" in particle.lower():
        var1_bins = _get_p_bins(df, numbins = numbins_array[0])
    elif (var2 == "P" or var3 == "P") and "mu" in particle.lower():
        printerror("If you want to calibrate muons using their total momentum, please set 'P' as your first binning variable."\
                   "This is so the efficiencies of the IsMuon stations are not mixed.")
        raise RuntimeError()
    else:
        printwarning("Having a first binning variable not be 'P' has not been tested. It's possible some errors may appear.")
        var1_bins = split_bin(df, var1, "", numbins_array[0])

    var2_bins, var3_bins = [], []
    for l in range(len(var1_bins)-1):
        sel_var2   = "{0}>{1} and {0}<{2}".format(var1, var1_bins[l], var1_bins[l+1])
        var2_bins += [split_bin(df, var2, sel_var2, numbins_array[1])]

        var3_bins.append([])
        for k in range(len(var2_bins[l])-1):
            sel_var3 = sel_var2 + " and " + "{0}>{1} and {0}<{2}".format(var2, var2_bins[l][k], var2_bins[l][k+1])
            var3_bins[l] += [split_bin(df, var3, sel_var3, numbins_array[2])]

    bins = {var1 : numpy.array(var1_bins),
            var2 : numpy.array(var2_bins),
            var3 : numpy.array(var3_bins)
            }
    
    return bins
    
def setup_binning(year, mag, particle, bin_var_names, cuts, pid_cuts, nbins_array, max_files, force_compute_bins = False):
    '''
    Set up the binning for a given year, magnet polarity, and set of preliminary cuts and PID cuts,
    as well as a specific number of bins for each of the binning variables.

    :year:               str or int, (2016, 2017, 2018)
    :mag:                str, ("Up", "Down")
    :particle:           str, name of the particle to be studied
    :bin_var_names:      array of str, names of the binning variables
    :cuts:               array of str, preliminary cuts to align the calibration and reference samples
    :pid_cuts:           array of str, PID cut(s) to study
    :nbins_array:        array of 3 ints, it contains the number of bins each of the binning variables will have
    :force_compute_bins: bool, True if we want to compute the bins again even if they have already been computed
    '''
    from utils.pidutils import get_custom_filename
    from utils.pyutils  import load_data_file, file_exists

    if pid_cuts == ["DLLmu>0"] and "IsMuon==1" not in cuts:
        printwarning("PID cut to be studied is 'DLLmu>0' and 'IsMuon==1' is not in the user-given cuts. Adding it. If you wish for this not to be the case, go back to the code and fix it.")
        cuts += ["IsMuon==1"]

    binning_filename = get_custom_filename(mode          = "bins",
                                           year          = year,
                                           mag           = mag,
                                           particle      = particle,
                                           bin_var_names = bin_var_names,
                                           cuts          = cuts,
                                           pid_cuts      = pid_cuts,
                                           nbins_array   = nbins_array)

    printdebug("Output binning filename set to {}. File does {}exist".format(binning_filename, "not "*(not file_exists(binning_filename))))
    if not file_exists(binning_filename) or force_compute_bins:
        return compute_bins(year, mag, particle, max_files, bin_var_names, cuts, pid_cuts, nbins_array, binning_filename)
    else:
        return load_data_file(binning_filename)

def add_arguments(parser):
    parser.add_argument("--year", "-y", type=str, default = "2016",
                        choices = years, help = "Year to run.")
    parser.add_argument("--mag", "-m", type = str, default = "Up",
                        choices = ["Up", "Down"], help = "Magnet polarity to run.")
    parser.add_argument("--particle", "-p", type=str, default = "Mu_nopt",
                        help = "Particle to calibrate")
    parser.add_argument("--bin-vars", "-bv", default = "['P', 'ETA', 'nSPDhits']",
                        help = "Array containing the names of the binning variables")
    parser.add_argument("--cuts", "-c", type = str,
                        default = '["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1"]',
                        help = "Array containing the cuts to align the reference and calibration samples.")
    parser.add_argument("--pid-cuts", type = str, default = '["IsMuon==1"]',
                        help = "PID cuts to study. It inputs an array")
    parser.add_argument("--nbins", type = str, default = '[3, 4, 4]',
                        help = "Array containing the number of bins for the binning variables (in the same order they are given)")
    parser.add_argument("--max-files", "-max", default = None,
                        help = "Maximum number of files to process when computing the binning")
    parser.add_argument("--force-compute-bins", "-fb", action = "store_true", default = False,
                        help = "Compute the bins even if they have already been computed for this setup.")
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse, ast

    parser = argparse.ArgumentParser(description = "Run the binning for a given combination of year,"\
                                     "magnet polarity, user cuts, PID cuts and number of bins for the binning variables")
    add_arguments(parser)

    args = parser.parse_args()
    
    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    setup_binning(year               = args.year,
                  mag                = args.mag,
                  particle           = args.particle,
                  bin_var_names      = ast.literal_eval(args.bin_vars),
                  cuts               = ast.literal_eval(args.cuts),
                  pid_cuts           = ast.literal_eval(args.pid_cuts),
                  nbins_array        = ast.literal_eval(args.nbins),
                  max_files          = args.max_files if args.max_files != "None" else None,
                  force_compute_bins = args.force_compute_bins)
    
