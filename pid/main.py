def get_K0S2mu4_params(pid_cuts):
    particle = "Mu_nopt"
    cuts     = ["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1", "ETA<5.2"]
    if pid_cuts == ["DLLmu>0"]:
        cuts += ["IsMuon==1"]
    bin_var_names = ["P", "ETA", "nSPDhits"]
    nbins_array   = [3, 4, 4]
    
    return {"particle"     : particle,
            "cuts"         : cuts,
            "pid_cuts"     : pid_cuts,
            "bin_var_names": bin_var_names,
            "nbins_array"  : nbins_array}
            
def get_pidvar_from_pidcut(pidcut):
    import re

    if not isinstance(pidcut, list): pidcut = [pidcut]
    pidvar = ""

    for p in pidcut:
        pidvar += re.sub(r"(==|<|>)[0-9]", "", p) + "&"
    
    return pidvar[:-1]

def run():
    from pid.calibrate import calibrate

    info = {}
    myinfo = {v: [] for v in ["MCEff", "MCErr", "PIDCalibEff", "PIDCalibErr"]+["PIDCalibEff_Factor{}".format(i+1) for i in range(3)]+["PIDCalibErr_Factor{}".format(i+1) for i in range(3)]}
    corr = {}
    for year in years:
        info[year] = {}
        for mag in ("Up", "Down"):
            printdebug("Calibrating {} {}".format(year, mag))
            info[year][mag] = {}
            for pidcut in (["IsMuon==1"], ["DLLmu>0"], ["IsMuon==1", "DLLmu>0"]):
                pidvar = get_pidvar_from_pidcut(pidcut)
                info[year][mag][pidvar] = calibrate(info                = {},
                                                    year                = year,
                                                    mag                 = mag,
                                                    force_compute_bins  = False,
                                                    force_compute_hists = False,
                                                    **get_K0S2mu4_params(pidcut))

    return info

def get_info():
    from utils.ana_utils import get_ana_filenames
    from utils.pyutils   import file_exists, load_data_file, save_data_file
    
    pidfilename = get_ana_filenames("PID")
    if file_exists(pidfilename):
        return load_data_file(pidfilename)

    info = run()
    save_data_file(pidfilename, info)

    return info

def add_arguments(parser):
    parser.add_argument("--verbose", default = False, action = "store_true",
                        help = "Print debug messages")

if __name__ == "__main__":
    import argparse
    import pandas

    from scripts         import printdebug
    from utils.ana_utils import years
    from pid.calibrate   import calibrate

    parser = argparse.ArgumentParser(description = "Compute the calibration efficiency of a given particle for all years and magnet polarities, for a given set of user cuts and PID cuts and number of bins for the binning variables")
    add_arguments(parser)

    args = parser.parse_args()

    if args.verbose:
        from scripts import setup_debug
        setup_debug()

    '''
    info = {}
    myinfo = {v: [] for v in ["PIDCalibEff", "PIDCalibErr"]+["PIDCalibEff_Factor{}".format(i+1) for i in range(3)]+["PIDCalibErr_Factor{}".format(i+1) for i in range(3)]}
    for year in years:
        info[year] = {}
        for mag in ("Up", "Down"):
            info[year][mag] = {}
            for pidcut in (["IsMuon==1"], ["DLLmu>0"], ["IsMuon==1", "DLLmu>0"]):
                pidvar = get_pidvar_from_pidcut(pidcut)
                info[year][mag][pidvar] = calibrate(info                = {},
                                                    year                = year,
                                                    mag                 = mag,
                                                    force_compute_bins  = False,
                                                    force_compute_hists = False,
                                                    **get_K0S2mu4_params(pidcut))

                cols = list(info[year][mag][pidvar].keys())
                for col in cols:
                    myinfo[col] += [info[year][mag][pidvar][col]]
    
    df = pandas.DataFrame.from_dict(myinfo)
    '''
    
    info = get_info()
