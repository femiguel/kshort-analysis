# PID Calibration

This directory contains the code to perform the calibration of the four muons in the analysis $`K_S^0\to\mu^+\mu^-\mu^+\mu^-`$. To run the calibration of a specific year and magnet polarity, simply run:

```bash
lb-run Urania/latest python calibrate.py -h
```

This will print the different options for the analysis. Bear in mind that, while the code is rather generic for the most part, it's also specific to the $`K_S^0\to\mu^+\mu^-\mu^+\mu^-`$ analysis. This is especially notable in the file [calibrate.py](calibrate.py) -- the other modules are rather generic, though if they are to be applied to different analyses, they should be revised first. While the muons in $`K_S^0\to\mu^+\mu^-\mu^+\mu^-`$ are low-PT muons (this is done by setting the `--particle` parameter to `Mu_nopt`), the code is also applicable to generic muons (`Mu`). It assumes that the names of the muons in the reference sample are `mu_1`, `mu_2`, etc.

[main.py](main.py) runs over all years, magnet polarities, and PID cuts pertinent to this analysis. You can run it by typing:

```bash
lb-run Urania/latest python main.py
```

The three steps of the PID Calibration procedure are now explained.

## Binning computation

To compute the binning, three variables are used (in this particular case, they are `P`, `ETA`, and `nSPDhits`, but there is implementation for other options too) the following way -- a number of bins for each of the variables is given. For `P`, the minimum is 3 bins, which correspond with the momentum ranges that differentiate between the muon stations to trigger the `IsMuon` decision. 

If it is to be included, `P` must also be the first variable, because the bin edges of the second variable are customized for each of the bins of the first variable. Here's an example: Let the first variable be P, and let us have 3 `P` bins. These will be the momentum ranges used for the IsMuon trigger stations: 3 GeV $`< P <`$ 6 GeV; 6 GeV $`< P <`$ 10 GeV; $`P >`$ 10 GeV. Now, let `ETA` be the second binning variable, and let us have 4 `ETA` bins. We take the first `P` bin (3 GeV $`< P <`$ 6 GeV) and divide the sample in 4, such that each of the divisions has a similar sum of the sWeight variable. We proceed the same way with the other two P bins. Once the `ETA` binning is done (it will be 2-dimensional numpy array), we proceed with the third variable. The bin edges will be a 3-dimensional numpy array here.

The binning computation is performed in [get_binning](get_binning.py). A visual representation of the binning procedure can be computed by running [draw_graphics](draw_graphics.py). The bin edges are stored in a pickle file inside a directory named `bins_[num bins of first variable][first variable]_[num bins of the second variable][second variable]_[num bins of the third variable][third variable]`

## Histograms computation

After the bins have been computed, we take the calibration samples (in the case of this analysis, they are from J/psi -> mu+ mu-), apply to them the same cuts that we apply to our own (reference) samples, and then compute the efficiency of the PID cuts we are interested in. To do that, we will compute the amount of events in our calibration samples that are inside each of the bins (because of how the bins are computed, all numbers should be in the same order of magnitude). Then, we compute the amount of events, inside each bin, that pass the PID cuts. Finally, we store in a third numpy array the efficiency, i.e., we divide the number of passing events over the total number of events. All of this information is stored in a dictionary that is then saved in a pickle file inside a directory named `hists_[num bins of first variable][first variable]_[num bins of the second variable][second variable]_[num bins of the third variable][third variable]`

The histograms are created in [get_hists.py](get_hists.py).

## Calibration

Once the histograms are computed using the reference samples, we can now move on to our own Monte Carlo. We select the reference sample, apply the same cuts than the ones we applied to the calibration samples, and use the efficiency histograms to assign a PID calibration efficiency to each of our events. This is done by applying to each event the efficiency of the corresponding bin, based on its values of the three binning variables. The average is then computed, and printed on screen.

The calibration is done in [calibration.py](calibration.py)