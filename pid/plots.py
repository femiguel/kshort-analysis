import os
from tqdm import tqdm
import pandas
def get_calib_df(year, mag, particle, bin_var_names, cuts, pid_cuts, max_files):
    from pid.utils import (get_samples     , get_tree_paths,
                           get_branch_names, load_pidcalib_sample_as_dataframe,
                           save_output_pickle)

    sample = "Turbo"+str(year)[-2:]
    calib_sample = get_samples(sample    = sample,
                               mag       = mag,
                               particle  = particle,
                               max_files = max_files)

    tree_names = get_tree_paths(particle = particle)

    bin_vars, cut_vars, pidcut_vars = get_branch_names(bin_var_names, cuts, pid_cuts)
    branches = {**bin_vars, **cut_vars, **pidcut_vars}

    dfs = []
    for k,path in enumerate(tqdm(calib_sample["files"], leave = True, desc = "Processing files")):
        _df = load_pidcalib_sample_as_dataframe(path, tree_names, branches, cuts+pid_cuts)
        dfs.append(_df)

    df = pandas.concat(dfs, ignore_index = True)

    return df

def plot_var(df_calib, df_ref, varname):
    #from scripts.plot_utils import draw_hist, PlotOptions
    from ROOT import TCanvas, TH1F, TLegend, kBlue, kRed

    var_calib_array = df_calib[varname]
    var_ref_array   = pandas.concat([df_ref["mu{}_P".format(k)] for k in range(1,5)], ignore_index=True)

    canvas = TCanvas("c1", "c1", 1920, 1080)

    h2 = TH1F("h2", "h2", 100, 0, 60000)
    for v in var_ref_array:
        h2.Fill(v)
    h2.SetLineColor(kRed)
    h2.SetFillColor(kRed)
    h2.SetFillStyle(3004)
    h2.DrawNormalized("same err")

    h = TH1F("h1", "h1", 100, 0, 60000)
    for v in var_calib_array:
        h.Fill(v)
    h.SetLineColor(kBlue)
    h.SetFillColor(kBlue)
    h.SetFillStyle(3004)
    h.GetXaxis().SetTitle(varname)
    h.DrawNormalized("same hist")

    canvas.Update()

    legend = TLegend(0.68, 0.75, 0.9, 0.9)
    legend.AddEntry("h1", "Calibration sample", "l")
    legend.AddEntry("h2", "Reference sample", "l")
    legend.Draw()

    canvas.Update()

    input("Waiting for approval. Press ENTER to continue.")

    canvas.SaveAs("graphics/{}_comparison.png".format(varname))

if __name__ == "__main__":
    from pid.utils import get_rootfile_as_dataframe, mybranches
    from pid.calibrate import get_reference_dataframe
    from scripts import get_tuples_path

    #cuts = ["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1", "ETA<5.2"]
    cuts = ["TRCHI2NDOF<2.5", "MC15TuneV1_ProbNNghost<0.7", "TRACK_GHOSTPROB<0.25", "PT<1000", "InMuonAcc==1"]

    df = get_calib_df(year          = "2016",
                      mag           = "Up",
                      particle      = "Mu_nopt",
                      bin_var_names = ["P", "ETA", "nSPDhits"],
                      cuts          = cuts,
                      pid_cuts      = ["IsMuon==1"],
                      max_files     = 1)


    df_ref, inacc_cuts = get_reference_dataframe(year = "2016", 
                                                 mag  = "Up", 
                                                 bin_var_names = ["P", "ETA", "nSPDhits"], 
                                                 cuts = cuts, 
                                                 pid_cuts = ["IsMuon==1"])

    df_ref = df_ref.query(" and ".join(inacc_cuts))

    #print(df_ref)
        
    plot_var(df, df_ref, "P")
