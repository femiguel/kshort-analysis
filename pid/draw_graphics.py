import numpy

from pid.main import get_K0S2mu4_params
from pid.utils import get_custom_filename, load_pickle

def draw(x_edges, y_edges, values, xlabel, ylabel, logscale = False, outfname = None):
    '''
    Draw a 2-dimensional contour plot, where the X and Y axis have the names of two of the binning variables
    and the third dimension has the sum of weights inside each bin.

    :x_edges:  numpy 1-d array, edges of the X variable
    :y_edges:  numpy multi-dimensional array, edges of the Y variable
    :values:   numpy 1-d array, total sum of weights inside each bin
    :logscale: bool, True if the X axis is to have logarithmic scale
    :outfname: str, path to the output PNG file
    '''
    import matplotlib.pyplot
    from   matplotlib.collections import PolyCollection

    s = numpy.array(y_edges.shape)
    x_edges = numpy.tile(x_edges, s[0]-1).reshape( (s[0]-1, s[1]+1) )

    x = numpy.c_[x_edges[:,:-1].flatten(), x_edges[:,:-1].flatten(),
                 x_edges[:,1: ].flatten(), x_edges[:,1: ].flatten()]
    y = numpy.c_[y_edges[:-1,:].flatten(), y_edges[1:,: ].flatten(),
                 y_edges[1:,: ].flatten(), y_edges[:-1,:].flatten()]
    xy = numpy.stack( (x,y), axis = 2)

    pc = PolyCollection(xy, closed = True, edgecolors = "k", linewidth = 0.72, cmap = "inferno")
    pc.set_array(values)

    fig, ax = matplotlib.pyplot.subplots()
    ax.add_collection(pc)
    fig.colorbar(pc, ax = ax)
    ax.margins(0)
    ax.autoscale()

    matplotlib.pyplot.xlabel(xlabel, fontsize = 12)
    matplotlib.pyplot.ylabel(ylabel, fontsize = 12)
    if logscale:
        matplotlib.pyplot.xscale("log")
    matplotlib.pyplot.show()
    if outfname:
        fig.savefig(outfname)



## Example: use 2016, Mag Up
year = "2016"
mag  = "Up"

## Example: use IsMuon==1 PID cut
params = get_K0S2mu4_params(pid_cuts = ["IsMuon==1"])
params.update({"year": year, "mag": mag})
var1, var2, var3 = params["bin_var_names"]

bins_filename  = get_custom_filename(mode = "bins", **params)
hists_filename = get_custom_filename(mode = "hists", **params)
bins  = load_pickle(bins_filename)
hists = load_pickle(hists_filename)

## ETA vs. P
x_edges = bins[var1]
y_edges = bins[var2].T
values = numpy.array([[sum(hists["total"][j][k]) for k in range(len(hists["total"]))] for j in range(len(hists["total"]))])
values = values.T.flatten()

draw(x_edges  = x_edges,
     y_edges  = y_edges,
     values   = values,
     xlabel   = var1,
     ylabel   = var2,
     logscale = True,
     outfname = "graphics/contour_{0}_{1}.png".format(var1, var2))

## nSPDhits vs. ETA
for j in range(len(bins[var1]) - 1):
    x_edges = bins[var2][j]
    y_edges = bins[var3][j].T

    values = numpy.array([[hists["total"][j][k][l] for l in range(len(hists["total"][j][k]))] for k in range(len(hists["total"][j]))])
    values = values.T.flatten()

    draw(x_edges  = x_edges,
         y_edges  = y_edges,
         values   = values,
         xlabel   = var2,
         ylabel   = var3,
         logscale = False,
         outfname = "graphics/contour_{0}_{1}.png".format(var2, var3))

## Create LaTeX table with the bin edges
table = ""
for j in range(len(bins["P"])-1):
    table += "\\begin{tabular}{c|c|c|c|c|c|c|c|c}\n"
    table += "\\texttt{P} & \\texttt{ETA} & \\texttt{nSPDhits} \\\\ \\hline \n"
    for k in range(len(bins["ETA"][j])-1):
        for l in range(len(bins["nSPDhits"][j][k])):
            if j>0:
                table += " & "
            if k == 0 and l == 0:
                table += "\\multirow{%6i}{*}{%6.3f $-$ %6.3f} & "%(len(bins["nSPDhits"][j].flatten()), bins["P"][j], bins["P"][j+1])
            else:
                table += " & "
            if l == 0:
                table += "\\multirow{%6i}{*}{%6.3f $-$ %6.3f} & "%(len(bins["nSPDhits"][j][k].flatten()), bins["ETA"][j][k], bins["ETA"][j][k+1])
            else:
                table += " & "
            table += "%6.3f \\\\ \n"%(bins["nSPDhits"][j][k][l])

    table += "\hline\n"

    table += "\\end{tabular}\n"

print(table)
